ProceduralGenerator = 
{
	type = "ProceduralGenerator",
	
	Properties = 
	{
		nGenUpDown = 1,
		bGenerateDecals = 0,
		fDecalsGenCoof = 15,
		fileDecalOne = "",
		fileDecalTwo = "",
		fileDecalThree = "",
		fileDecalFour = "",
		fileDecalFive = "",
		fDecalsGenBaseScale = 1,
		fDecalsGenRandomScale = 0,
		fDecalsGenUpHeight = 10,
		fDecalsGenRandPower = 35,
		bUseOneOfGenPointsArrToDistrAllObj = 0,
		bGenerateBrushes = 0,
		fHeigthUpFromNormal = 0.1,
		fHeigthUpFromNormalRand = 0,
		objBrushOne = "",
		objBrushTwo = "",
		objBrushThree = "",
		objBrushFour = "",
		objBrushFive = "",
		nBrushOnePhyPrm = 0,
		nBrushTwoPhyPrm = 0,
		nBrushThreePhyPrm = 0,
		nBrushFourPhyPrm = 0,
		nBrushFivePhyPrm = 0,
		bBrushesGenFloating = 0,
		fBrushesGenCoof = 15,
		fBrushesGenBaseScale = 1,
		fBrushesGenRandomScale = 0,
		fBrushesGenUpHeight = 10,
		fBrushesGenRandPower = 35,
		bBrushesGenFullRandRotation = 1,
		bGenerateArcEntites = 0,
		bArcEntitesGenFloating = 0,
		sArcEntityOne = "",
		sArcEntityTwo = "",
		sArcEntityThree = "",
		sArcEntityFour = "",
		sArcEntityFive = "",
		fArcEntitesGenCoof = 15,
		fArcEntitesGenBaseScale = 1,
		fArcEntitesGenRandomScale = 0,
		fArcEntitesGenUpHeight = 10,
		fArcEntitesGenRandPower = 35,
		bArcEntitesGenFullRandRotation = 1,
		bGenerateTerrainSurfaceIds = 0,
		nTerrainSurfaceIdOne = 0,
		nTerrainSurfaceIdTwo = 0,
		nTerrainSurfaceIdThree = 0,
		nTerrainSurfaceIdFour = 0,
		nTerrainSurfaceIdFive = 0,
		nTerrainSurfaceGenCoof = 15,
		fTerrainSurfaceGenRandPower = 35,
		bGenerateVegetations = 0,
		objVegetationOne = "",
		objVegetationTwo = "",
		objVegetationThree = "",
		objVegetationFour = "",
		objVegetationFive = "",
		fVegetationIndexOne = 0,
		fVegetationIndexTwo = 0,
		fVegetationIndexThree = 0,
		fVegetationIndexFour = 0,
		fVegetationIndexFive = 0,
		bVegetationsGenFloating = 0,
		fVegetationsGenCoof = 15,
		fVegetationsGenBaseScale = 1,
		fVegetationsGenRandomScale = 0,
		fVegetationsGenUpHeight = 10,
		fVegetationsGenRandPower = 35,
		bVegetationsGenFullRandRotation = 1,
		fVegetationsBending = 1.0,
		bVegetationsAutoMerged = 0,
		sGenerationBitmap = "",
		fBitmapScale = 1.0,
		fBitmapGenPixelScale = 1.0,
		fGenObjectsViewDistance = 50,
		fGenObjectsLodDistance = 50,
		bGenOnTerrainOnly = 0,
		bRefresh = 0,
		bSaveInXMLGRPFile = 0,
		fGenDirectionRot = 0,
		fGenDirectionRHeight = 0,
		fGenZRotationRandomVal = 359.0,
		bUseTriangulation = 1,
		bOnSplineGen = 0,
		bOnShapeBorderGen = 0,
		bRotateToShapeOrSplineSegmentDir = 0,
		fShapeGenWidth = 1.0,
		fMltSplineSeg = 1.0,
		bRotToSurfaceNormal = 1,
		fMinDistToOldGenObjs = 0.0,
		fMinDistToOldGenObjsRandom = 0.0,
		fRotToNormalMlt = 1.0,
		fRotToNormalRandom = 0.0,
		fPaintRadius = 5.0,
		bPaintEnable = 0,
		fMinHeight = 0.0,
		fMaxHeight = 16384.0,
		fMinSlope = -180.0,
		fMaxSlope = 180.0,
		bLoadFromGrpFile = 0,
		fileGrpFileToLoad = "",
		bEnableObjectsStreaming = 0,
		fStreamingSectorSize = 128.0,
		fStreamingGridSize = 4096.0,
		bEnableLoadAndUnloadByDistance = 0,
		fDistanceToLoad = 1000.0,
		fDistanceToUnload= 1050.0,
		bUseCameraDirectonInPaintMode = 0,
		bEnableQLoading = 0,
		fQObjPerframeLimit= 25.0,
		bLoadFromLevelObjectsGenFile = 0,
		bLoadFromStreamingGenFiles = 0,
		bUseGenManagerThread = 0,
	},
	
	Editor = 
	{
		Model="Editor/Objects/T.cgf",
		Icon="Seed.bmp",
		ShowBounds = 1,
		IsScalable = false;
		IsRotatable = true;
	},
}



function ProceduralGenerator:OnPropertyChange()
end



function ProceduralGenerator:IsShapeOnly()
	return 0;
end



function ProceduralGenerator:Event_Hide()
	self:Hide(1);
	self:ActivateOutput("Hidden", true);
end;



function ProceduralGenerator:Event_UnHide()
	self:Hide(0);
	self:ActivateOutput( "UnHidden", true );
end;




ProceduralGenerator.FlowEvents =
{
	Inputs =
	{
		Hide = { ProceduralGenerator.Event_Hide, "bool" },
		UnHide = { ProceduralGenerator.Event_UnHide, "bool" },
	},
	Outputs =
	{
		Hidden = "bool",
		UnHidden = "bool",
	},
}

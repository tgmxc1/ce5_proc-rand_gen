// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

#include "StdAfx.h"
#include "ProcAnRandGeneratorDLL.h"
#include "ProceduralGenerator.h"

#include <CryEntitySystem/IEntityClass.h>

// Included only once per DLL module.
#include <CryCore/Platform/platform_impl.inl>

USE_CRYPLUGIN_FLOWNODES

#define HIDE_FROM_EDITOR_X(className)																																				\
  { IEntityClass *pItemClass = gEnv->pEntitySystem->GetClassRegistry()->FindClass(className);\
  pItemClass->SetFlags(pItemClass->GetFlags() | ECLF_INVISIBLE); }																				\

#define REGISTER_EDITOR_VOLUME_CLASS_X(frameWork, className)                                          \
{	                                                                                                  \
	IGameVolumes* pGameVolumes = frameWork->GetIGameVolumesManager();                                 \
	IGameVolumesEdit* pGameVolumesEdit = pGameVolumes ? pGameVolumes->GetEditorInterface() : NULL;    \
	if (pGameVolumesEdit != NULL)                                                                     \
	{                                                                                                 \
		pGameVolumesEdit->RegisterEntityClass( className );                                             \
	}                                                                                                 \
} 

#define REGISTER_GAME_OBJECT_X(framework, name, script)\
	{\
	IEntityClassRegistry::SEntityClassDesc clsDesc;\
	clsDesc.sName = #name;\
	clsDesc.sScriptFile = script;\
struct C##name##Creator : public IGameObjectExtensionCreatorBase\
		{\
		IGameObjectExtension* Create(IEntity *pEntity)\
			{\
				return pEntity->CreateComponentClass<C##name>();\
			}\
			void GetGameObjectExtensionRMIData( void ** ppRMI, size_t * nCount )\
			{\
			C##name::GetGameObjectExtensionRMIData( ppRMI, nCount );\
			}\
		};\
		static C##name##Creator _creator;\
		framework->GetIGameObjectSystem()->RegisterExtension(#name, &_creator, &clsDesc);\
	}

CGenerationManager *gen_man = NULL;
class CSystemEventListener : public ISystemEventListener
{
public:
	~CSystemEventListener()
	{
		gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);
		//SAFE_DELETE(genManagerX);
		//genManagerX = NULL;
	}

	virtual void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override
	{
		switch (event)
		{
		case ESYSTEM_EVENT_GAME_POST_INIT:
		{
			REGISTER_GAME_OBJECT_X(gEnv->pGameFramework, ProceduralGenerator, "Scripts/Entities/Others/ProceduralGenerator.lua");
			HIDE_FROM_EDITOR_X("ProceduralGenerator");
			REGISTER_EDITOR_VOLUME_CLASS_X(gEnv->pGameFramework, "ProceduralGenerator");
			/*genManagerX = new CGenerationManager();
			if (genManagerX && !genManagerX->IsManagerIntialized())
				genManagerX->InitManager();*/

			if (RandGenx)
				RandGenx->SetUpdateFlags(IPluginUpdateListener::EUpdateType_Update);
		}
		case ESYSTEM_EVENT_FULL_SHUTDOWN:
		{
			SAFE_DELETE(genManagerX);
			genManagerX = NULL;
		}
		case ESYSTEM_EVENT_CRYSYSTEM_INIT_DONE:
		{
			genManagerX = new CGenerationManager();
			if (genManagerX && !genManagerX->IsManagerIntialized())
				genManagerX->InitManager();

			gen_man = genManagerX;
		}
		break;
		}
	}
};

CSystemEventListener g_listener;


bool CPlugin_ProcAnRandGenerator::Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams)
{
	RandGenx = this;
	env.pSystem->GetISystemEventDispatcher()->RegisterListener(&g_listener);
	return true;
}

void CPlugin_ProcAnRandGenerator::OnPluginUpdate(EPluginUpdateType updateType)
{
	if (updateType == EUpdateType_Update)
	{
		if(genManagerX != NULL)
			genManagerX->UpdateManager();

		if (genManagerX == NULL)
		{
			genManagerX = gen_man;
			if (genManagerX && !genManagerX->IsManagerIntialized())
				genManagerX->InitManager();
		}
	}
}

CRYREGISTER_SINGLETON_CLASS(CPlugin_ProcAnRandGenerator)

#include <CryCore/CrtDebugStats.h>

// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

#pragma once

#include <CrySystem/ICryPlugin.h>
#include "GenerationManager.h"

class CPlugin_ProcAnRandGenerator : public ICryPlugin
{
	CRYINTERFACE_BEGIN()
		CRYINTERFACE_ADD(ICryPlugin)
		CRYINTERFACE_END()

		CRYGENERATE_SINGLETONCLASS(CPlugin_ProcAnRandGenerator, "Plugin_ProcAnRandGenerator", 0x362a829a2bbe4a5c, 0x6c74bccd93b94a00)

		PLUGIN_FLOWNODE_REGISTER
		PLUGIN_FLOWNODE_UNREGISTER

		//~CPlugin_ProcAnRandGenerator();

public:

	//! Retrieve name of plugin.
	virtual const char* GetName() const override { return "ProcAnRandGenerator"; }

	//! Retrieve category for the plugin.
	virtual const char* GetCategory() const override { return "Plugin"; }

	//! This is called to initialize the new plugin.
	virtual bool Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams) override;

	virtual void OnPluginUpdate(EPluginUpdateType updateType) override;
};

CGenerationManager *genManagerX = NULL;
CPlugin_ProcAnRandGenerator *RandGenx = NULL;
// Copyright 2001-2016 Crytek GmbH / Crytek Group. All rights reserved.

#pragma once

#define eCryModule eCryM_EnginePlugin

#include <CryCore/Project/CryModuleDefs.h> 

// Insert your headers here
#include <CryCore/Platform/platform.h>
#include <algorithm>
#include <vector>
#include <memory>
#include <list>
#include <map>
#include <functional>
#include <limits>


#include <CryCore/smartptr.h>

#include <CryCore/Containers/VectorSet.h>
#include <CryCore/StlUtils.h>

#include <CryMath/Cry_Math.h>
#include <CryMath/Cry_Camera.h>
#include <CryCore/Containers/CryListenerSet.h>
#include <CrySystem/ISystem.h>
#include <Cry3DEngine/I3DEngine.h>
#include <CryParticleSystem/IParticles.h>
#include <CryInput/IInput.h>
#include <CrySystem/IConsole.h>
#include <CrySystem/ITimer.h>
#include <CrySystem/ILog.h>
//#include <IGameplayRecorder.h>
#include <CryNetwork/ISerialize.h>

//#include "CryMacros.h"

#include <CryGame/GameUtils.h>

#include <CrySystem/Profilers/FrameProfiler/FrameProfiler_JobSystem.h>
#include "StdAfx.h"
#include "ProceduralGenerator.h"
//#include "EntityUtility/EntityEffects.h"
#include <CryInput/IHardwareMouse.h>
#include <CryMath/Cry_GeoOverlap.h>
#include <CryRenderer/IRenderer.h>      // this is needed for debug drawing
#include <CryRenderer/IRenderAuxGeom.h> // this is needed for debug drawing
#include <../CryAction/ILevelSystem.h>
#include "ProcAnRandGeneratorDLL.h"
//#include <../CryAction/PlayerProfiles/RichSaveGameTypes.h>
#include <../CryAction/PlayerProfiles/RichSaveGameTypes_info.h>

const char* frtX[2] = { "A","B" };

namespace BMPXX
{
	bool LoadBMP(const char* filename, FILE* pFile, uint8* pByteData, int& width, int& height, int& depth, bool bForceInverseY)
	{
		// todo: cleanup for pFile
		struct Cleanup
		{
			Cleanup() : m_pFile(0), m_bCloseFile(false), m_bRestoreCur(false), m_offset(0) {}
			~Cleanup()
			{
				if (m_pFile)
				{
					if (m_bRestoreCur)
						gEnv->pCryPak->FSeek(m_pFile, m_offset, SEEK_SET);
					if (m_bCloseFile)
						gEnv->pCryPak->FClose(m_pFile);
				}
			}
			void SetFile(FILE* pFile, bool bClose, bool bRestoreCur)
			{
				m_pFile = pFile;
				m_bCloseFile = bClose;
				m_bRestoreCur = bRestoreCur;
				if (m_pFile && m_bRestoreCur)
					m_offset = gEnv->pCryPak->FTell(m_pFile);
			}
		protected:
			FILE* m_pFile;
			bool  m_bCloseFile;
			bool  m_bRestoreCur;
			long  m_offset;
		};

		Cleanup cleanup;   // potentially close the file on return (if pFile was given, do nothing)
		CryBitmapFileHeader bitmapfileheader;
		CryBitmapInfoHeader bitmapinfoheader;

		memset(&bitmapfileheader, 0, sizeof(CryBitmapFileHeader));
		memset(&bitmapinfoheader, 0, sizeof(CryBitmapInfoHeader));

		ICryPak* pCryPak = gEnv->pCryPak;
		const bool bFileGiven = pFile != 0;
		if (bFileGiven == false)
			pFile = pCryPak->FOpen(filename, "rb");
		if (pFile == 0)
			return false;

		cleanup.SetFile(pFile, !bFileGiven, bFileGiven);

		size_t els = pCryPak->FRead(&bitmapfileheader, 1, pFile);
		if (els != 1)
			return false;

		els = pCryPak->FRead(&bitmapinfoheader, 1, pFile);
		if (els != 1)
			return false;

		if (bitmapfileheader.bfType != 0x4D42)
			return false;

		if (bitmapinfoheader.biCompression != 0)
			return false;

		if (bitmapinfoheader.biBitCount != 24 && bitmapinfoheader.biBitCount != 32)
			return false;

		if (bitmapinfoheader.biPlanes != 1)
			return false;

		int imgWidth = bitmapinfoheader.biWidth;
		int imgHeight = bitmapinfoheader.biHeight;
		int imgDepth = bitmapinfoheader.biBitCount == 24 ? 3 : 4;

		int imgBytesPerLine = imgWidth * imgDepth;
		int fileBytesPerLine = imgBytesPerLine;
		if (fileBytesPerLine & 0x0003)
		{
			fileBytesPerLine |= 0x0003;
			++fileBytesPerLine;
		}
		size_t prologSize = sizeof(CryBitmapFileHeader) + sizeof(CryBitmapInfoHeader);
		size_t bufSize = bitmapfileheader.bfSize - prologSize;

		// some BMPs from Adobe PhotoShop seem 2 bytes longer
		if (bufSize != fileBytesPerLine * imgHeight && bufSize != (fileBytesPerLine * imgHeight) + 2)
			return false;

		bool bFlipY = true;
		width = imgWidth;
		height = imgHeight;
		if (height < 0)     // height > 0: bottom->top, height < 0: top->bottom, no flip necessary
		{
			height = -height;
			bFlipY = false;
		}
		depth = imgDepth;

		if (pByteData == 0)   // only requested dimensions
			return true;

		uint8* pTempImage = new uint8[bufSize];
		if (pTempImage == 0)
			return false;

		els = pCryPak->FReadRaw(pTempImage, 1, bufSize, pFile);
		if (els != bufSize)
		{
			delete[] pTempImage;
			return false;
		}

		if (bForceInverseY)
			bFlipY = !bFlipY;

		// read all rows
		uint8* pSrcLine = pTempImage;
		for (int y = 0; y < imgHeight; ++y)
		{
			int dstY = y;
			if (bFlipY)
				dstY = imgHeight - y - 1;

			uint8* pDstLine = pByteData + dstY * imgBytesPerLine;
			memcpy(pDstLine, pSrcLine, imgBytesPerLine);
			pSrcLine += fileBytesPerLine;
		}

		delete[] pTempImage;

		// Success
		return true;
	}
}

namespace RANDOMNAMESXRC
{
	const char* abc[51] = { "A","B","C","D","E","F","J","K","L","N",
		"a","b","c","d","e","f","j","k","l","n",
		"1","2","3","4","5","6","7","8","9","0",
		"x","p","m","o","i","u","y","t","r","w",
		"X","P","M","O","I","U","Y","T","R","W", "Q" };

	const char* ABC[17] = { "A","B","C","D","E","F"
		"1","2","3","4","5","6","7","8","9","0","0" 
		 };

	string GenerateObjectName(string fts_nm)
	{
		for (int iK = 1; iK < 18; iK++)
		{
			int rnd_val_ch = cry_random(0, 50);
			int iters_nm = cry_random(0, 7);
			for (int iKk = 0; iKk < iters_nm; iKk++)
			{
				rnd_val_ch = cry_random(0, 50);
			}
			fts_nm += abc[rnd_val_ch];
		}
		return fts_nm;
	}

	string GenerateObjectGUID()
	{
		//sample {00000000-0000-0000-0000-000000000000}
		string GUID = "{";
		string vd = "-";
		for (int i = 0; i < 8; i++)
		{
			int rnd_val_ch = cry_random(0, 16);
			int iters_nm = cry_random(0, 7);
			for (int ic = 0; ic < iters_nm; ic++)
			{
				rnd_val_ch = cry_random(0, 16);
			}
			GUID += ABC[rnd_val_ch];
		}
		GUID += vd;
		for (int i = 0; i < 4; i++)
		{
			int rnd_val_ch = cry_random(0, 16);
			int iters_nm = cry_random(0, 7);
			for (int ic = 0; ic < iters_nm; ic++)
			{
				rnd_val_ch = cry_random(0, 16);
			}
			GUID += ABC[rnd_val_ch];
		}
		GUID += vd;
		for (int i = 0; i < 4; i++)
		{
			int rnd_val_ch = cry_random(0, 16);
			int iters_nm = cry_random(0, 7);
			for (int ic = 0; ic < iters_nm; ic++)
			{
				rnd_val_ch = cry_random(0, 16);
			}
			GUID += ABC[rnd_val_ch];
		}
		GUID += vd;
		for (int i = 0; i < 4; i++)
		{
			int rnd_val_ch = cry_random(0, 16);
			int iters_nm = cry_random(0, 7);
			for (int ic = 0; ic < iters_nm; ic++)
			{
				rnd_val_ch = cry_random(0, 16);
			}
			GUID += ABC[rnd_val_ch];
		}
		GUID += vd;
		for (int i = 0; i < 12; i++)
		{
			int rnd_val_ch = cry_random(0, 16);
			int iters_nm = cry_random(0, 7);
			for (int ic = 0; ic < iters_nm; ic++)
			{
				rnd_val_ch = cry_random(0, 16);
			}
			GUID += ABC[rnd_val_ch];
		}
		GUID += "}";
		return GUID;
	}
}

CProceduralGenerator::CProceduralGenerator()
{
	succesfully_gen_oblects.clear();
	succesfully_gen_oblects.resize(0);
	succesfully_gen_entites.clear();
	succesfully_gen_entites.resize(0);
	upd_timer_c1 = 0.0f;
	request_regenerate = true;
	old_upd_prm_refr = true;
	already_gen = false;
	gen_direction = Vec3(0,0,-1);
	for (int i = 0; i < 5; i++)
	{
		veg_grps[i];
	}
	paint_pos[0] = Vec3(ZERO);
	paint_pos[1] = Vec3(ZERO);
	is_SplineGen = false;
	is_ShapeGen = false;
	is_paint_started = false;
	is_erase_started = false;
	xrf_gen_shape.clear();
	xrf_gen_shape.resize(0);
	bmp_loaded_wh[0] = 0;
	bmp_loaded_wh[1] = 0;
	current_mouse_world_pos = Vec3(ZERO);
	current_mouse_world_pos2 = Vec3(ZERO);
	current_mouse_world_pos_projected = Vec3(ZERO);
	center_point = Vec3(ZERO);
	generatorWorldPoint = Vec2(ZERO);
	is_que_enabled = false;
	que_perframe_limit = 1000;
	//gen_objects_queue._Get_container.clear();
	//gen_objects_queue._Get_container.resize(0);
	generated_sectors.clear();
	loaded_sectors_ids.clear();
	unloading_sectors_ids.clear();
	generated_sectors.resize(0);
	loaded_sectors_ids.resize(0);
	unloading_sectors_ids.resize(0);
	old_nearest_steaming_segment = -1;
	is_streaming_sectors_enabled = false;
	streaming_cover_size = 4096;
	generated_objects_in_stream_rd = 0;
	use_generation_manager_thread = false;
	setting_data = SGenSettingData();
	generation_setting_data_refilled = false;
	load_objects_in_streamFromStreamingGenFiles = false;
	sector_size = 128;
}

CProceduralGenerator::~CProceduralGenerator()
{
	ClearGeneratedRenderNodes();
	ClearGeneratedRenderNodesInAllSectors();
	//RegisterInputListner(false);
	if (genManagerX != NULL)
	{
		genManagerX->DelGeneratorFromManager(this);
	}
}

bool CProceduralGenerator::Init(IGameObject * pGameObject)
{
	SetGameObject(pGameObject);
	return true;
}

void CProceduralGenerator::PostInit(IGameObject * pGameObject)
{
	if(GetGameObject())
		GetGameObject()->EnableUpdateSlot(this, 0);

	if (genManagerX != NULL)
	{
		genManagerX->AddGeneratorToManager(this);
	}

	if (m_TimerGen)
	{
		gEnv->pGameFramework->RemoveTimer(m_TimerGen);
		m_TimerGen = NULL;
	}

	if (!m_TimerGen)
		m_TimerGen = gEnv->pGameFramework->AddTimer(CTimeValue(cry_random(0.01f, 0.4f)), false, functor(*this, &CProceduralGenerator::DelayedGenObjects), NULL);
}

void CProceduralGenerator::Release()
{
	ClearGeneratedRenderNodes();
	delete this;
}

void CProceduralGenerator::Update(SEntityUpdateContext & ctx, int slot)
{
	//CryLogAlways("CProceduralGenerator::Update called");
	bool save_in_xml = false;
	upd_timer_c1 -= ctx.fFrameTime;
	if (upd_timer_c1 <= 0.0f)
	{
		upd_timer_c1 = cry_random(0.3f,0.5f);
		SmartScriptTable props;
		IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
		if (pScriptTable && pScriptTable->GetValue("Properties", props))
		{
			if (gEnv->IsEditor() && !gEnv->IsEditorGameMode())
			{
				bool refresh = false;
				props->GetValue("bRefresh", refresh);
				if (old_upd_prm_refr != refresh)
				{
					bool painting_enabled = false;
					props->GetValue("bPaintEnable", painting_enabled);
					if (painting_enabled)
					{
						GetGameObject()->EnablePostUpdates(this);
						RegisterInputListner(true);
					}
					else
					{
						GetGameObject()->DisablePostUpdates(this);
						RegisterInputListner(false);
					}
					//CryLogAlways("CProceduralGenerator::Update called 2, request regenerate objects");
					old_upd_prm_refr = refresh;
					request_regenerate = true;
				}

				UpdateGenDirection();
				props->GetValue("bSaveInXMLGRPFile", save_in_xml);

				props->GetValue("bOnSplineGen", is_SplineGen);
				props->GetValue("bOnShapeBorderGen", is_ShapeGen);

				RefillGenerationSettingData();
			}
			bool EnableLoadAndUnloadByDistance = false;
			props->GetValue("bEnableLoadAndUnloadByDistance", EnableLoadAndUnloadByDistance);
			if (EnableLoadAndUnloadByDistance && !center_point.IsZero())
			{
				CCamera& cam = GetISystem()->GetViewCamera();
				center_point.z = cam.GetPosition().z;
				float DistanceToLoad, DistanceToUnload = 0.0f;
				props->GetValue("fDistanceToLoad", DistanceToLoad);
				props->GetValue("fDistanceToUnload", DistanceToUnload);
				if (cam.GetPosition().GetDistance(center_point) < DistanceToLoad)
				{
					if(!already_gen)
						GenObjects();
				}
				else if (cam.GetPosition().GetDistance(center_point) > DistanceToUnload)
				{
					if (already_gen)
					{
						ClearGeneratedRenderNodes();
						already_gen = false;
					}
				}
			}
			props->GetValue("bUseGenManagerThread", use_generation_manager_thread);
			bool EnableStreaming = false;
			props->GetValue("bEnableObjectsStreaming", EnableStreaming);
			if (EnableStreaming != is_streaming_sectors_enabled)
			{
				ClearGeneratedRenderNodesInAllSectors();
			}
			props->GetValue("bEnableObjectsStreaming", is_streaming_sectors_enabled);
			if (EnableStreaming && !IsGenerationManagerThreadUsed())
			{
				props->GetValue("bEnableQLoading", is_que_enabled);
				props->GetValue("fQObjPerframeLimit", que_perframe_limit);
				props->GetValue("fStreamingGridSize", streaming_cover_size);
				props->GetValue("fStreamingSectorSize", sector_size);
				UpdateStreamingProcess();
			}
		}
	}

	if (GetEntity() && gEnv->IsEditor())
	{
		Vec3 ShapeWPos = GetEntity()->GetWorldPos();
		ShapeWPos.z += 3.0f;
		Vec3 PointTwo = ShapeWPos + (gen_direction * 2.0f);
		IRenderer* pRenderer = gEnv->pRenderer;
		if (pRenderer)
		{
			IRenderAuxGeom* pAuxGeom = pRenderer->GetIRenderAuxGeom();
			if (pAuxGeom)
			{
				pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
				ColorB colNormal(200, 80, 150, 128);
				ColorB colNormalEnd(90, 100, 200, 128);
				pAuxGeom->DrawLine(ShapeWPos, colNormal, PointTwo, colNormalEnd, 10.0f);
				pAuxGeom->DrawCone(PointTwo, gen_direction, 0.3f, 0.5f, colNormalEnd);
				if (is_SplineGen || is_ShapeGen)
				{
					std::vector<Vec3>::const_iterator it = xrf_gen_shape.begin();
					std::vector<Vec3>::const_iterator end = xrf_gen_shape.end();
					int count = xrf_gen_shape.size();
					if (count > 0)
					{
						colNormal = ColorB(80,10,5,128);
						colNormalEnd = ColorB(5,150,35,128);
						//CryLogAlways("CTrainRails::Update called, point count = %i", count);
						pAuxGeom->SetRenderFlags(e_Def3DPublicRenderflags);
						Vec3 old_pnt(ZERO);
						bool inverse = false;
						for (int i = 0; i < count, it != end; i++, ++it)
						{
							if (i > 0)
							{
								Vec3 cvdx = (*it);
								if (!inverse)
									pAuxGeom->DrawLine(old_pnt, colNormalEnd, cvdx, colNormal, 10.0f);
								else
									pAuxGeom->DrawLine(old_pnt, colNormal, cvdx, colNormalEnd, 10.0f);

								if (i == 1)
								{
									pAuxGeom->DrawCone(old_pnt, (cvdx - old_pnt).GetNormalized(), 0.4f, 0.9f, colNormal);
								}
							}
							inverse = !inverse;
							old_pnt = (*it);
						}
					}
				}

				if (bmp_loaded_wh[0] != 0)
				{
					colNormalEnd = ColorB(50, 255, 50, 128);
					float wh[2] = { float(bmp_loaded_wh[0]), float(bmp_loaded_wh[1]) };
					SmartScriptTable props;
					IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
					if (pScriptTable && pScriptTable->GetValue("Properties", props))
					{
						float BitmapScale = 1.0f;
						props->GetValue("fBitmapScale", BitmapScale);
						wh[0] = wh[0] * BitmapScale;
						wh[1] = wh[1] * BitmapScale;
					}
					Vec3 p_line[4];
					p_line[0] = ShapeWPos;
					p_line[1] = ShapeWPos + Vec3(0, wh[0],0);
					p_line[2] = p_line[1] + Vec3(wh[1], 0, 0);
					p_line[3] = p_line[2] - Vec3(0, wh[0], 0);
					pAuxGeom->DrawPolyline(p_line, 4, true, colNormalEnd, 10);
				}

				SmartScriptTable props;
				IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
				if (pScriptTable && pScriptTable->GetValue("Properties", props))
				{
					bool painting_enabled = false;
					props->GetValue("bPaintEnable", painting_enabled);
					if (painting_enabled)
					{
						float paint_rd = 1.0f;
						props->GetValue("fPaintRadius", paint_rd);
						bool gen_olly_on_terrain = false;
						props->GetValue("bGenOnTerrainOnly", gen_olly_on_terrain);
						int gen_on_stat_objs = ent_static;
						if (gen_olly_on_terrain)
							gen_on_stat_objs = 0;

						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(current_mouse_world_pos, (current_mouse_world_pos2 - current_mouse_world_pos).GetNormalized() * 3000, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						if (n > 0)
						{
							colNormal = ColorB(0, 150, 25, 14);
							if(is_paint_started && !is_erase_started)
								colNormal = ColorB(0, 25, 150, 14);
							else if(is_erase_started)
								colNormal = ColorB(150, 25, 0, 14);

							pAuxGeom->DrawSphere(hit.pt, paint_rd, colNormal);
							paint_pos[0] = hit.pt;
						}
					}
					CCamera& cam = GetISystem()->GetViewCamera();
					Vec3 cam_pos_cv = cam.GetPosition();
					int gen_objects_num = succesfully_gen_oblects.size() + succesfully_gen_entites.size();
					int gen_objects_num_streamx = 0;
					bool EnableStreaming = false;
					if (is_streaming_sectors_enabled)
					{
						std::vector<SGenerationSector *>::const_iterator it_x = generated_sectors.begin();
						std::vector<SGenerationSector *>::const_iterator end_x = generated_sectors.end();
						int count_x = generated_sectors.size();
						if (count_x > 0)
						{
							for (int ir = 0; ir < count_x, it_x != end_x; ir++, ++it_x)
							{
								SGenerationSector *pSgen_sect = (*it_x);
								if (!pSgen_sect)
									continue;

								gen_objects_num += pSgen_sect->sector_gen_oblects.size();
							}
							generated_objects_in_stream_rd = gen_objects_num;
						}

						for (int ic = 0; ic < loaded_sectors_ids.size(); ++ic)
						{
							if (loaded_sectors_ids[ic] > 0)
							{
								SGenerationSector *pSgen_sect = GetStreamingSectorPointer(loaded_sectors_ids[ic]);
								if (pSgen_sect == nullptr)
									continue;

								gen_objects_num_streamx += (pSgen_sect->sector_succesfully_gen_oblects.size() + pSgen_sect->sector_succesfully_gen_entites.size());
								colNormalEnd = ColorB(255, 12, 45, 128);
								Vec3 p_line[4];
								p_line[0] = Vec3(float(pSgen_sect->sector_start_point.x), float(pSgen_sect->sector_start_point.y), 220.0f);
								float sc_sz = float(pSgen_sect->sector_size);
								p_line[1] = p_line[0] + Vec3(0, sc_sz, 0);
								p_line[2] = p_line[1] + Vec3(sc_sz, 0, 0);
								p_line[3] = p_line[2] - Vec3(0, sc_sz, 0);
								pAuxGeom->DrawPolyline(p_line, 4, true, colNormalEnd, 10);

								Vec3 sector_cntr = Vec3(pSgen_sect->sector_center_point.x, pSgen_sect->sector_center_point.y, 250.0f);
								IRenderAuxText::DrawLabelExF(sector_cntr, 1.5f, (float*)&colNormal, false, true, "Number of generated objects = %i", pSgen_sect->sector_succesfully_gen_oblects.size());
								IRenderAuxText::DrawLabelExF(Vec3(float(pSgen_sect->sector_start_point.x), float(pSgen_sect->sector_start_point.y), 220.0f), 2.3f, (float*)&colNormal, false, true, "Streaming sector id = %i", pSgen_sect->sector_id);
								IRenderAuxText::DrawLabelExF(sector_cntr + Vec3(0, 0, -50), 1.7f, (float*)&colNormal, false, true, "Number of pre generated objs = %i", pSgen_sect->sector_gen_oblects.size());
							}
						}

						for (int ic = 0; ic < unloading_sectors_ids.size(); ++ic)
						{
							if (unloading_sectors_ids[ic] > 0)
							{
								SGenerationSector *pSgen_sect = GetStreamingSectorPointer(unloading_sectors_ids[ic]);
								if (pSgen_sect == nullptr)
									continue;

								gen_objects_num_streamx += (pSgen_sect->sector_succesfully_gen_oblects.size() + pSgen_sect->sector_succesfully_gen_entites.size());
								colNormalEnd = ColorB(255, 10, 255, 128);
								Vec3 p_line[4];
								p_line[0] = Vec3(float(pSgen_sect->sector_start_point.x), float(pSgen_sect->sector_start_point.y), 220.0f);
								float sc_sz = float(pSgen_sect->sector_size);
								p_line[1] = p_line[0] + Vec3(0, sc_sz, 0);
								p_line[2] = p_line[1] + Vec3(sc_sz, 0, 0);
								p_line[3] = p_line[2] - Vec3(0, sc_sz, 0);
								pAuxGeom->DrawPolyline(p_line, 4, true, colNormalEnd, 10);
							}
						}
					}
					if (gen_objects_num_streamx > 0)
					{
						IRenderAuxText::DrawLabelExF(GetEntity()->GetWorldPos() + Vec3(0, 0, 8), 2.4f, (float*)&colNormal, false, true, "Number of created objects in streaming sectors = %i", gen_objects_num_streamx);
					}
					IRenderAuxText::DrawLabelExF(GetEntity()->GetWorldPos() + Vec3(0, 0, 2), 1.8f, (float*)&colNormal, false, true, "Number of generated objects = %i", gen_objects_num);
				}
			}
		}
	}

	if (request_regenerate)
	{
		if (save_in_xml && already_gen)
		{
			SaveObjectsInXML();
		}
		request_regenerate = false;
		//if(!g_pGame->GetIGameFramework()->IsGameStarted())
		GenObjects();
	}

	if (is_que_enabled && !gen_objects_queue.empty() && already_gen && !is_streaming_sectors_enabled)
	{
		int cq_objects_loaded = 0;
		while (!gen_objects_queue.empty())
		{
			GreateObjectFromQue(gen_objects_queue.front());
			gen_objects_queue.pop();
			cq_objects_loaded += 1;
			if (cq_objects_loaded >= que_perframe_limit)
				break;
		}
	}

	if (is_que_enabled && already_gen && is_streaming_sectors_enabled/* && !IsGenerationManagerThreadUsed()*/)
	{
		for (int ic = 0; ic < loaded_sectors_ids.size(); ++ic)
		{
			uint32 cq_objects_loaded = 0;
			uint32 old_numb_ojs = 9999999;
			if (loaded_sectors_ids[ic] > 0)
			{
				SGenerationSector *pSgen_sect = GetStreamingSectorPointer(loaded_sectors_ids[ic]);
				if (pSgen_sect == nullptr)
					continue;

				if (load_objects_in_streamFromStreamingGenFiles)
				{
					if (!pSgen_sect->loading_xml_complete)
					{
						continue;
					}
				}

				while ((pSgen_sect->GetNumberOfGeneratedObjects() != pSgen_sect->GetNumberOfLoadedObjects())
					|| !pSgen_sect->IsGenerationComplete())
				{
					if (old_numb_ojs == pSgen_sect->GetNumberOfLoadedObjects())
					{
						break;
					}
					old_numb_ojs = pSgen_sect->GetNumberOfLoadedObjects();
					GenerateObjectInSector(pSgen_sect, pSgen_sect->GetNumberOfLoadedObjects());
					cq_objects_loaded += 1;
					if (cq_objects_loaded >= que_perframe_limit)
						break;
				}
			}
		}

		//if(!IsGenerationManagerThreadUsed())
			ProcessCurrentSectors();
	}
}

void CProceduralGenerator::RegisterInputListner(bool bRegister)
{
	if (IInput* pInput = gEnv->pInput)
	{
		if (bRegister)
		{
			pInput->AddEventListener(this);
		}
		else
		{
			pInput->RemoveEventListener(this);
		}
	}
}

bool CProceduralGenerator::OnInputEvent(const SInputEvent & event)
{
	//if (gEnv->pConsole->IsOpened())
	//	return false;

	string m_eventKeyName = event.keyName.c_str();

	const string& keyName = "y";
	const string& keyName2 = "n";

	if (keyName.empty() == false)
	{
		if ((stricmp(keyName.c_str(), m_eventKeyName.c_str()) != 0) && (stricmp(keyName2.c_str(), m_eventKeyName.c_str()) != 0))
			return false;
	}

	const bool nullInputValue = (fabs_tpl(event.value) < 0.02f);
	if ((event.state == eIS_Pressed) && !nullInputValue)
	{
		is_paint_started = true;
		SmartScriptTable props;
		IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
		if (pScriptTable && pScriptTable->GetValue("Properties", props))
		{
			bool painting_enabled = false;
			props->GetValue("bPaintEnable", painting_enabled);
			if (painting_enabled)
			{
				float paint_rd = 1.0f;
				props->GetValue("fPaintRadius", paint_rd);
				if (!stricmp(keyName2.c_str(), m_eventKeyName.c_str()))
				{
					DoEraseObjectsAtPont(paint_pos[0], paint_rd);
					is_erase_started = true;
				}
				else
				{
					DoPaint(paint_pos[0], paint_rd);
				}
			}
		}
	}
	else if ((event.state == eIS_Released) || ((event.state == eIS_Changed) && !nullInputValue))
	{
		is_paint_started = false;
		is_erase_started = false;
	}

	return false;
}

void CProceduralGenerator::PostUpdate(float frameTime)
{
	if (gEnv->pRenderer)
	{
		float m_mouseY, m_mouseX = 0.0f;
		if (gEnv->pHardwareMouse)
		{
			gEnv->pHardwareMouse->GetHardwareMouseClientPosition(&m_mouseX, &m_mouseY);
		}
		int invMouseY = gEnv->pRenderer->GetHeight() - int(m_mouseY);
		Vec3 vPos0(0, 0, 0);
		gEnv->pRenderer->UnProjectFromScreen((float)m_mouseX, (float)invMouseY, 0, &vPos0.x, &vPos0.y, &vPos0.z);

		Vec3 vPos1(0, 0, 0);
		gEnv->pRenderer->UnProjectFromScreen((float)m_mouseX, (float)invMouseY, 1, &vPos1.x, &vPos1.y, &vPos1.z);

		Vec3 vDir = vPos1 - vPos0;
		vDir.Normalize();
		current_mouse_world_pos = vPos0;
		vPos0 = vPos0 + (vDir * 10.0f);
		current_mouse_world_pos2 = vPos0;
		Vec3 screenPos(0, 0, 0);
		gEnv->pRenderer->ProjectToScreen(vPos0.x, vPos0.y, vPos0.z, &screenPos.x, &screenPos.y, &screenPos.z);
		current_mouse_world_pos_projected = screenPos;
	}

	if (is_paint_started)
	{
		SmartScriptTable props;
		IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
		float paint_rd = 1.0f;
		if (pScriptTable && pScriptTable->GetValue("Properties", props))
		{
			props->GetValue("fPaintRadius", paint_rd);
			if (paint_pos[0].GetDistance(paint_pos[1]) > (paint_rd / 2.0f))
			{
				if (!is_erase_started)
				{
					DoPaint(paint_pos[0], paint_rd);
				}
				else
				{
					DoEraseObjectsAtPont(paint_pos[0], paint_rd);
				}
			}
		}
	}
}

void CProceduralGenerator::GetMemoryUsage(ICrySizer * pSizer) const
{
	pSizer->AddObject(this, sizeof(*this));
	pSizer->AddContainer(succesfully_gen_oblects);
	pSizer->AddContainer(succesfully_gen_entites);
	pSizer->AddContainer(xrf_gen_shape);
	//pSizer->AddContainer(gen_objects_queue);
	pSizer->AddContainer(generated_sectors);
	pSizer->AddContainer(loaded_sectors_ids);
	pSizer->AddContainer(unloading_sectors_ids);
}

void CProceduralGenerator::DelayedGenObjects(void * pUserData, IGameFramework::TimerID handler)
{
	if (m_TimerGen)
	{
		gEnv->pGameFramework->RemoveTimer(m_TimerGen);
		m_TimerGen = NULL;
	}

	if(!already_gen && !gEnv->IsEditor())
		GenObjects();
}

void CProceduralGenerator::GenObjects()
{
	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	if (!gEnv->pPhysicalWorld)
		return;

	ClearGeneratedRenderNodes();
	ClearGeneratedRenderNodesInAllSectors();
	generation_setting_data_refilled = false;
	UpdateGenDirection();

	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	if(!props)
		return;

	props->GetValue("bUseGenManagerThread", use_generation_manager_thread);
	props->GetValue("bEnableObjectsStreaming", is_streaming_sectors_enabled);
	if (is_streaming_sectors_enabled)
	{
		props->GetValue("bEnableQLoading", is_que_enabled);
		props->GetValue("fQObjPerframeLimit", que_perframe_limit);
		props->GetValue("fStreamingGridSize", streaming_cover_size);
		props->GetValue("fStreamingSectorSize", sector_size);
		UpdateStreamingProcess();
	}
	//int sector_size = 128;
	props->GetValue("fStreamingSectorSize", sector_size);

	props->GetValue("bEnableQLoading", is_que_enabled);
	props->GetValue("fQObjPerframeLimit", que_perframe_limit);

	props->GetValue("bLoadFromStreamingGenFiles", load_objects_in_streamFromStreamingGenFiles);
	if (load_objects_in_streamFromStreamingGenFiles)
	{
		already_gen = true;
		return;
	}

	CCamera& camX = GetISystem()->GetViewCamera();
	bool EnableLoadAndUnloadByDistance = false;
	props->GetValue("bEnableLoadAndUnloadByDistance", EnableLoadAndUnloadByDistance);
	if (EnableLoadAndUnloadByDistance)
	{
		IGameVolumes::VolumeInfo volumeInfo;
		if (!GetVolumeInfoForEntityGen(GetEntityId(), &volumeInfo))
			return;

		AABB genBounds;
		GetEntity()->GetWorldBounds(genBounds);
		Matrix34 mat = GetEntity()->GetWorldTM();
		mat.SetScale(Vec3(1.0f));
		mat.SetTranslation(GetEntity()->GetWorldPos());
		uint32 vertexCount = volumeInfo.verticesCount;
		float volumeHeight = volumeInfo.volumeHeight;
		float dist = 0.0f;
		int vertex_last = 1;
		genBounds.min = mat.TransformPoint(volumeInfo.pVertices[0]);
		for (uint32 i = 1; i < vertexCount; ++i)
		{
			Vec3 v1 = volumeInfo.pVertices[i];
			float dv = v1.GetDistance(volumeInfo.pVertices[0]);
			if (dist < dv)
			{
				dist = dv;
				vertex_last = i;
			}
		}
		genBounds.max = mat.TransformPoint(volumeInfo.pVertices[vertex_last]) + Vec3(0, 0, volumeHeight);
		center_point = genBounds.GetCenter();
		center_point.z = camX.GetPosition().z;
		float DistanceToLoad, DistanceToUnload = 0.0f;
		props->GetValue("fDistanceToLoad", DistanceToLoad);
		props->GetValue("fDistanceToUnload", DistanceToUnload);
		if (camX.GetPosition().GetDistance(center_point) > DistanceToLoad)
		{
			return;
		}
	}

	bool LoadFromLevelObjectsGenFile = false;
	props->GetValue("bLoadFromLevelObjectsGenFile", LoadFromLevelObjectsGenFile);
	if (LoadFromLevelObjectsGenFile)
	{
		bool is_mod = false;
		if (const ICmdLineArg* pModArg = gEnv->pSystem->GetICmdLine()->FindArg(eCLAT_Pre, "MOD"))
		{
			if (gEnv->pSystem->IsMODValid(pModArg->GetValue()))
			{
				is_mod = true;
			}
		}
		string file_nmn = GetEntity()->GetName() + string(".grp");
		string levelPath = PathUtil::GetGameFolder();
		ILevelInfo* pLevelInfo = gEnv->pGameFramework->GetILevelSystem()->GetCurrentLevel();
		if (pLevelInfo)
		{
			levelPath = PathUtil::AddSlash(levelPath);
			if (is_mod)
				levelPath = "";

			levelPath = levelPath + pLevelInfo->GetPath();
		}
		levelPath = PathUtil::AddSlash(levelPath);
		levelPath = levelPath + string("GeneratedObjs");
		levelPath = PathUtil::AddSlash(levelPath);
		string file_pth = levelPath;
		file_pth.append(file_nmn);
		XmlNodeRef Objects_nodeC = GetISystem()->LoadXmlFromFile(file_pth);
		if (Objects_nodeC)
		{
			GenerateObjectsFromXMLFile(file_pth);
			already_gen = true;
			return;
		}
	}

	bool LoadFromGrpFile = false;
	props->GetValue("bLoadFromGrpFile", LoadFromGrpFile);
	if (LoadFromGrpFile)
	{
		string GrpFileToLoad = "";
		props->GetValue("fileGrpFileToLoad", GrpFileToLoad);
		if(!GrpFileToLoad.empty())
			GenerateObjectsFromXMLFile(GrpFileToLoad);

		already_gen = true;
		return;
	}

	string generation_image = "";
	ICryPak* pCryPak = gEnv->pCryPak;
	props->GetValue("sGenerationBitmap", generation_image);
	if (!generation_image.empty() && pCryPak)
	{
		FILE* pFile = pCryPak->FOpen(generation_image, "rbx");
		if (pFile)
		{
			int width = 0;
			int height = 0;
			int depth = 0;
			bool bSuccess = BMPXX::LoadBMP("", pFile, 0, width, height, depth, false);
			if (bSuccess)
			{
				bmp_loaded_wh[0] = width;
				bmp_loaded_wh[1] = height;
				DynArray<uint8> data;
				data.resize(width * height * depth);
				bSuccess = BMPXX::LoadBMP("", pFile, data.begin(), width, height, depth, false);
				if (bSuccess)
				{
					float BitmapScale = 1.0f;
					props->GetValue("fBitmapScale", BitmapScale);
					float BitmapGenPixelScale = 1.0f;
					props->GetValue("fBitmapGenPixelScale", BitmapGenPixelScale);
					int origBytesPerLine = width * depth;
					for (int y = 0; y < height; y++)
					{
						int src_y = y;
						src_y = (height - 1) - y;
						uint8* pSrcLine = data.begin() + src_y * origBytesPerLine;
						if (depth == 3)
						{
							uint8 r, g, b;
							for (int x = 0; x < width; x++)
							{
								b = *pSrcLine++;
								g = *pSrcLine++;
								r = *pSrcLine++;
								float gen_xp = float(b + g + r);
								if (gen_xp > 0.0f)
								{
									gen_xp = (gen_xp / 768.0f) * 100.0f;
									Vec2 xy_map(float(x*BitmapScale), float(y*BitmapScale));
									
									GenObjectsAtTerrainPoint(xy_map, gen_xp, BitmapGenPixelScale);
								}
							}
						}
					}
					already_gen = true;
					data.clear();
					return;
				}
				data.clear();
			}
		}
	}

	bmp_loaded_wh[0] = 0;
	bmp_loaded_wh[1] = 0;
	props->GetValue("bOnSplineGen", is_SplineGen);
	props->GetValue("bOnShapeBorderGen", is_ShapeGen);
	if (is_SplineGen)
	{
		ReconstructSplineFromShape();
		GenObjectsOnShapeBorder();
		return;
	}
	else if (is_ShapeGen)
	{
		ReconstructSplineFromShape();
		GenObjectsOnShapeBorder();
		return;
	}

	if(!generation_setting_data_refilled)
		RefillGenerationSettingData();

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = "";
		brushes[i] = "";
		arc_entites[i] = "";
		vegetations[i] = "";
		surf_ids[i] = 0;
		veg_inds[i] = 0;
		brush_params[i] = 0;
	}
	

	bool decal_gen = false;
	bool brushes_gen = false;
	bool arc_ents_gen = false;
	bool terrain_surf_ids_gen = false;
	bool vegetation_gen = false;
	float vegetations_gen_coof = 0;
	float vegetations_base_scale = 0;
	float vegetations_rnd_scale = 0;
	float vegetations_gen_up_ht = 0;
	float vegetations_gen_rnd_pwr = 0;
	bool gen_vegetations_floating = false;
	bool gen_vegetations_rnd_rot_full = false;
	float decals_gen_coof = 0;
	int decals_aval_num = 0;
	float decals_base_scale = 0;
	float decals_rnd_scale = 0;
	float decals_gen_up_ht = 0;
	float decals_gen_rnd_pwr = 0;
	int gen_updwn = 0;
	float gen_up_from_nrm = 0;
	bool gen_arr_dist_points_for_all = false;
	bool gen_brushes_floating = false;
	bool gen_brushes_rnd_rot_full = false;
	float gen_brushes_base_scale = 0.0f;
	float gen_brushes_rnd_scale = 0.0f;
	float gen_brushes_up_hgt = 0.0f;
	float gen_brushes_rnd_pwr = 0.0f;
	float gen_brushes_coof = 0;
	float gen_arc_ent_coof = 0;
	float gen_surf_coof = 0;

	bool gen_arc_ent_floating = false;
	bool gen_arc_ent_rnd_rot_full = false;
	float gen_arc_ent_base_scale = 0.0f;
	float gen_arc_ent_rnd_scale = 0.0f;
	float gen_arc_ent_up_hgt = 0.0f;
	float gen_arc_ent_rnd_pwr = 0.0f;
	float gen_surf_rnd_pwr = 0.0f;

	if (!props)
		return;

	//-----------Slope&Height Setup-------------
	float fMinHeight = 0.0f;
	float fMaxHeight = 11111.0f;
	float fMinSlope = 0.0;
	float fMaxSlope = 180.0;
	props->GetValue("fMinHeight", fMinHeight);
	props->GetValue("fMaxHeight", fMaxHeight);
	props->GetValue("fMinSlope", fMinSlope);
	props->GetValue("fMaxSlope", fMaxSlope);
	//------------------------------------------

	props->GetValue("nGenUpDown", gen_updwn);
	//decals------------------------------
	props->GetValue("bGenerateDecals", decal_gen);
	props->GetValue("fDecalsGenCoof", decals_gen_coof);
	props->GetValue("fileDecalOne", decals[0]);
	props->GetValue("fileDecalTwo", decals[1]);
	props->GetValue("fileDecalThree", decals[2]);
	props->GetValue("fileDecalFour", decals[3]);
	props->GetValue("fileDecalFive", decals[4]);
	props->GetValue("fDecalsGenBaseScale", decals_base_scale);
	props->GetValue("fDecalsGenRandomScale", decals_rnd_scale);
	props->GetValue("fDecalsGenUpHeight", decals_gen_up_ht);
	props->GetValue("fDecalsGenRandPower", decals_gen_rnd_pwr);
	//---------------------------------
	props->GetValue("bUseOneOfGenPointsArrToDistrAllObj", gen_arr_dist_points_for_all);
	props->GetValue("fHeigthUpFromNormal", gen_up_from_nrm);
	//brushes--------------------------
	props->GetValue("bGenerateBrushes", brushes_gen);
	props->GetValue("objBrushOne", brushes[0]);
	props->GetValue("objBrushTwo", brushes[1]);
	props->GetValue("objBrushThree", brushes[2]);
	props->GetValue("objBrushFour", brushes[3]);
	props->GetValue("objBrushFive", brushes[4]);
	props->GetValue("nBrushOnePhyPrm", brush_params[0]);
	props->GetValue("nBrushTwoPhyPrm", brush_params[1]);
	props->GetValue("nBrushThreePhyPrm", brush_params[2]);
	props->GetValue("nBrushFourPhyPrm", brush_params[3]);
	props->GetValue("nBrushFivePhyPrm", brush_params[4]);
	props->GetValue("bBrushesGenFloating", gen_brushes_floating);
	props->GetValue("fBrushesGenCoof", gen_brushes_coof);
	props->GetValue("fBrushesGenBaseScale", gen_brushes_base_scale);
	props->GetValue("fBrushesGenRandomScale", gen_brushes_rnd_scale);
	props->GetValue("fBrushesGenUpHeight", gen_brushes_up_hgt);
	props->GetValue("fBrushesGenRandPower", gen_brushes_rnd_pwr);
	props->GetValue("bBrushesGenFullRandRotation", gen_brushes_rnd_rot_full);
	//---------------------------------
	//Archetype entites----------------
	props->GetValue("bGenerateArcEntites", arc_ents_gen);
	props->GetValue("bArcEntitesGenFloating", gen_arc_ent_floating);
	props->GetValue("sArcEntityOne", arc_entites[0]);
	props->GetValue("sArcEntityTwo", arc_entites[1]);
	props->GetValue("sArcEntityThree", arc_entites[2]);
	props->GetValue("sArcEntityFour", arc_entites[3]);
	props->GetValue("sArcEntityFive", arc_entites[4]);
	props->GetValue("fArcEntitesGenCoof", gen_arc_ent_coof);
	props->GetValue("fArcEntitesGenBaseScale", gen_arc_ent_base_scale);
	props->GetValue("fArcEntitesGenRandomScale", gen_arc_ent_rnd_scale);
	props->GetValue("fArcEntitesGenUpHeight", gen_arc_ent_up_hgt);
	props->GetValue("fArcEntitesGenRandPower", gen_arc_ent_rnd_pwr);
	props->GetValue("bArcEntitesGenFullRandRotation", gen_arc_ent_rnd_rot_full);
	//---------------------------------
	//terrain surfaces ids------------------
	props->GetValue("bGenerateTerrainSurfaceIds", terrain_surf_ids_gen);
	props->GetValue("nTerrainSurfaceIdOne", surf_ids[0]);
	props->GetValue("nTerrainSurfaceIdTwo", surf_ids[1]);
	props->GetValue("nTerrainSurfaceIdThree", surf_ids[2]);
	props->GetValue("nTerrainSurfaceIdFour", surf_ids[3]);
	props->GetValue("nTerrainSurfaceIdFive", surf_ids[4]);
	props->GetValue("nTerrainSurfaceGenCoof", gen_surf_coof);
	props->GetValue("fTerrainSurfaceGenRandPower", gen_surf_rnd_pwr);
	//---------------------------------
	//vegetations------------------
	props->GetValue("bGenerateVegetations", vegetation_gen);
	props->GetValue("objVegetationOne", vegetations[0]);
	props->GetValue("objVegetationTwo", vegetations[1]);
	props->GetValue("objVegetationThree", vegetations[2]);
	props->GetValue("objVegetationFour", vegetations[3]);
	props->GetValue("objVegetationFive", vegetations[4]);
	props->GetValue("fVegetationIndexOne", veg_inds[0]);
	props->GetValue("fVegetationIndexTwo", veg_inds[1]);
	props->GetValue("fVegetationIndexThree", veg_inds[2]);
	props->GetValue("fVegetationIndexFour", veg_inds[3]);
	props->GetValue("fVegetationIndexFive", veg_inds[4]);
	props->GetValue("bVegetationsGenFloating", gen_vegetations_floating);
	props->GetValue("fVegetationsGenCoof", vegetations_gen_coof);
	props->GetValue("fVegetationsGenBaseScale", vegetations_base_scale);
	props->GetValue("fVegetationsGenRandomScale", vegetations_rnd_scale);
	props->GetValue("fVegetationsGenUpHeight", vegetations_gen_up_ht);
	props->GetValue("fVegetationsGenRandPower", vegetations_gen_rnd_pwr);
	props->GetValue("bVegetationsGenFullRandRotation", gen_vegetations_rnd_rot_full);
	bool veg_atm = false;
	float veg_bending = 0.0f;
	props->GetValue("fVegetationsBending", veg_bending);
	props->GetValue("bVegetationsAutoMerged", veg_atm);
	//---------------------------------
	float z_gen_rot_rand_val = 0.0f;
	props->GetValue("fGenZRotationRandomVal", z_gen_rot_rand_val);

	int obj_view_params[2];
	for (int i = 0; i < 2; i++)
	{
		obj_view_params[i] = 0;
	}
	props->GetValue("fGenObjectsViewDistance", obj_view_params[0]);
	props->GetValue("fGenObjectsLodDistance", obj_view_params[1]);
	bool gen_olly_on_terrain = false;
	props->GetValue("bGenOnTerrainOnly", gen_olly_on_terrain);
	int gen_on_stat_objs = ent_static;
	if (gen_olly_on_terrain)
		gen_on_stat_objs = 0;

	float min_dist_gen_old = 0.0f;
	props->GetValue("fMinDistToOldGenObjs", min_dist_gen_old);

	float min_dist_gen_old_rnd = 0.0f;
	props->GetValue("fMinDistToOldGenObjsRandom", min_dist_gen_old_rnd);
	float normal_rot_mult_rnd = 0.0f;
	props->GetValue("fRotToNormalRandom", normal_rot_mult_rnd);

	float normal_rot_mult_x = 1.0f;
	props->GetValue("fRotToNormalMlt", normal_rot_mult_x);
	normal_rot_mult_x = min(1.0f, normal_rot_mult_x);

	for (int i = 0; i < 2; i++)
	{
		if (obj_view_params[i] > 255)
			obj_view_params[i] = 255;
	}

	for (int i = 0; i < 5; i++)
	{
		if (!decals[i].empty())
		{
			decals_aval_num += 1;
		}
	}


	IGameVolumes::VolumeInfo volumeInfo;
	if (!GetVolumeInfoForEntityGen(GetEntityId(), &volumeInfo))
		return;

	AABB genBounds;
	GetEntity()->GetWorldBounds(genBounds);
	Matrix34 mat = GetEntity()->GetWorldTM();
	mat.SetScale(Vec3(1.0f));
	mat.SetTranslation(GetEntity()->GetWorldPos());
	uint32 vertexCount = volumeInfo.verticesCount;
	float volumeHeight = volumeInfo.volumeHeight;
	float dist = 0.0f;
	int vertex_last = 1;
	genBounds.min = mat.TransformPoint(volumeInfo.pVertices[0]);
	std::vector<Vec3> poli_shape;
	poli_shape.clear();
	poli_shape.push_back(genBounds.min);
	for (uint32 i = 1; i < vertexCount; ++i)
	{
		Vec3 v1 = volumeInfo.pVertices[i];
		poli_shape.push_back(mat.TransformPoint(v1));
		float dv = v1.GetDistance(volumeInfo.pVertices[0]);
		if (dist < dv)
		{
			dist = dv;
			vertex_last = i;
		}
	}
	genBounds.max = mat.TransformPoint(volumeInfo.pVertices[vertex_last]) + Vec3(0, 0, volumeHeight);
	dist = genBounds.min.GetDistance(genBounds.max)*2.0f;
	dist *= 1.3f;
	std::vector<Matrix34> gen_points;
	gen_points.clear();

	float z_bouds = genBounds.min.z;
	if (z_bouds < genBounds.max.z)
	{
		z_bouds = genBounds.max.z;
	}

	//TriangulatePolygon2D();

	if (gen_arr_dist_points_for_all)
	{
		for (int ic = 0; ic < int(gen_brushes_coof*dist); ic++)
		{
			Vec3 pnt = GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);
			//CryLogAlways("CProceduralGenerator decal pnt = x- %f y- %f z- %f", pnt.x, pnt.y, pnt.z);

			if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
			{
				ray_hit hit;
				int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

				bool under_terrain = false;
				if (n <= 0)
				{
					n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
						rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

					under_terrain = true;
				}

				if (!gen_brushes_floating)
				{
					if (n > 0)
					{
						IRenderNode *pNode = NULL;
						pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
						pNode->SetRndFlags(ERF_PROCEDURAL, true);
						Matrix34 mat_2 = mat;
						Vec3 pss = hit.pt;
						pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
						mat_2.SetTranslation(pss);
						Vec3 nrm = hit.n;
						if (under_terrain)
							nrm = -nrm;

						Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
						float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
						if (gen_brushes_rnd_rot_full)
						{
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
						}

						mat_2.Set(Vec3(1.00f), projected_orientation, pss);
						gen_points.push_back(mat_2);
					}
				}
				else
				{
					IRenderNode *pNode = NULL;
					pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
					pNode->SetRndFlags(ERF_PROCEDURAL, true);
					Matrix34 mat_2 = mat;
					Vec3 pss = pnt;
					pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
					mat_2.SetTranslation(pss);
					Vec3 nrm = Vec3(0,0,1);

					Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
					float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
					projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
					if (gen_brushes_rnd_rot_full)
					{
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
					}

					mat_2.Set(Vec3(1.00f), projected_orientation, pss);
					gen_points.push_back(mat_2);
				}
			}
		}
	}

	if (decal_gen && decals_aval_num > 0)
	{
		for (int i = 0; i < 5; i++)
		{
			if (!decals[i].empty())
			{
				if (!gen_arr_dist_points_for_all)
				{
					for (int ic = 0; ic < int(decals_gen_coof*dist); ic++)
					{
						Vec3 pnt = GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);
						//CryLogAlways("CProceduralGenerator decal pnt = x- %f y- %f z- %f", pnt.x, pnt.y, pnt.z);

						if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
						{
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								under_terrain = true;
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								Vec3 pss = hit.pt;
								pss.z += 0.1f;
								SDecalProperties decalProperties;
								decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
								mat.CreateIdentity();
								mat.SetTranslation(pss);
								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								const Matrix34& wtm(mat);
								Vec3 x(wtm.GetColumn0().GetNormalized());
								Vec3 y(wtm.GetColumn1().GetNormalized());
								Vec3 z(nrm.GetNormalized());

								y = z.Cross(x);
								if (y.GetLengthSquared() < 1e-4f)
									y = z.GetOrthogonal();
								y.Normalize();
								x = y.Cross(z);

								Matrix33 newOrient;
								newOrient.SetColumn(0, x);
								newOrient.SetColumn(1, y);
								newOrient.SetColumn(2, z);
								Quat q(newOrient);
								float angle(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								q= (q * Quat(Matrix33::CreateRotationZ(angle)));

								mat.Set(Vec3(1.00f), q, pss);

								decalProperties.m_pos = pss;
								decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
								Matrix33 rotation(mat);
								rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
								rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
								rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

								//rotation.SetScale(Vec3(0.05f));
								float add_rads = 0.0f;
								if (decals_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
								}

								decalProperties.m_pMaterialName = decals[i];
								decalProperties.m_radius = decals_base_scale + add_rads;
								decalProperties.m_explicitRightUpFront = rotation;
								decalProperties.m_sortPrio = cry_random(1, 150);
								decalProperties.m_deferred = true;
								decalProperties.m_depth = cry_random(0.5f, 3.0f);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
								pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

								IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
								pRenderNode->SetDecalProperties(decalProperties);
								if (pNode)
								{
									pNode->SetMatrix(mat);
									pNode->SetViewDistRatio(obj_view_params[0]);
									gEnv->p3DEngine->RegisterEntity(pNode);
									succesfully_gen_oblects.push_back(pNode);
								}
								//CryLogAlways("CProceduralGenerator decal creation called");
							}
						}
					}
				}
				else
				{
					std::vector<Matrix34>::const_iterator it = gen_points.begin();
					std::vector<Matrix34>::const_iterator end = gen_points.end();
					int count = gen_points.size();
					if (count > 0)
					{
						for (int i = 0; i < count, it != end; i++, ++it)
						{
							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
								Matrix34 tm = (*it);
								if (que_obj.object_scale != Vec3(1, 1, 1))
									tm.OrthonormalizeFast();

								Ang3 angles = Ang3::GetAnglesXYZ(tm);
								Quat Rot = Quat::CreateRotationXYZ(angles);

								que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
								que_obj.object_rot = Rot;

								que_obj.object_slot = i;
								que_obj.object_type = 2;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = false;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
								Matrix34 tm = (*it);
								if (que_obj.object_scale != Vec3(1, 1, 1))
									tm.OrthonormalizeFast();

								Ang3 angles = Ang3::GetAnglesXYZ(tm);
								Quat Rot = Quat::CreateRotationXYZ(angles);

								que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
								que_obj.object_rot = Rot;

								que_obj.object_slot = i;
								que_obj.object_type = 2;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = false;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}
							IRenderNode *pNode = NULL;
							pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
							pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

							SDecalProperties decalProperties;
							decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
							decalProperties.m_pos = (*it).TransformPoint(Vec3(0, 0, 0));
							decalProperties.m_normal = (*it).TransformPoint(Vec3(0, 0, 1));
							Matrix33 rotation((*it));
							rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
							rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
							rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

							float add_rads = 0.0f;
							if (decals_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
							}

							decalProperties.m_pMaterialName = decals[i];
							decalProperties.m_radius = decals_base_scale + add_rads;
							decalProperties.m_explicitRightUpFront = rotation;
							decalProperties.m_sortPrio = cry_random(1, 150);
							decalProperties.m_deferred = true;
							decalProperties.m_depth = cry_random(0.5f, 3.0f);

							IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
							pRenderNode->SetDecalProperties(decalProperties);
							if (pNode)
							{
								pNode->SetMatrix((*it));
								pNode->SetViewDistRatio(obj_view_params[0]);
								gEnv->p3DEngine->RegisterEntity(pNode);
								succesfully_gen_oblects.push_back(pNode);
							}
						}
					}
				}
			}
		}
	}

	if (brushes_gen)
	{
		for (int i = 0; i < 5; i++)
		{
			if (!brushes[i].empty())
			{
				std::vector<Matrix34>::const_iterator it = gen_points.begin();
				std::vector<Matrix34>::const_iterator end = gen_points.end();
				int count = gen_points.size();
				for (int ic = 0; ic < int(gen_brushes_coof*dist); ic++)
				{
					if (count > 0 && ic < count)
					{
						Matrix34 idtn_mtx;
						idtn_mtx.CreateIdentity();
						if (it != end)
						{
							if (gen_arr_dist_points_for_all)
							{
								idtn_mtx = (*it);
								float add_rads = 0.0f;
								if (gen_brushes_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
								}

								Vec3 nrm = Vec3(0,0,1);
								Vec3 pss = idtn_mtx.GetTranslation();
								Quat projected_orientation = GenerateObjOrientation(idtn_mtx, nrm);
								float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
								if (gen_brushes_rnd_rot_full)
								{
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
								}
								idtn_mtx.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									gen_objects_queue.push(que_obj);
									++it;
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									++it;
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
								IStatObj *pObj = NULL;
								pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);
								if (pNode)
								{
									IBrush *pBrush = (IBrush*)pNode;
									if (pBrush && pObj)
									{
										pObj->AddRef();
										AABB box2 = pObj->GetAABB();
										box2.SetTransformedAABB(idtn_mtx, box2);
										pBrush->SetBBox(box2);
										pBrush->SetMaterial(0);
										pBrush->OffsetPosition(Vec3(ZERO));
										pBrush->SetEntityStatObj(0, pObj, 0);
										pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
										pBrush->SetLodRatio(obj_view_params[1]);
										pBrush->SetViewDistRatio(obj_view_params[0]);
										if(brush_params[i] == 1)
											pBrush->Physicalize();

										pBrush->SetBBox(box2);
										gEnv->p3DEngine->RegisterEntity(pBrush);
										pBrush->SetMatrix(idtn_mtx);
										pBrush->SetBBox(box2);
										pBrush->SetDrawLast(false);
										if (brush_params[i] == 0)
											pBrush->Dephysicalize();
									}
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
						++it;

						if (gen_arr_dist_points_for_all)
							continue;
					}

					if (!gen_brushes_floating)
					{
						Vec3 pnt = GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);

						//CryLogAlways("CProceduralGenerator decal pnt = x- %f y- %f z- %f", pnt.x, pnt.y, pnt.z);

						if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
						{
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								if (n > 0)
								{
									under_terrain = true;
								}
							}

							if (gen_updwn == 1 && under_terrain)
							{
								pnt.z = pnt.z + cry_random(5.0f, 8.0f);
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								float add_rads = 0.0f;
								if (gen_brushes_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
								}

								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								Vec3 pss = hit.pt;
								pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
								mat.CreateIdentity();
								mat.SetTranslation(pss);

								Quat projected_orientation = GenerateObjOrientation(mat, nrm);
								float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
								if (gen_brushes_rnd_rot_full)
								{
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
								}
								mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;

								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
								IStatObj *pObj = NULL;
								pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

								if (pNode)
								{
									IBrush *pBrush = (IBrush*)pNode;
									if (pBrush && pObj)
									{
										pObj->AddRef();
										AABB box2 = pObj->GetAABB();
										box2.SetTransformedAABB(mat, box2);
										pBrush->SetBBox(box2);
										pBrush->SetMaterial(0);

										pBrush->OffsetPosition(Vec3(ZERO));
										pBrush->SetEntityStatObj(0, pObj, 0);
										//1610612744
										//pBrush->SetRndFlags(ERF_HAS_CASTSHADOWMAPS | ERF_CASTSHADOWMAPS | ERF_PROCEDURAL/* | ERF_RENDER_ALWAYS*/ | ERF_REGISTER_BY_POSITION | ERF_STATIC_INSTANCING, true);
										pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
										pBrush->SetLodRatio(obj_view_params[1]);
										pBrush->SetViewDistRatio(obj_view_params[0]);
										if (brush_params[i] == 1)
											pBrush->Physicalize();

										pBrush->SetBBox(box2);
										gEnv->p3DEngine->RegisterEntity(pBrush);
										pBrush->SetMatrix(mat);
										pBrush->SetBBox(box2);
										pBrush->SetDrawLast(false);
										if (brush_params[i] == 0)
											pBrush->Dephysicalize();
										//CryLogAlways("brushes_gen brush created and registred");
									}
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
					}
					else
					{
						Vec3 pnt = GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);

						//CryLogAlways("CProceduralGenerator decal pnt = x- %f y- %f z- %f", pnt.x, pnt.y, pnt.z);

						if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
						{
							float add_rads = 0.0f;
							if (gen_brushes_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
							}
							Vec3 nrm = Vec3(0, 0, 1);


							Vec3 pss = pnt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_arc_ent_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IRenderNode *pNode = NULL;
							pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
							IStatObj *pObj = NULL;
							pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

							if (pNode)
							{
								IBrush *pBrush = (IBrush*)pNode;
								if (pBrush && pObj)
								{
									pObj->AddRef();
									AABB box2 = pObj->GetAABB();
									box2.SetTransformedAABB(mat, box2);
									pBrush->SetBBox(box2);
									pBrush->SetMaterial(0);
									pBrush->OffsetPosition(Vec3(ZERO));
									pBrush->SetEntityStatObj(0, pObj, 0);
									pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
									pBrush->SetLodRatio(obj_view_params[1]);
									pBrush->SetViewDistRatio(obj_view_params[0]);
									if (brush_params[i] == 1)
										pBrush->Physicalize();

									pBrush->SetBBox(box2);
									gEnv->p3DEngine->RegisterEntity(pBrush);
									pBrush->SetMatrix(mat);
									pBrush->SetBBox(box2);
									pBrush->SetDrawLast(false);
									if (brush_params[i] == 0)
										pBrush->Dephysicalize();
								}
								succesfully_gen_oblects.push_back(pNode);
							}
						}
					}
				}
			}
		}
	}

	if (arc_ents_gen)
	{
		for (int i = 0; i < 5; i++)
		{
			if (!arc_entites[i].empty())
			{
				for (int ic = 0; ic < int(gen_arc_ent_coof*dist); ic++)
				{
					Vec3 pnt = GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);

					if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
					{
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							string nmn_generated = "Arc_ent_generated_c_x_";
							nmn_generated = GenerateNodeXName(nmn_generated);

							float add_rads = 0.0f;
							if (gen_arc_ent_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_arc_ent_rnd_scale, gen_arc_ent_rnd_scale);
							}
							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_brushes_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_arc_ent_base_scale + add_rads), projected_orientation, pss);

							IEntity* pEntity = NULL;
							SEntitySpawnParams params;
							IEntityArchetype* pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(arc_entites[i]);
							if (NULL != pArchetype)
							{
								params.nFlags = ENTITY_FLAG_SPAWNED;
								params.pArchetype = pArchetype;
								params.sName = nmn_generated;
								params.vPosition = mat.GetTranslation();
								params.vScale = Vec3(gen_arc_ent_base_scale + add_rads);

								
								params.qRotation = projected_orientation;

								// Create
								int nCastShadowMinSpec = 0;
								if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
								{
									objectVars->getAttr("CastShadowMinSpec", nCastShadowMinSpec);

									static ICVar* pObjShadowCastSpec = gEnv->pConsole->GetCVar("e_ObjShadowCastSpec");
									if (nCastShadowMinSpec <= pObjShadowCastSpec->GetIVal())
										params.nFlags |= ENTITY_FLAG_CASTSHADOW;

									bool bRecvWind;
									objectVars->getAttr("RecvWind", bRecvWind);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_RECVWIND;

									bool bOutdoorOnly;
									objectVars->getAttr("OutdoorOnly", bOutdoorOnly);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_OUTDOORONLY;

									bool bNoStaticDecals;
									objectVars->getAttr("NoStaticDecals", bNoStaticDecals);
									if (bNoStaticDecals)
										params.nFlags |= ENTITY_FLAG_NO_DECALNODE_DECALS;
								}

								pEntity = gEnv->pEntitySystem->SpawnEntity(params);
								if (pEntity)
								{
									succesfully_gen_entites.push_back(pEntity->GetId());
									IEntityRender* pRenderProx = pEntity->GetRenderInterface();
									if (pRenderProx && pRenderProx->GetRenderNode())
									{
										if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
										{
											int nViewDistRatio = 100;
											objectVars->getAttr("ViewDistRatio", nViewDistRatio);
											pRenderProx->SetViewDistRatio(nViewDistRatio);

											int nLodRatio = 100;
											objectVars->getAttr("lodRatio", nLodRatio);
											pRenderProx->SetLodRatio(nLodRatio);
										}
									}

									if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
									{
										bool bHiddenInGame = false;
										objectVars->getAttr("HiddenInGame", bHiddenInGame);
										if (bHiddenInGame)
											pEntity->Hide(true);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if (vegetation_gen)
	{
		int old_iter = 0;
		ITerrain* pterrain = gEnv->p3DEngine->GetITerrain();
		for (int i = 0; i < 5; i++)
		{
			if (!pterrain)
				break;

			if (!vegetations[i].empty())
			{
				IStatInstGroup Group;
				if (veg_inds[i] <= 0)
				{
					int ctfn = GetVegetationGrpId(vegetations[i]);
					if (ctfn >= 0)
					{
						veg_inds[i] = ctfn;
					}
				}

				if(veg_inds[i] <= -1)
					continue;

				gEnv->p3DEngine->GetStatInstGroup(veg_inds[i], Group);
				for (int ic = 0; ic < int(vegetations_gen_coof*dist); ic++)
				{
					Vec3 pnt = GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);

					//CryLogAlways("CProceduralGenerator vegetation pnt = x- %f y- %f z- %f", pnt.x, pnt.y, pnt.z);

					if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
					{
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							float add_rads = 0.0f;
							if (vegetations_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-vegetations_rnd_scale, vegetations_rnd_scale);
							}

							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);

							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_vegetations_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(vegetations_base_scale + add_rads), projected_orientation, pss);
							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IVegetation *pBrush = nullptr;
							uint8 ang_veg[3];
							Ang3 orient_xc = Ang3(projected_orientation);
							ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
							ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
							ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
							pBrush = (IVegetation*)pterrain->AddVegetationInstance(veg_inds[i], pss, vegetations_base_scale + add_rads, 1, ang_veg[0], ang_veg[1], ang_veg[2]);

							if (pBrush)
							{
								pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
								if(!Group.bAutoMerged)
									succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
							}
						}
					}
				}
			}
		}
	}

	if (terrain_surf_ids_gen)
	{
		
	}

	already_gen = true;
}

void CProceduralGenerator::GenObjectsAtTerrainPoint(Vec2 point, float genPower, float cellScale)
{
	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	ITerrain* pterrain = gEnv->p3DEngine->GetITerrain();
	std::vector<Matrix34> gen_points;
	gen_points.clear();

	Matrix34 mat = GetEntity()->GetWorldTM();
	mat.SetScale(Vec3(1.0f));
	mat.SetTranslation(GetEntity()->GetWorldPos());

	if (!generation_setting_data_refilled)
		RefillGenerationSettingData();

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = "";
		brushes[i] = "";
		arc_entites[i] = "";
		vegetations[i] = "";
		surf_ids[i] = 0;
		veg_inds[i] = 0;
		brush_params[i] = 0;
	}


	bool decal_gen = false;
	bool brushes_gen = false;
	bool arc_ents_gen = false;
	bool terrain_surf_ids_gen = false;
	bool vegetation_gen = false;
	float vegetations_gen_coof = 0;
	float vegetations_base_scale = 0;
	float vegetations_rnd_scale = 0;
	float vegetations_gen_up_ht = 0;
	float vegetations_gen_rnd_pwr = 0;
	bool gen_vegetations_floating = false;
	bool gen_vegetations_rnd_rot_full = false;
	float decals_gen_coof = 0;
	int decals_aval_num = 0;
	float decals_base_scale = 0;
	float decals_rnd_scale = 0;
	float decals_gen_up_ht = 0;
	float decals_gen_rnd_pwr = 0;
	int gen_updwn = 0;
	float gen_up_from_nrm = 0;
	bool gen_arr_dist_points_for_all = false;
	bool gen_brushes_floating = false;
	bool gen_brushes_rnd_rot_full = false;
	float gen_brushes_base_scale = 0.0f;
	float gen_brushes_rnd_scale = 0.0f;
	float gen_brushes_up_hgt = 0.0f;
	float gen_brushes_rnd_pwr = 0.0f;
	float gen_brushes_coof = 0;
	float gen_arc_ent_coof = 0;
	float gen_surf_coof = 0;

	bool gen_arc_ent_floating = false;
	bool gen_arc_ent_rnd_rot_full = false;
	float gen_arc_ent_base_scale = 0.0f;
	float gen_arc_ent_rnd_scale = 0.0f;
	float gen_arc_ent_up_hgt = 0.0f;
	float gen_arc_ent_rnd_pwr = 0.0f;
	float gen_surf_rnd_pwr = 0.0f;

	if (!props)
		return;

	//int sector_size = 128;
	props->GetValue("fStreamingSectorSize", sector_size);

	bool UseCameraDirectonInPaintMode = false;
	props->GetValue("bUseCameraDirectonInPaintMode", UseCameraDirectonInPaintMode);

	//-----------Slope&Height Setup-------------
	float fMinHeight = 0.0f;
	float fMaxHeight = 11111.0f;
	float fMinSlope = 0.0;
	float fMaxSlope = 180.0;
	props->GetValue("fMinHeight", fMinHeight);
	props->GetValue("fMaxHeight", fMaxHeight);
	props->GetValue("fMinSlope", fMinSlope);
	props->GetValue("fMaxSlope", fMaxSlope);
	//------------------------------------------

	props->GetValue("nGenUpDown", gen_updwn);
	//decals------------------------------
	props->GetValue("bGenerateDecals", decal_gen);
	props->GetValue("fDecalsGenCoof", decals_gen_coof);
	props->GetValue("fileDecalOne", decals[0]);
	props->GetValue("fileDecalTwo", decals[1]);
	props->GetValue("fileDecalThree", decals[2]);
	props->GetValue("fileDecalFour", decals[3]);
	props->GetValue("fileDecalFive", decals[4]);
	props->GetValue("fDecalsGenBaseScale", decals_base_scale);
	props->GetValue("fDecalsGenRandomScale", decals_rnd_scale);
	props->GetValue("fDecalsGenUpHeight", decals_gen_up_ht);
	props->GetValue("fDecalsGenRandPower", decals_gen_rnd_pwr);
	//---------------------------------
	props->GetValue("bUseOneOfGenPointsArrToDistrAllObj", gen_arr_dist_points_for_all);
	props->GetValue("fHeigthUpFromNormal", gen_up_from_nrm);
	//brushes--------------------------
	props->GetValue("bGenerateBrushes", brushes_gen);
	props->GetValue("objBrushOne", brushes[0]);
	props->GetValue("objBrushTwo", brushes[1]);
	props->GetValue("objBrushThree", brushes[2]);
	props->GetValue("objBrushFour", brushes[3]);
	props->GetValue("objBrushFive", brushes[4]);
	props->GetValue("nBrushOnePhyPrm", brush_params[0]);
	props->GetValue("nBrushTwoPhyPrm", brush_params[1]);
	props->GetValue("nBrushThreePhyPrm", brush_params[2]);
	props->GetValue("nBrushFourPhyPrm", brush_params[3]);
	props->GetValue("nBrushFivePhyPrm", brush_params[4]);
	props->GetValue("bBrushesGenFloating", gen_brushes_floating);
	props->GetValue("fBrushesGenCoof", gen_brushes_coof);
	props->GetValue("fBrushesGenBaseScale", gen_brushes_base_scale);
	props->GetValue("fBrushesGenRandomScale", gen_brushes_rnd_scale);
	props->GetValue("fBrushesGenUpHeight", gen_brushes_up_hgt);
	props->GetValue("fBrushesGenRandPower", gen_brushes_rnd_pwr);
	props->GetValue("bBrushesGenFullRandRotation", gen_brushes_rnd_rot_full);
	//---------------------------------
	//Archetype entites----------------
	props->GetValue("bGenerateArcEntites", arc_ents_gen);
	props->GetValue("bArcEntitesGenFloating", gen_arc_ent_floating);
	props->GetValue("sArcEntityOne", arc_entites[0]);
	props->GetValue("sArcEntityTwo", arc_entites[1]);
	props->GetValue("sArcEntityThree", arc_entites[2]);
	props->GetValue("sArcEntityFour", arc_entites[3]);
	props->GetValue("sArcEntityFive", arc_entites[4]);
	props->GetValue("fArcEntitesGenCoof", gen_arc_ent_coof);
	props->GetValue("fArcEntitesGenBaseScale", gen_arc_ent_base_scale);
	props->GetValue("fArcEntitesGenRandomScale", gen_arc_ent_rnd_scale);
	props->GetValue("fArcEntitesGenUpHeight", gen_arc_ent_up_hgt);
	props->GetValue("fArcEntitesGenRandPower", gen_arc_ent_rnd_pwr);
	props->GetValue("bArcEntitesGenFullRandRotation", gen_arc_ent_rnd_rot_full);
	//---------------------------------
	//terrain surfaces ids------------------
	props->GetValue("bGenerateTerrainSurfaceIds", terrain_surf_ids_gen);
	props->GetValue("nTerrainSurfaceIdOne", surf_ids[0]);
	props->GetValue("nTerrainSurfaceIdTwo", surf_ids[1]);
	props->GetValue("nTerrainSurfaceIdThree", surf_ids[2]);
	props->GetValue("nTerrainSurfaceIdFour", surf_ids[3]);
	props->GetValue("nTerrainSurfaceIdFive", surf_ids[4]);
	props->GetValue("nTerrainSurfaceGenCoof", gen_surf_coof);
	props->GetValue("fTerrainSurfaceGenRandPower", gen_surf_rnd_pwr);
	//---------------------------------
	//vegetations------------------
	props->GetValue("bGenerateVegetations", vegetation_gen);
	props->GetValue("objVegetationOne", vegetations[0]);
	props->GetValue("objVegetationTwo", vegetations[1]);
	props->GetValue("objVegetationThree", vegetations[2]);
	props->GetValue("objVegetationFour", vegetations[3]);
	props->GetValue("objVegetationFive", vegetations[4]);
	props->GetValue("fVegetationIndexOne", veg_inds[0]);
	props->GetValue("fVegetationIndexTwo", veg_inds[1]);
	props->GetValue("fVegetationIndexThree", veg_inds[2]);
	props->GetValue("fVegetationIndexFour", veg_inds[3]);
	props->GetValue("fVegetationIndexFive", veg_inds[4]);
	props->GetValue("bVegetationsGenFloating", gen_vegetations_floating);
	props->GetValue("fVegetationsGenCoof", vegetations_gen_coof);
	props->GetValue("fVegetationsGenBaseScale", vegetations_base_scale);
	props->GetValue("fVegetationsGenRandomScale", vegetations_rnd_scale);
	props->GetValue("fVegetationsGenUpHeight", vegetations_gen_up_ht);
	props->GetValue("fVegetationsGenRandPower", vegetations_gen_rnd_pwr);
	props->GetValue("bVegetationsGenFullRandRotation", gen_vegetations_rnd_rot_full);
	bool veg_atm = false;
	float veg_bending = 0.0f;
	props->GetValue("fVegetationsBending", veg_bending);
	props->GetValue("bVegetationsAutoMerged", veg_atm);
	//---------------------------------
	float z_gen_rot_rand_val = 0.0f;
	props->GetValue("fGenZRotationRandomVal", z_gen_rot_rand_val);

	int obj_view_params[2];
	for (int i = 0; i < 2; i++)
	{
		obj_view_params[i] = 0;
	}
	props->GetValue("fGenObjectsViewDistance", obj_view_params[0]);
	props->GetValue("fGenObjectsLodDistance", obj_view_params[1]);
	bool gen_olly_on_terrain = false;
	props->GetValue("bGenOnTerrainOnly", gen_olly_on_terrain);
	int gen_on_stat_objs = ent_static;
	if (gen_olly_on_terrain)
		gen_on_stat_objs = 0;

	float min_dist_gen_old = 0.0f;
	props->GetValue("fMinDistToOldGenObjs", min_dist_gen_old);

	float min_dist_gen_old_rnd = 0.0f;
	props->GetValue("fMinDistToOldGenObjsRandom", min_dist_gen_old_rnd);
	float normal_rot_mult_rnd = 0.0f;
	props->GetValue("fRotToNormalRandom", normal_rot_mult_rnd);

	float normal_rot_mult_x = 1.0f;
	props->GetValue("fRotToNormalMlt", normal_rot_mult_x);
	normal_rot_mult_x = min(1.0f, normal_rot_mult_x);

	for (int i = 0; i < 2; i++)
	{
		if (obj_view_params[i] > 255)
			obj_view_params[i] = 255;
		else if (obj_view_params[i] < 0)
			obj_view_params[i] = 0;
	}

	for (int i = 0; i < 5; i++)
	{
		if (!decals[i].empty())
		{
			decals_aval_num += 1;
		}
	}

	AABB genBounds;
	float dist = 0.0f;
	float terr_elev = 0.0f;
	Vec3 wrld_pos = GetEntity()->GetWorldPos();
	Vec3 conv_point = Vec3(wrld_pos.x + point.x, wrld_pos.y + point.y, 0.0f);
	terr_elev = gEnv->p3DEngine->GetTerrainElevation(conv_point.x, conv_point.y);
	conv_point.z = terr_elev + gen_brushes_up_hgt/2.0f;
	Vec3 scalx = Vec3(0.5f * cellScale, 0.5f * cellScale, 0);
	genBounds.min = conv_point - scalx;
	conv_point.z = conv_point.z + cry_random(-(gen_brushes_up_hgt / 2.5f), gen_brushes_up_hgt / 2.5f);
	genBounds.max = conv_point + scalx;
	dist = Vec2(genBounds.min).GetDistance(Vec2(genBounds.max));
	float z_bouds = genBounds.min.z;
	if (z_bouds < genBounds.max.z)
	{
		z_bouds = genBounds.max.z;
	}
	//float gen_prw_x = genPower *0.1f;
	//vegetations_gen_coof *= cry_random(0.0f, gen_prw_x);
	//decals_gen_coof *= cry_random(0.0f, gen_prw_x);
	//gen_arc_ent_coof *= cry_random(0.0f, gen_prw_x);
	//gen_brushes_coof *= cry_random(0.0f, gen_prw_x);
	float gen_chance = cry_random(0.0f, genPower);

	if (gen_arr_dist_points_for_all)
	{
		for (int ic = 0; ic < int(gen_brushes_coof*dist); ic++)
		{
			float ntcg = cry_random(0.0f, 100.0f);
			if (ntcg >= gen_chance)
			{
				continue;
			}
			Vec3 pnt = GenerateVecRandomValue(genBounds.min, genBounds.max);
			if (pnt.IsValid())
			{
				ray_hit hit;
				int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

				bool under_terrain = false;
				if (n <= 0)
				{
					n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
						rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

					under_terrain = true;
				}

				if (!gen_brushes_floating)
				{
					if (n > 0)
					{
						if (min_dist_gen_old > 0.0f)
						{
							float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
							if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
							{
								continue;
							}
						}

						if (normal_rot_mult_x < 1.0f)
						{
							float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
							hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
						}

						IRenderNode *pNode = NULL;
						pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
						pNode->SetRndFlags(ERF_PROCEDURAL, true);
						Matrix34 mat_2 = mat;
						Vec3 pss = hit.pt;
						pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
						mat_2.SetTranslation(pss);
						Vec3 nrm = hit.n;
						if (under_terrain)
							nrm = -nrm;

						if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
						{
							continue;
						}

						Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
						float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
						if (gen_brushes_rnd_rot_full)
						{
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
						}

						mat_2.Set(Vec3(1.00f), projected_orientation, pss);
						gen_points.push_back(mat_2);
					}
				}
				else
				{
					IRenderNode *pNode = NULL;
					pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
					pNode->SetRndFlags(ERF_PROCEDURAL, true);
					Matrix34 mat_2 = mat;
					Vec3 pss = pnt;
					pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
					mat_2.SetTranslation(pss);
					Vec3 nrm = Vec3(0, 0, 1);

					Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
					float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
					projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
					if (gen_brushes_rnd_rot_full)
					{
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
					}

					mat_2.Set(Vec3(1.00f), projected_orientation, pss);
					gen_points.push_back(mat_2);
				}
			}
		}
	}

	if (decal_gen && decals_aval_num > 0)
	{
		for (int i = 0; i < 5; i++)
		{
			if (!decals[i].empty())
			{
				if (cry_random(0.0f, 99.0f) >= decals_gen_rnd_pwr)
				{
					continue;
				}

				if (!gen_arr_dist_points_for_all)
				{
					for (int ic = 0; ic < int(decals_gen_coof*dist); ic++)
					{
						float ntcg= cry_random(0.0f, 100.0f);
						if (ntcg >= gen_chance)
						{
							continue;
						}
						Vec3 pnt = GenerateVecRandomValue(genBounds.min, genBounds.max);
						if (pnt.IsValid())
						{
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								under_terrain = true;
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								Vec3 pss = hit.pt;
								pss.z += 0.1f;
								SDecalProperties decalProperties;
								decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
								mat.CreateIdentity();
								mat.SetTranslation(pss);
								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								const Matrix34& wtm(mat);
								Vec3 x(wtm.GetColumn0().GetNormalized());
								Vec3 y(wtm.GetColumn1().GetNormalized());
								Vec3 z(nrm.GetNormalized());

								y = z.Cross(x);
								if (y.GetLengthSquared() < 1e-4f)
									y = z.GetOrthogonal();
								y.Normalize();
								x = y.Cross(z);

								Matrix33 newOrient;
								newOrient.SetColumn(0, x);
								newOrient.SetColumn(1, y);
								newOrient.SetColumn(2, z);
								Quat q(newOrient);
								float angle(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								q = (q * Quat(Matrix33::CreateRotationZ(angle)));

								mat.Set(Vec3(1.00f), q, pss);

								decalProperties.m_pos = pss;
								decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
								Matrix33 rotation(mat);
								rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
								rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
								rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

								//rotation.SetScale(Vec3(0.05f));
								float add_rads = 0.0f;
								if (decals_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
								}

								decalProperties.m_pMaterialName = decals[i];
								decalProperties.m_radius = decals_base_scale + add_rads;
								decalProperties.m_explicitRightUpFront = rotation;
								decalProperties.m_sortPrio = cry_random(1, 150);
								decalProperties.m_deferred = true;
								decalProperties.m_depth = cry_random(0.5f, 3.0f);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
								pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

								IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
								pRenderNode->SetDecalProperties(decalProperties);
								if (pNode)
								{
									pNode->SetMatrix(mat);
									pNode->SetViewDistRatio(obj_view_params[0]);
									gEnv->p3DEngine->RegisterEntity(pNode);
									succesfully_gen_oblects.push_back(pNode);
								}
								//CryLogAlways("CProceduralGenerator decal creation called");
							}
						}
					}
				}
				else
				{
					std::vector<Matrix34>::const_iterator it = gen_points.begin();
					std::vector<Matrix34>::const_iterator end = gen_points.end();
					int count = gen_points.size();
					if (count > 0)
					{
						for (int i = 0; i < count, it != end; i++, ++it)
						{
							IRenderNode *pNode = NULL;
							pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
							pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
								Matrix34 tm = (*it);
								if (que_obj.object_scale != Vec3(1, 1, 1))
									tm.OrthonormalizeFast();

								Ang3 angles = Ang3::GetAnglesXYZ(tm);
								Quat Rot = Quat::CreateRotationXYZ(angles);

								que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
								que_obj.object_rot = Rot;

								que_obj.object_slot = i;
								que_obj.object_type = 2;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = false;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
								Matrix34 tm = (*it);
								if (que_obj.object_scale != Vec3(1, 1, 1))
									tm.OrthonormalizeFast();

								Ang3 angles = Ang3::GetAnglesXYZ(tm);
								Quat Rot = Quat::CreateRotationXYZ(angles);

								que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
								que_obj.object_rot = Rot;

								que_obj.object_slot = i;
								que_obj.object_type = 2;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = false;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							SDecalProperties decalProperties;
							decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
							decalProperties.m_pos = (*it).TransformPoint(Vec3(0, 0, 0));
							decalProperties.m_normal = (*it).TransformPoint(Vec3(0, 0, 1));
							Matrix33 rotation((*it));
							rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
							rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
							rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

							float add_rads = 0.0f;
							if (decals_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
							}

							decalProperties.m_pMaterialName = decals[i];
							decalProperties.m_radius = decals_base_scale + add_rads;
							decalProperties.m_explicitRightUpFront = rotation;
							decalProperties.m_sortPrio = cry_random(1, 150);
							decalProperties.m_deferred = true;
							decalProperties.m_depth = cry_random(0.5f, 3.0f);

							IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
							pRenderNode->SetDecalProperties(decalProperties);
							if (pNode)
							{
								pNode->SetMatrix((*it));
								pNode->SetViewDistRatio(obj_view_params[0]);
								gEnv->p3DEngine->RegisterEntity(pNode);
								succesfully_gen_oblects.push_back(pNode);
							}
						}
					}
				}
			}
		}
	}

	if (brushes_gen)
	{
		for (int i = 0; i < 5; i++)
		{
			if (!brushes[i].empty())
			{
				if (cry_random(0.0f, 99.0f) >= gen_brushes_rnd_pwr)
				{
					continue;
				}

				std::vector<Matrix34>::const_iterator it = gen_points.begin();
				std::vector<Matrix34>::const_iterator end = gen_points.end();
				int count = gen_points.size();
				for (int ic = 0; ic < int(gen_brushes_coof*dist); ic++)
				{
					float ntcg = cry_random(0.0f, 100.0f);
					if (ntcg >= gen_chance)
					{
						continue;
					}

					if (count > 0 && ic < count)
					{
						Matrix34 idtn_mtx;
						idtn_mtx.CreateIdentity();
						if (it != end)
						{
							if (gen_arr_dist_points_for_all)
							{
								idtn_mtx = (*it);
								float add_rads = 0.0f;
								if (gen_brushes_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
								}

								Vec3 nrm = Vec3(0, 0, 1);
								Vec3 pss = idtn_mtx.GetTranslation();
								Quat projected_orientation = GenerateObjOrientation(idtn_mtx, nrm);
								float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
								if (gen_brushes_rnd_rot_full)
								{
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
								}
								idtn_mtx.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									gen_objects_queue.push(que_obj);
									++it;
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									++it;
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
								IStatObj *pObj = NULL;
								pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);
								if (pNode)
								{
									IBrush *pBrush = (IBrush*)pNode;
									if (pBrush && pObj)
									{
										pObj->AddRef();
										AABB box2 = pObj->GetAABB();
										box2.SetTransformedAABB(idtn_mtx, box2);
										pBrush->SetBBox(box2);
										pBrush->SetMaterial(0);
										pBrush->OffsetPosition(Vec3(ZERO));
										pBrush->SetEntityStatObj(0, pObj, 0);
										pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
										pBrush->SetLodRatio(obj_view_params[1]);
										pBrush->SetViewDistRatio(obj_view_params[0]);
										if (brush_params[i] == 1)
											pBrush->Physicalize();

										pBrush->SetBBox(box2);
										gEnv->p3DEngine->RegisterEntity(pBrush);
										pBrush->SetMatrix(idtn_mtx);
										pBrush->SetBBox(box2);
										pBrush->SetDrawLast(false);
										if (brush_params[i] == 0)
											pBrush->Dephysicalize();
									}
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
						++it;

						if (gen_arr_dist_points_for_all)
							continue;
					}

					if (!gen_brushes_floating)
					{
						Vec3 pnt = GenerateVecRandomValue(genBounds.min, genBounds.max);
						if (pnt.IsValid())
						{
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								if (n > 0)
								{
									under_terrain = true;
								}
							}

							if (gen_updwn == 1 && under_terrain)
							{
								pnt.z = pnt.z + cry_random(5.0f, 8.0f);
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								float add_rads = 0.0f;
								if (gen_brushes_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
								}

								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								Vec3 pss = hit.pt;
								pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
								mat.CreateIdentity();
								mat.SetTranslation(pss);

								Quat projected_orientation = GenerateObjOrientation(mat, nrm);
								float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
								if (gen_brushes_rnd_rot_full)
								{
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
								}
								mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;

								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
								IStatObj *pObj = NULL;
								pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

								if (pNode)
								{
									IBrush *pBrush = (IBrush*)pNode;
									if (pBrush && pObj)
									{
										pObj->AddRef();
										AABB box2 = pObj->GetAABB();
										box2.SetTransformedAABB(mat, box2);
										pBrush->SetBBox(box2);
										pBrush->SetMaterial(0);

										pBrush->OffsetPosition(Vec3(ZERO));
										pBrush->SetEntityStatObj(0, pObj, 0);
										pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
										pBrush->SetLodRatio(obj_view_params[1]);
										pBrush->SetViewDistRatio(obj_view_params[0]);
										if (brush_params[i] == 1)
											pBrush->Physicalize();

										pBrush->SetBBox(box2);
										gEnv->p3DEngine->RegisterEntity(pBrush);
										pBrush->SetMatrix(mat);
										pBrush->SetBBox(box2);
										pBrush->SetDrawLast(false);
										if (brush_params[i] == 0)
											pBrush->Dephysicalize();
										//CryLogAlways("brushes_gen brush created and registred");
									}
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
					}
					else
					{
						Vec3 pnt = GenerateVecRandomValue(genBounds.min, genBounds.max);
						if (pnt.IsValid())
						{
							float add_rads = 0.0f;
							if (gen_brushes_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
							}
							Vec3 nrm = Vec3(0, 0, 1);


							Vec3 pss = pnt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_arc_ent_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IRenderNode *pNode = NULL;
							pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
							IStatObj *pObj = NULL;
							pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

							if (pNode)
							{
								IBrush *pBrush = (IBrush*)pNode;
								if (pBrush && pObj)
								{
									pObj->AddRef();
									AABB box2 = pObj->GetAABB();
									box2.SetTransformedAABB(mat, box2);
									pBrush->SetBBox(box2);
									pBrush->SetMaterial(0);
									pBrush->OffsetPosition(Vec3(ZERO));
									pBrush->SetEntityStatObj(0, pObj, 0);
									pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
									pBrush->SetLodRatio(obj_view_params[1]);
									pBrush->SetViewDistRatio(obj_view_params[0]);
									if (brush_params[i] == 1)
										pBrush->Physicalize();

									pBrush->SetBBox(box2);
									gEnv->p3DEngine->RegisterEntity(pBrush);
									pBrush->SetMatrix(mat);
									pBrush->SetBBox(box2);
									pBrush->SetDrawLast(false);
									if (brush_params[i] == 0)
										pBrush->Dephysicalize();
								}
								succesfully_gen_oblects.push_back(pNode);
							}
						}
					}
				}
			}
		}
	}

	if (arc_ents_gen)
	{
		for (int i = 0; i < 5; i++)
		{
			if (!arc_entites[i].empty())
			{
				if (cry_random(0.0f, 99.0f) >= gen_arc_ent_rnd_pwr)
				{
					continue;
				}

				for (int ic = 0; ic < int(gen_arc_ent_coof*dist); ic++)
				{
					float ntcg = cry_random(0.0f, 100.0f);
					if (ntcg >= gen_chance)
					{
						continue;
					}

					Vec3 pnt = GenerateVecRandomValue(genBounds.min, genBounds.max);
					if (pnt.IsValid())
					{
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							string nmn_generated = "Arc_ent_generated_c_x_";
							nmn_generated = GenerateNodeXName(nmn_generated);

							float add_rads = 0.0f;
							if (gen_arc_ent_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_arc_ent_rnd_scale, gen_arc_ent_rnd_scale);
							}
							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_brushes_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_arc_ent_base_scale + add_rads), projected_orientation, pss);

							IEntity* pEntity = NULL;
							SEntitySpawnParams params;
							IEntityArchetype* pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(arc_entites[i]);
							if (NULL != pArchetype)
							{
								params.nFlags = ENTITY_FLAG_SPAWNED;
								params.pArchetype = pArchetype;
								params.sName = nmn_generated;
								params.vPosition = mat.GetTranslation();
								params.vScale = Vec3(gen_arc_ent_base_scale + add_rads);


								params.qRotation = projected_orientation;

								// Create
								int nCastShadowMinSpec = 0;
								if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
								{
									objectVars->getAttr("CastShadowMinSpec", nCastShadowMinSpec);

									static ICVar* pObjShadowCastSpec = gEnv->pConsole->GetCVar("e_ObjShadowCastSpec");
									if (nCastShadowMinSpec <= pObjShadowCastSpec->GetIVal())
										params.nFlags |= ENTITY_FLAG_CASTSHADOW;

									bool bRecvWind;
									objectVars->getAttr("RecvWind", bRecvWind);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_RECVWIND;

									bool bOutdoorOnly;
									objectVars->getAttr("OutdoorOnly", bOutdoorOnly);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_OUTDOORONLY;

									bool bNoStaticDecals;
									objectVars->getAttr("NoStaticDecals", bNoStaticDecals);
									if (bNoStaticDecals)
										params.nFlags |= ENTITY_FLAG_NO_DECALNODE_DECALS;
								}

								pEntity = gEnv->pEntitySystem->SpawnEntity(params);
								if (pEntity)
								{
									succesfully_gen_entites.push_back(pEntity->GetId());
									IEntityRender* pRenderProx = pEntity->GetRenderInterface();
									if (pRenderProx && pRenderProx->GetRenderNode())
									{
										if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
										{
											int nViewDistRatio = 100;
											objectVars->getAttr("ViewDistRatio", nViewDistRatio);
											pRenderProx->SetViewDistRatio(nViewDistRatio);

											int nLodRatio = 100;
											objectVars->getAttr("lodRatio", nLodRatio);
											pRenderProx->SetLodRatio(nLodRatio);
										}
									}

									if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
									{
										bool bHiddenInGame = false;
										objectVars->getAttr("HiddenInGame", bHiddenInGame);
										if (bHiddenInGame)
											pEntity->Hide(true);
									}
								}
							}
						}
					}
				}
			}
		}
	}

	if (vegetation_gen)
	{
		int old_iter = 0;
		for (int i = 0; i < 5; i++)
		{
			if (!pterrain)
				break;

			if (!vegetations[i].empty())
			{
				if (cry_random(0.0f, 99.0f) >= vegetations_gen_rnd_pwr)
				{
					continue;
				}

				IStatInstGroup Group;
				/*int grp_id = 0;
				Group.fSize = 1.0f;
				Group.fSizeVar = 0.3f;
				Group.fBending = veg_bending;
				Group.bRandomRotation = true;
				Group.fAlignToTerrainCoefficient = 0.0f;
				Group.bUseTerrainColor = false;
				Group.bAllowIndoor = false;
				Group.bHideability = false;
				Group.bHideabilitySecondary = false;
				Group.nPlayerHideable = false;
				Group.bAutoMerged = veg_atm;
				Group.fStiffness = 0.5f;
				Group.fDamping = 2.5f;
				Group.fVariance = 0.6;
				Group.fAirResistance = 1.0f;
				Group.bPickable = true;
				Group.fVegRadius = -1;
				Group.fBrightness = 1;
				Group.fDensity = 1;
				Group.fElevationMin = 0;
				Group.fElevationMax = 4096;
				Group.fSlopeMin = 0;
				Group.fSlopeMax = 255;
				Group.nCastShadowMinSpec = 1;*/
				if (veg_inds[i] <= 0)
				{
					int ctfn = GetVegetationGrpId(vegetations[i]);
					if (ctfn >= 0)
					{
						veg_inds[i] = ctfn;
					}
				}

				if (veg_inds[i] <= -1)
					continue;

				gEnv->p3DEngine->GetStatInstGroup(veg_inds[i], Group);
				for (int ic = 0; ic < int(vegetations_gen_coof*dist); ic++)
				{
					float ntcg = cry_random(0.0f, 100.0f);
					if (ntcg >= gen_chance)
					{
						continue;
					}

					Vec3 pnt = GenerateVecRandomValue(genBounds.min, genBounds.max);
					if (pnt.IsValid())
					{
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							float add_rads = 0.0f;
							if (vegetations_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-vegetations_rnd_scale, vegetations_rnd_scale);
							}

							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);

							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_vegetations_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(vegetations_base_scale + add_rads), projected_orientation, pss);
							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IVegetation *pBrush = nullptr;
							uint8 ang_veg[3];
							Ang3 orient_xc = Ang3(projected_orientation);
							ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
							ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
							ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
							pBrush = (IVegetation*)pterrain->AddVegetationInstance(veg_inds[i], pss, vegetations_base_scale + add_rads, 1, ang_veg[0], ang_veg[1], ang_veg[2]);

							if (pBrush)
							{
								pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
								if (!Group.bAutoMerged)
									succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
							}
						}
					}
				}
			}
		}
	}

	if (terrain_surf_ids_gen)
	{
		
	}

	already_gen = true;
}

void CProceduralGenerator::GenObjectsOnSpline()
{
	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	if (!gEnv->pPhysicalWorld)
		return;

	ClearGeneratedRenderNodes();

	UpdateGenDirection();

	if (!generation_setting_data_refilled)
		RefillGenerationSettingData();

	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = "";
		brushes[i] = "";
		arc_entites[i] = "";
		vegetations[i] = "";
		surf_ids[i] = 0;
		veg_inds[i] = 0;
		brush_params[i] = 0;
	}


	bool decal_gen = false;
	bool brushes_gen = false;
	bool arc_ents_gen = false;
	bool terrain_surf_ids_gen = false;
	bool vegetation_gen = false;
	float vegetations_gen_coof = 0;
	float vegetations_base_scale = 0;
	float vegetations_rnd_scale = 0;
	float vegetations_gen_up_ht = 0;
	float vegetations_gen_rnd_pwr = 0;
	bool gen_vegetations_floating = false;
	bool gen_vegetations_rnd_rot_full = false;
	float decals_gen_coof = 0;
	int decals_aval_num = 0;
	float decals_base_scale = 0;
	float decals_rnd_scale = 0;
	float decals_gen_up_ht = 0;
	float decals_gen_rnd_pwr = 0;
	int gen_updwn = 0;
	float gen_up_from_nrm = 0;
	bool gen_arr_dist_points_for_all = false;
	bool gen_brushes_floating = false;
	bool gen_brushes_rnd_rot_full = false;
	float gen_brushes_base_scale = 0.0f;
	float gen_brushes_rnd_scale = 0.0f;
	float gen_brushes_up_hgt = 0.0f;
	float gen_brushes_rnd_pwr = 0.0f;
	float gen_brushes_coof = 0;
	float gen_arc_ent_coof = 0;
	float gen_surf_coof = 0;

	bool gen_arc_ent_floating = false;
	bool gen_arc_ent_rnd_rot_full = false;
	float gen_arc_ent_base_scale = 0.0f;
	float gen_arc_ent_rnd_scale = 0.0f;
	float gen_arc_ent_up_hgt = 0.0f;
	float gen_arc_ent_rnd_pwr = 0.0f;
	float gen_surf_rnd_pwr = 0.0f;

	if (!props)
		return;

	//int sector_size = 128;
	props->GetValue("fStreamingSectorSize", sector_size);

	//-----------Slope&Height Setup-------------
	float fMinHeight = 0.0f;
	float fMaxHeight = 11111.0f;
	float fMinSlope = 0.0;
	float fMaxSlope = 180.0;
	props->GetValue("fMinHeight", fMinHeight);
	props->GetValue("fMaxHeight", fMaxHeight);
	props->GetValue("fMinSlope", fMinSlope);
	props->GetValue("fMaxSlope", fMaxSlope);
	//------------------------------------------

	props->GetValue("nGenUpDown", gen_updwn);
	//decals------------------------------
	props->GetValue("bGenerateDecals", decal_gen);
	props->GetValue("fDecalsGenCoof", decals_gen_coof);
	props->GetValue("fileDecalOne", decals[0]);
	props->GetValue("fileDecalTwo", decals[1]);
	props->GetValue("fileDecalThree", decals[2]);
	props->GetValue("fileDecalFour", decals[3]);
	props->GetValue("fileDecalFive", decals[4]);
	props->GetValue("fDecalsGenBaseScale", decals_base_scale);
	props->GetValue("fDecalsGenRandomScale", decals_rnd_scale);
	props->GetValue("fDecalsGenUpHeight", decals_gen_up_ht);
	props->GetValue("fDecalsGenRandPower", decals_gen_rnd_pwr);
	//---------------------------------
	props->GetValue("bUseOneOfGenPointsArrToDistrAllObj", gen_arr_dist_points_for_all);
	props->GetValue("fHeigthUpFromNormal", gen_up_from_nrm);
	//brushes--------------------------
	props->GetValue("bGenerateBrushes", brushes_gen);
	props->GetValue("objBrushOne", brushes[0]);
	props->GetValue("objBrushTwo", brushes[1]);
	props->GetValue("objBrushThree", brushes[2]);
	props->GetValue("objBrushFour", brushes[3]);
	props->GetValue("objBrushFive", brushes[4]);
	props->GetValue("nBrushOnePhyPrm", brush_params[0]);
	props->GetValue("nBrushTwoPhyPrm", brush_params[1]);
	props->GetValue("nBrushThreePhyPrm", brush_params[2]);
	props->GetValue("nBrushFourPhyPrm", brush_params[3]);
	props->GetValue("nBrushFivePhyPrm", brush_params[4]);
	props->GetValue("bBrushesGenFloating", gen_brushes_floating);
	props->GetValue("fBrushesGenCoof", gen_brushes_coof);
	props->GetValue("fBrushesGenBaseScale", gen_brushes_base_scale);
	props->GetValue("fBrushesGenRandomScale", gen_brushes_rnd_scale);
	props->GetValue("fBrushesGenUpHeight", gen_brushes_up_hgt);
	props->GetValue("fBrushesGenRandPower", gen_brushes_rnd_pwr);
	props->GetValue("bBrushesGenFullRandRotation", gen_brushes_rnd_rot_full);
	//---------------------------------
	//Archetype entites----------------
	props->GetValue("bGenerateArcEntites", arc_ents_gen);
	props->GetValue("bArcEntitesGenFloating", gen_arc_ent_floating);
	props->GetValue("sArcEntityOne", arc_entites[0]);
	props->GetValue("sArcEntityTwo", arc_entites[1]);
	props->GetValue("sArcEntityThree", arc_entites[2]);
	props->GetValue("sArcEntityFour", arc_entites[3]);
	props->GetValue("sArcEntityFive", arc_entites[4]);
	props->GetValue("fArcEntitesGenCoof", gen_arc_ent_coof);
	props->GetValue("fArcEntitesGenBaseScale", gen_arc_ent_base_scale);
	props->GetValue("fArcEntitesGenRandomScale", gen_arc_ent_rnd_scale);
	props->GetValue("fArcEntitesGenUpHeight", gen_arc_ent_up_hgt);
	props->GetValue("fArcEntitesGenRandPower", gen_arc_ent_rnd_pwr);
	props->GetValue("bArcEntitesGenFullRandRotation", gen_arc_ent_rnd_rot_full);
	//---------------------------------
	//terrain surfaces ids------------------
	props->GetValue("bGenerateTerrainSurfaceIds", terrain_surf_ids_gen);
	props->GetValue("nTerrainSurfaceIdOne", surf_ids[0]);
	props->GetValue("nTerrainSurfaceIdTwo", surf_ids[1]);
	props->GetValue("nTerrainSurfaceIdThree", surf_ids[2]);
	props->GetValue("nTerrainSurfaceIdFour", surf_ids[3]);
	props->GetValue("nTerrainSurfaceIdFive", surf_ids[4]);
	props->GetValue("nTerrainSurfaceGenCoof", gen_surf_coof);
	props->GetValue("fTerrainSurfaceGenRandPower", gen_surf_rnd_pwr);
	//---------------------------------
	//vegetations------------------
	props->GetValue("bGenerateVegetations", vegetation_gen);
	props->GetValue("objVegetationOne", vegetations[0]);
	props->GetValue("objVegetationTwo", vegetations[1]);
	props->GetValue("objVegetationThree", vegetations[2]);
	props->GetValue("objVegetationFour", vegetations[3]);
	props->GetValue("objVegetationFive", vegetations[4]);
	props->GetValue("fVegetationIndexOne", veg_inds[0]);
	props->GetValue("fVegetationIndexTwo", veg_inds[1]);
	props->GetValue("fVegetationIndexThree", veg_inds[2]);
	props->GetValue("fVegetationIndexFour", veg_inds[3]);
	props->GetValue("fVegetationIndexFive", veg_inds[4]);
	props->GetValue("bVegetationsGenFloating", gen_vegetations_floating);
	props->GetValue("fVegetationsGenCoof", vegetations_gen_coof);
	props->GetValue("fVegetationsGenBaseScale", vegetations_base_scale);
	props->GetValue("fVegetationsGenRandomScale", vegetations_rnd_scale);
	props->GetValue("fVegetationsGenUpHeight", vegetations_gen_up_ht);
	props->GetValue("fVegetationsGenRandPower", vegetations_gen_rnd_pwr);
	props->GetValue("bVegetationsGenFullRandRotation", gen_vegetations_rnd_rot_full);
	bool veg_atm = false;
	float veg_bending = 0.0f;
	props->GetValue("fVegetationsBending", veg_bending);
	props->GetValue("bVegetationsAutoMerged", veg_atm);
	//---------------------------------
	float z_gen_rot_rand_val = 0.0f;
	props->GetValue("fGenZRotationRandomVal", z_gen_rot_rand_val);

	int obj_view_params[2];
	for (int i = 0; i < 2; i++)
	{
		obj_view_params[i] = 0;
	}
	props->GetValue("fGenObjectsViewDistance", obj_view_params[0]);
	props->GetValue("fGenObjectsLodDistance", obj_view_params[1]);
	bool gen_olly_on_terrain = false;
	props->GetValue("bGenOnTerrainOnly", gen_olly_on_terrain);
	int gen_on_stat_objs = ent_static;
	if (gen_olly_on_terrain)
		gen_on_stat_objs = 0;

	bool RotateToShapeOrSplineSegmentDir = false;
	props->GetValue("bRotateToShapeOrSplineSegmentDir", RotateToShapeOrSplineSegmentDir);

	float ShapeGenWidth = 1.0f;
	props->GetValue("fShapeGenWidth", ShapeGenWidth);
	bool RotToSurfaceNormal = true;
	props->GetValue("bRotToSurfaceNormal", RotToSurfaceNormal);

	float min_dist_gen_old = 0.0f;
	props->GetValue("fMinDistToOldGenObjs", min_dist_gen_old);

	float min_dist_gen_old_rnd = 0.0f;
	props->GetValue("fMinDistToOldGenObjsRandom", min_dist_gen_old_rnd);
	float normal_rot_mult_rnd = 0.0f;
	props->GetValue("fRotToNormalRandom", normal_rot_mult_rnd);

	float normal_rot_mult_x = 1.0f;
	props->GetValue("fRotToNormalMlt", normal_rot_mult_x);
	normal_rot_mult_x = min(1.0f, normal_rot_mult_x);

	for (int i = 0; i < 2; i++)
	{
		if (obj_view_params[i] > 255)
			obj_view_params[i] = 255;
	}

	for (int i = 0; i < 5; i++)
	{
		if (!decals[i].empty())
		{
			decals_aval_num += 1;
		}
	}
	IGameVolumes::VolumeInfo volumeInfo;
	if (!GetVolumeInfoForEntityGen(GetEntityId(), &volumeInfo))
		return;

	AABB genBounds;
	GetEntity()->GetWorldBounds(genBounds);
	Matrix34 mat = GetEntity()->GetWorldTM();
	mat.SetScale(Vec3(1.0f));
	mat.SetTranslation(GetEntity()->GetWorldPos());
	uint32 vertexCount = volumeInfo.verticesCount;
	float volumeHeight = volumeInfo.volumeHeight;
	float dist = 0.0f;
	int vertex_last = 1;
	genBounds.min = mat.TransformPoint(volumeInfo.pVertices[0]);
	std::vector<Vec3> poli_shape;
	poli_shape.clear();
	poli_shape.push_back(genBounds.min);
	for (uint32 i = 1; i < vertexCount; ++i)
	{
		Vec3 v1 = volumeInfo.pVertices[i];
		poli_shape.push_back(mat.TransformPoint(v1));
		float dv = v1.GetDistance(volumeInfo.pVertices[0]);
		if (dist < dv)
		{
			dist = dv;
			vertex_last = i;
		}
	}
	genBounds.max = mat.TransformPoint(volumeInfo.pVertices[vertex_last]) + Vec3(0, 0, volumeHeight);
	dist = genBounds.min.GetDistance(genBounds.max)*2.0f;
	dist *= 1.3f;
	std::vector<Matrix34> gen_points;
	gen_points.clear();

	float z_bouds = genBounds.min.z;
	if (z_bouds < genBounds.max.z)
	{
		z_bouds = genBounds.max.z;
	}

	std::vector<Vec3>::const_iterator it_gtl = xrf_gen_shape.begin();
	std::vector<Vec3>::const_iterator end_gtl = xrf_gen_shape.end();
	int countxx = xrf_gen_shape.size();
	if (countxx <= 0)
		return;

	dist = ShapeGenWidth;
	Vec3 old_pnt(ZERO);
	for (int ifc = 0; ifc < countxx, it_gtl != end_gtl; ifc++, ++it_gtl)
	{
		if (ifc == 0)
		{
			old_pnt = (*it_gtl);
			continue;
		}
		Vec3 cvdx = (*it_gtl);
		Vec3 segment_dirc = (cvdx - old_pnt).GetNormalizedSafe();
		dist = cvdx.GetDistance(old_pnt);
		if (gen_arr_dist_points_for_all)
		{
			for (int ic = 0; ic < int(gen_brushes_coof*dist); ic++)
			{
				Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
				if (ShapeGenWidth > 0.0f)
				{
					pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f,3.5f)) * cry_random(0.0f, ShapeGenWidth));
				}
				pnt.z = cry_random(old_pnt.z, z_bouds);


				ray_hit hit;
				int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

				bool under_terrain = false;
				if (n <= 0)
				{
					n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
						rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

					under_terrain = true;
				}

				if (!gen_brushes_floating)
				{
					if (n > 0)
					{
						if (min_dist_gen_old > 0.0f)
						{
							float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
							if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
							{
								continue;
							}
						}

						if (normal_rot_mult_x < 1.0f)
						{
							float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
							hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
						}

						IRenderNode *pNode = NULL;
						pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
						pNode->SetRndFlags(ERF_PROCEDURAL, true);
						Matrix34 mat_2 = mat;
						Vec3 pss = hit.pt;
						pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
						mat_2.SetTranslation(pss);
						if (RotateToShapeOrSplineSegmentDir)
						{
							hit.n = segment_dirc;
						}
						Vec3 nrm = hit.n;
						if (under_terrain)
							nrm = -nrm;

						if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
						{
							continue;
						}

						Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
						float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
						if (gen_brushes_rnd_rot_full)
						{
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
						}

						mat_2.Set(Vec3(1.00f), projected_orientation, pss);
						gen_points.push_back(mat_2);
					}
				}
				else
				{
					if (min_dist_gen_old > 0.0f)
					{
						float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
						if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
						{
							continue;
						}
					}

					if (normal_rot_mult_x < 1.0f)
					{
						float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
						hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
					}

					IRenderNode *pNode = NULL;
					pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
					pNode->SetRndFlags(ERF_PROCEDURAL, true);
					Matrix34 mat_2 = mat;
					Vec3 pss = pnt;
					pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
					mat_2.SetTranslation(pss);
					Vec3 nrm = Vec3(0, 0, 1);
					if (RotateToShapeOrSplineSegmentDir)
					{
						nrm = segment_dirc;
					}

					if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
					{
						continue;
					}

					Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
					float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
					projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
					if (gen_brushes_rnd_rot_full)
					{
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
					}

					mat_2.Set(Vec3(1.00f), projected_orientation, pss);
					gen_points.push_back(mat_2);
				}
			}
		}

		if (decal_gen && decals_aval_num > 0)
		{
			for (int i = 0; i < 5; i++)
			{
				if (!decals[i].empty())
				{
					if (!gen_arr_dist_points_for_all)
					{
						for (int ic = 0; ic < int(decals_gen_coof*dist); ic++)
						{
							Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
							if (ShapeGenWidth > 0.0f)
							{
								pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f, 3.5f)) * cry_random(0.0f, ShapeGenWidth));
							}
							pnt.z = cry_random(old_pnt.z, z_bouds);
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								under_terrain = true;
							}

							if (RotateToShapeOrSplineSegmentDir)
							{
								hit.n = segment_dirc;
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								Vec3 pss = hit.pt;
								pss.z += 0.1f;
								SDecalProperties decalProperties;
								decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
								mat.CreateIdentity();
								mat.SetTranslation(pss);
								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								const Matrix34& wtm(mat);
								Vec3 x(wtm.GetColumn0().GetNormalized());
								Vec3 y(wtm.GetColumn1().GetNormalized());
								Vec3 z(nrm.GetNormalized());

								y = z.Cross(x);
								if (y.GetLengthSquared() < 1e-4f)
									y = z.GetOrthogonal();
								y.Normalize();
								x = y.Cross(z);

								Matrix33 newOrient;
								newOrient.SetColumn(0, x);
								newOrient.SetColumn(1, y);
								newOrient.SetColumn(2, z);
								Quat q(newOrient);
								float angle(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								q = (q * Quat(Matrix33::CreateRotationZ(angle)));

								mat.Set(Vec3(1.00f), q, pss);

								decalProperties.m_pos = pss;
								decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
								Matrix33 rotation(mat);
								rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
								rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
								rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

								//rotation.SetScale(Vec3(0.05f));
								float add_rads = 0.0f;
								if (decals_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
								}

								decalProperties.m_pMaterialName = decals[i];
								decalProperties.m_radius = decals_base_scale + add_rads;
								decalProperties.m_explicitRightUpFront = rotation;
								decalProperties.m_sortPrio = cry_random(1, 150);
								decalProperties.m_deferred = true;
								decalProperties.m_depth = cry_random(0.5f, 3.0f);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
								pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

								IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
								pRenderNode->SetDecalProperties(decalProperties);
								if (pNode)
								{
									pNode->SetMatrix(mat);
									pNode->SetViewDistRatio(obj_view_params[0]);
									gEnv->p3DEngine->RegisterEntity(pNode);
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
					}
					else
					{
						std::vector<Matrix34>::const_iterator it = gen_points.begin();
						std::vector<Matrix34>::const_iterator end = gen_points.end();
						int count = gen_points.size();
						if (count > 0)
						{
							for (int i = 0; i < count, it != end; i++, ++it)
							{
								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
								pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
									Matrix34 tm = (*it);
									if (que_obj.object_scale != Vec3(1, 1, 1))
										tm.OrthonormalizeFast();

									Ang3 angles = Ang3::GetAnglesXYZ(tm);
									Quat Rot = Quat::CreateRotationXYZ(angles);

									que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
									que_obj.object_rot = Rot;

									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
									Matrix34 tm = (*it);
									if (que_obj.object_scale != Vec3(1, 1, 1))
										tm.OrthonormalizeFast();

									Ang3 angles = Ang3::GetAnglesXYZ(tm);
									Quat Rot = Quat::CreateRotationXYZ(angles);

									que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
									que_obj.object_rot = Rot;

									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								SDecalProperties decalProperties;
								decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
								decalProperties.m_pos = (*it).TransformPoint(Vec3(0, 0, 0));
								decalProperties.m_normal = (*it).TransformPoint(Vec3(0, 0, 1));
								Matrix33 rotation((*it));
								rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
								rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
								rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

								float add_rads = 0.0f;
								if (decals_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
								}

								decalProperties.m_pMaterialName = decals[i];
								decalProperties.m_radius = decals_base_scale + add_rads;
								decalProperties.m_explicitRightUpFront = rotation;
								decalProperties.m_sortPrio = cry_random(1, 150);
								decalProperties.m_deferred = true;
								decalProperties.m_depth = cry_random(0.5f, 3.0f);

								IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
								pRenderNode->SetDecalProperties(decalProperties);
								if (pNode)
								{
									pNode->SetMatrix((*it));
									pNode->SetViewDistRatio(obj_view_params[0]);
									gEnv->p3DEngine->RegisterEntity(pNode);
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
					}
				}
			}
		}

		if (brushes_gen)
		{
			for (int i = 0; i < 5; i++)
			{
				if (!brushes[i].empty())
				{
					std::vector<Matrix34>::const_iterator it = gen_points.begin();
					std::vector<Matrix34>::const_iterator end = gen_points.end();
					int count = gen_points.size();
					for (int ic = 0; ic < int(gen_brushes_coof*dist); ic++)
					{
						if (count > 0 && ic < count)
						{
							Matrix34 idtn_mtx;
							idtn_mtx.CreateIdentity();
							if (it != end)
							{
								if (gen_arr_dist_points_for_all)
								{
									idtn_mtx = (*it);
									float add_rads = 0.0f;
									if (gen_brushes_rnd_scale > 0.0f)
									{
										add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
									}

									Vec3 nrm = Vec3(0, 0, 1);
									if (RotateToShapeOrSplineSegmentDir)
									{
										nrm = segment_dirc;
									}
									Vec3 pss = idtn_mtx.GetTranslation();
									Quat projected_orientation = GenerateObjOrientation(idtn_mtx, nrm);
									float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
									if (gen_brushes_rnd_rot_full)
									{
										projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
										projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
									}
									idtn_mtx.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);
									if (is_que_enabled && !is_streaming_sectors_enabled)
									{
										SQuedGenObject que_obj = SQuedGenObject();
										que_obj.object_pos = pss;
										que_obj.object_rot = projected_orientation;
										que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
										que_obj.object_slot = i;
										que_obj.object_type = 1;
										que_obj.vDistRate[0] = obj_view_params[1];
										que_obj.vDistRate[1] = obj_view_params[0];
										que_obj.physics_enable = brush_params[i] != 0;
										gen_objects_queue.push(que_obj);
										++it;
										continue;
									}

									if (is_streaming_sectors_enabled)
									{
										SQuedGenObject que_obj = SQuedGenObject();
										que_obj.object_pos = pss;
										que_obj.object_rot = projected_orientation;
										que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
										que_obj.object_slot = i;
										que_obj.object_type = 1;
										que_obj.vDistRate[0] = obj_view_params[1];
										que_obj.vDistRate[1] = obj_view_params[0];
										que_obj.physics_enable = brush_params[i] != 0;
										AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
										++it;
										continue;
									}
									IRenderNode *pNode = NULL;
									pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
									IStatObj *pObj = NULL;
									pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);
									if (pNode)
									{
										IBrush *pBrush = (IBrush*)pNode;
										if (pBrush && pObj)
										{
											pObj->AddRef();
											AABB box2 = pObj->GetAABB();
											box2.SetTransformedAABB(idtn_mtx, box2);
											pBrush->SetBBox(box2);
											pBrush->SetMaterial(0);
											pBrush->OffsetPosition(Vec3(ZERO));
											pBrush->SetEntityStatObj(0, pObj, 0);
											pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
											pBrush->SetLodRatio(obj_view_params[1]);
											pBrush->SetViewDistRatio(obj_view_params[0]);
											if (brush_params[i] == 1)
												pBrush->Physicalize();

											pBrush->SetBBox(box2);
											gEnv->p3DEngine->RegisterEntity(pBrush);
											pBrush->SetMatrix(idtn_mtx);
											pBrush->SetBBox(box2);
											pBrush->SetDrawLast(false);
											if (brush_params[i] == 0)
												pBrush->Dephysicalize();
										}
										succesfully_gen_oblects.push_back(pNode);
									}
								}
							}
							++it;

							if (gen_arr_dist_points_for_all)
								continue;
						}

						if (!gen_brushes_floating)
						{
							Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
							if (ShapeGenWidth > 0.0f)
							{
								pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f, 3.5f)) * cry_random(0.0f, ShapeGenWidth));
							}
							pnt.z = cry_random(old_pnt.z, z_bouds);
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								if (n > 0)
								{
									under_terrain = true;
								}
							}

							if (gen_updwn == 1 && under_terrain)
							{
								pnt.z = pnt.z + cry_random(5.0f, 8.0f);
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
							}

							if (RotateToShapeOrSplineSegmentDir)
							{
								hit.n = segment_dirc;
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								float add_rads = 0.0f;
								if (gen_brushes_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
								}

								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								Vec3 pss = hit.pt;
								pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
								mat.CreateIdentity();
								mat.SetTranslation(pss);

								Quat projected_orientation = GenerateObjOrientation(mat, nrm);
								float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
								if (gen_brushes_rnd_rot_full)
								{
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
								}
								mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
								IStatObj *pObj = NULL;
								pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

								if (pNode)
								{
									IBrush *pBrush = (IBrush*)pNode;
									if (pBrush && pObj)
									{
										pObj->AddRef();
										AABB box2 = pObj->GetAABB();
										box2.SetTransformedAABB(mat, box2);
										pBrush->SetBBox(box2);
										pBrush->SetMaterial(0);

										pBrush->OffsetPosition(Vec3(ZERO));
										pBrush->SetEntityStatObj(0, pObj, 0);
										pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
										pBrush->SetLodRatio(obj_view_params[1]);
										pBrush->SetViewDistRatio(obj_view_params[0]);
										if (brush_params[i] == 1)
											pBrush->Physicalize();

										pBrush->SetBBox(box2);
										gEnv->p3DEngine->RegisterEntity(pBrush);
										pBrush->SetMatrix(mat);
										pBrush->SetBBox(box2);
										pBrush->SetDrawLast(false);
										if (brush_params[i] == 0)
											pBrush->Dephysicalize();
									}
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
						else
						{
							Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
							if (ShapeGenWidth > 0.0f)
							{
								pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f, 3.5f)) * cry_random(0.0f, ShapeGenWidth));
							}
							pnt.z = cry_random(old_pnt.z, z_bouds);
							float add_rads = 0.0f;
							if (gen_brushes_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
							}
							Vec3 nrm = Vec3(0, 0, 1);
							if (RotateToShapeOrSplineSegmentDir)
							{
								nrm = segment_dirc;
							}

							Vec3 pss = pnt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_arc_ent_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IRenderNode *pNode = NULL;
							pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
							IStatObj *pObj = NULL;
							pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

							if (pNode)
							{
								IBrush *pBrush = (IBrush*)pNode;
								if (pBrush && pObj)
								{
									pObj->AddRef();
									AABB box2 = pObj->GetAABB();
									box2.SetTransformedAABB(mat, box2);
									pBrush->SetBBox(box2);
									pBrush->SetMaterial(0);
									pBrush->OffsetPosition(Vec3(ZERO));
									pBrush->SetEntityStatObj(0, pObj, 0);
									pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
									pBrush->SetLodRatio(obj_view_params[1]);
									pBrush->SetViewDistRatio(obj_view_params[0]);
									if (brush_params[i] == 1)
										pBrush->Physicalize();

									pBrush->SetBBox(box2);
									gEnv->p3DEngine->RegisterEntity(pBrush);
									pBrush->SetMatrix(mat);
									pBrush->SetBBox(box2);
									pBrush->SetDrawLast(false);
									if (brush_params[i] == 0)
										pBrush->Dephysicalize();
								}
								succesfully_gen_oblects.push_back(pNode);
							}
						}
					}
				}
			}
		}

		if (arc_ents_gen)
		{
			for (int i = 0; i < 5; i++)
			{
				if (!arc_entites[i].empty())
				{
					for (int ic = 0; ic < int(gen_arc_ent_coof*dist); ic++)
					{
						Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
						if (ShapeGenWidth > 0.0f)
						{
							pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f, 3.5f)) * cry_random(0.0f, ShapeGenWidth));
						}
						pnt.z = cry_random(old_pnt.z, z_bouds);
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (RotateToShapeOrSplineSegmentDir)
						{
							hit.n = segment_dirc;
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							string nmn_generated = "Arc_ent_generated_c_x_";
							nmn_generated = GenerateNodeXName(nmn_generated);

							float add_rads = 0.0f;
							if (gen_arc_ent_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_arc_ent_rnd_scale, gen_arc_ent_rnd_scale);
							}
							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_brushes_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_arc_ent_base_scale + add_rads), projected_orientation, pss);

							IEntity* pEntity = NULL;
							SEntitySpawnParams params;
							IEntityArchetype* pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(arc_entites[i]);
							if (NULL != pArchetype)
							{
								params.nFlags = ENTITY_FLAG_SPAWNED;
								params.pArchetype = pArchetype;
								params.sName = nmn_generated;
								params.vPosition = mat.GetTranslation();
								params.vScale = Vec3(gen_arc_ent_base_scale + add_rads);


								params.qRotation = projected_orientation;

								// Create
								int nCastShadowMinSpec = 0;
								if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
								{
									objectVars->getAttr("CastShadowMinSpec", nCastShadowMinSpec);

									static ICVar* pObjShadowCastSpec = gEnv->pConsole->GetCVar("e_ObjShadowCastSpec");
									if (nCastShadowMinSpec <= pObjShadowCastSpec->GetIVal())
										params.nFlags |= ENTITY_FLAG_CASTSHADOW;

									bool bRecvWind;
									objectVars->getAttr("RecvWind", bRecvWind);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_RECVWIND;

									bool bOutdoorOnly;
									objectVars->getAttr("OutdoorOnly", bOutdoorOnly);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_OUTDOORONLY;

									bool bNoStaticDecals;
									objectVars->getAttr("NoStaticDecals", bNoStaticDecals);
									if (bNoStaticDecals)
										params.nFlags |= ENTITY_FLAG_NO_DECALNODE_DECALS;
								}

								pEntity = gEnv->pEntitySystem->SpawnEntity(params);
								if (pEntity)
								{
									succesfully_gen_entites.push_back(pEntity->GetId());
									IEntityRender* pRenderProx = pEntity->GetRenderInterface();
									if (pRenderProx && pRenderProx->GetRenderNode())
									{
										if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
										{
											int nViewDistRatio = 100;
											objectVars->getAttr("ViewDistRatio", nViewDistRatio);
											pRenderProx->SetViewDistRatio(nViewDistRatio);

											int nLodRatio = 100;
											objectVars->getAttr("lodRatio", nLodRatio);
											pRenderProx->SetLodRatio(nLodRatio);
										}
									}

									if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
									{
										bool bHiddenInGame = false;
										objectVars->getAttr("HiddenInGame", bHiddenInGame);
										if (bHiddenInGame)
											pEntity->Hide(true);
									}
								}
							}
						}
					}
				}
			}
		}

		if (vegetation_gen)
		{
			int old_iter = 0;
			ITerrain* pterrain = gEnv->p3DEngine->GetITerrain();
			for (int i = 0; i < 5; i++)
			{
				if (!pterrain)
					break;

				if (!vegetations[i].empty())
				{
					IStatInstGroup Group;
					if (veg_inds[i] <= 0)
					{
						int ctfn = GetVegetationGrpId(vegetations[i]);
						if (ctfn >= 0)
						{
							veg_inds[i] = ctfn;
						}
					}

					if (veg_inds[i] <= -1)
						continue;

					gEnv->p3DEngine->GetStatInstGroup(veg_inds[i], Group);
					for (int ic = 0; ic < int(vegetations_gen_coof*dist); ic++)
					{
						Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
						if (ShapeGenWidth > 0.0f)
						{
							pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f, 3.5f)) * cry_random(0.0f, ShapeGenWidth));
						}
						pnt.z = cry_random(old_pnt.z, z_bouds);
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (RotateToShapeOrSplineSegmentDir)
						{
							hit.n = segment_dirc;
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							float add_rads = 0.0f;
							if (vegetations_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-vegetations_rnd_scale, vegetations_rnd_scale);
							}

							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);

							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_vegetations_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(vegetations_base_scale + add_rads), projected_orientation, pss);
							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IVegetation *pBrush = nullptr;
							uint8 ang_veg[3];
							Ang3 orient_xc = Ang3(projected_orientation);
							ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
							ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
							ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
							pBrush = (IVegetation*)pterrain->AddVegetationInstance(veg_inds[i], pss, vegetations_base_scale + add_rads, 1, ang_veg[0], ang_veg[1], ang_veg[2]);

							if (pBrush)
							{
								pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
								if (!Group.bAutoMerged)
									succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
							}
						}
					}
				}
			}
		}

		if (terrain_surf_ids_gen)
		{

		}

		old_pnt = (*it_gtl);
	}

	already_gen = true;
}

void CProceduralGenerator::GenObjectsOnShapeBorder()
{
	GenObjectsOnSpline();
}

void CProceduralGenerator::SaveObjectsInXML()
{
	bool is_mod = false;
	if (const ICmdLineArg* pModArg = gEnv->pSystem->GetICmdLine()->FindArg(eCLAT_Pre, "MOD"))
	{
		if (gEnv->pSystem->IsMODValid(pModArg->GetValue()))
		{
			is_mod = true;
		}
	}
	string file_nmn = GetEntity()->GetName() + string(".grp");
	string levelPath = PathUtil::GetGameFolder();
	ILevelInfo* pLevelInfo = gEnv->pGameFramework->GetILevelSystem()->GetCurrentLevel();
	if (pLevelInfo)
	{
		levelPath = PathUtil::AddSlash(levelPath);
		if (is_mod)
			levelPath = "";

		levelPath = levelPath + pLevelInfo->GetPath();
	}
	levelPath = PathUtil::AddSlash(levelPath);
	levelPath = levelPath + string("GeneratedObjs");
	levelPath = PathUtil::AddSlash(levelPath);
	string file_pth = levelPath;
	if (is_streaming_sectors_enabled)
	{
		SaveStreamingSectorsInXML(file_pth);
		return;
	}
	file_pth.append(file_nmn);

	//XmlNodeRef Objects_nodeZ = GetISystem()->LoadXmlFromFile(file_pth);
	//if (!gEnv->IsEditor() && Objects_nodeZ)
	//{
	//	return;
	//}
	XmlNodeRef Objects_node = GetISystem()->CreateXmlNode("Objects");
	if (!Objects_node)
	{
		CryLogAlways("Cannot create xml node Objects(fnc CProceduralGenerator::SaveObjectsInXML)");
		return;
	}
	bool obj_added_to_fl = false;
	string layer = GetEntity()->GetName();
	string LayerGUID = GenerateRNDGUID();
	std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
	int count = succesfully_gen_oblects.size();
	if (count > 0)
	{
		for (int il = 0; il < count, it != end; il++, ++it)
		{
			if ((*it)->GetRenderNodeType() == eERType_Brush)
			{
				IBrush *pBrush = (IBrush*)(*it);
				if (pBrush)
				{
					XmlNodeRef rnd_node = Objects_node->newChild("Object");
					if (!rnd_node)
						continue;

					string type = "Brush";
					//string layer = "Main";
					//string LayerGUID = "{E0199113-42D5-4141-9C20-09D9C93AD7D4}";
					string objGUId = GenerateRNDGUID();
					string nmn_generated = "Brush_Generated_";
					nmn_generated = GenerateNodeXName(nmn_generated);
					int FlNumber = -1;
					int ColorRGB = 16777215;
					Vec3 Pos = pBrush->GetPos();
					Vec3 Scale = Vec3(pBrush->GetMatrix().GetColumn(0).GetLength());
					Matrix34 tm = pBrush->GetMatrix();
					if(Scale != Vec3(1,1,1))
						tm.OrthonormalizeFast();

					Ang3 angles = Ang3::GetAnglesXYZ(tm);
					Quat Rot = Quat::CreateRotationXYZ(angles);

					int MatLayersMask = 0;
					string Prefab = "";
					if (pBrush->GetEntityStatObj())
						Prefab = pBrush->GetEntityStatObj()->GetFilePath();

					int OutdoorOnly = 0; int CastShadowMaps = 1; int RainOccluder = 1;
					int SupportSecondVisarea = 0; int DynamicDistanceShadows = 0; int Hideable = 0;
					int LodRatio = pBrush->GetLodRatio();
					int ViewDistRatio = pBrush->GetViewDistRatio();
					int NotTriangulate = 0;
					int NoDynamicWater = 0;
					int AIRadius = -1; int NoStaticDecals = 0; int NoAmnbShadowCaster = 0;
					int RecvWind = 0; int Occluder = 0; int DrawLast = 0; int ShadowLodBias = 0;
					int RndFlags = 1610612744;

					rnd_node->setAttr("Type", type);
					rnd_node->setAttr("Layer", layer);
					rnd_node->setAttr("LayerGUID", LayerGUID);
					rnd_node->setAttr("Id", objGUId);
					rnd_node->setAttr("Name", nmn_generated);
					rnd_node->setAttr("Pos", Pos);
					rnd_node->setAttr("FloorNumber", FlNumber);
					rnd_node->setAttr("Rotate", Rot);
					rnd_node->setAttr("Scale", Scale);
					rnd_node->setAttr("ColorRGB", ColorRGB);
					rnd_node->setAttr("MatLayersMask", MatLayersMask);
					rnd_node->setAttr("Prefab", Prefab);
					rnd_node->setAttr("OutdoorOnly", OutdoorOnly);
					rnd_node->setAttr("CastShadowMaps", CastShadowMaps);
					rnd_node->setAttr("RainOccluder", RainOccluder);
					rnd_node->setAttr("SupportSecondVisarea", SupportSecondVisarea);
					rnd_node->setAttr("DynamicDistanceShadows", DynamicDistanceShadows);
					rnd_node->setAttr("Hideable", Hideable);
					rnd_node->setAttr("LodRatio", LodRatio);
					rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
					rnd_node->setAttr("NotTriangulate", NotTriangulate);
					rnd_node->setAttr("NoDynamicWater", NoDynamicWater);
					rnd_node->setAttr("AIRadius", AIRadius);
					rnd_node->setAttr("NoStaticDecals", NoStaticDecals);
					rnd_node->setAttr("NoAmnbShadowCaster", NoAmnbShadowCaster);
					rnd_node->setAttr("RecvWind", RecvWind);
					rnd_node->setAttr("Occluder", Occluder);
					rnd_node->setAttr("DrawLast", DrawLast);
					rnd_node->setAttr("ShadowLodBias", ShadowLodBias);
					rnd_node->setAttr("RndFlags", RndFlags);
					obj_added_to_fl = true;
				}
			}
			else if ((*it)->GetRenderNodeType() == eERType_Decal)
			{
				IDecalRenderNode *pDecal = static_cast<IDecalRenderNode*>((*it));
				if (pDecal)
				{
					XmlNodeRef rnd_node = Objects_node->newChild("Object");
					if (!rnd_node)
						continue;

					string type = "Decal";
					//string layer = "Main";
					//string LayerGUID = "{E0199113-42D5-4141-9C20-09D9C93AD7D4}";
					string objGUId = GenerateRNDGUID();
					string nmn_generated = "Decal_Generated_";
					Vec3 Pos = pDecal->GetPos();
					Vec3 Scale = Vec3(pDecal->GetMatrix().GetColumn(0).GetLength());
					Matrix34 tm = pDecal->GetMatrix();
					if (Scale != Vec3(1, 1, 1))
						tm.OrthonormalizeFast();

					Ang3 angles = Ang3::GetAnglesXYZ(tm);
					Quat Rot = Quat::CreateRotationXYZ(angles);

					nmn_generated = GenerateNodeXName(nmn_generated);
					int FlNumber = -1;
					int ColorRGB = 16744319;
					string Material = "";
					if (pDecal->GetMaterial())
						Material = pDecal->GetMaterial()->GetName();

					int ProjectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
					int Deferred = 1;
					int ViewDistRatio = pDecal->GetViewDistRatio();
					int SortPriority = pDecal->GetSortPriority();
					int rnd_flags = 0;
					float ProjectionDepth = pDecal->GetDecalProperties()->m_depth;
					rnd_node->setAttr("Type", type);
					rnd_node->setAttr("Layer", layer);
					rnd_node->setAttr("LayerGUID", LayerGUID);
					rnd_node->setAttr("Id", objGUId);
					rnd_node->setAttr("Name", nmn_generated);
					rnd_node->setAttr("Pos", Pos);
					rnd_node->setAttr("FloorNumber", FlNumber);
					rnd_node->setAttr("Rotate", Rot);
					rnd_node->setAttr("Scale", Scale);
					rnd_node->setAttr("ColorRGB", ColorRGB);
					rnd_node->setAttr("Material", Material);
					rnd_node->setAttr("ProjectionType", ProjectionType);
					rnd_node->setAttr("Deferred", Deferred);
					rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
					rnd_node->setAttr("SortPriority", SortPriority);
					rnd_node->setAttr("ProjectionDepth", ProjectionDepth);
					rnd_node->setAttr("RndFlags", rnd_flags);
					obj_added_to_fl = true;
				}
			}
			else if ((*it)->GetRenderNodeType() == eERType_Vegetation)
			{
				IVegetation *pVeg = static_cast<IVegetation*>((*it));
				if (pVeg)
				{
					XmlNodeRef rnd_node = Objects_node->newChild("Object");
					if (!rnd_node)
						continue;

					string type = "Vegetation";
					Vec3 Pos = pVeg->GetPos();
					Vec3 Scale = Vec3(pVeg->GetScale());
					Matrix34 tm = Matrix34::CreateIdentity();
					pVeg->GetEntityStatObj(0, 0, &tm);
					if (Scale != Vec3(1, 1, 1))
						tm.OrthonormalizeFast();

					Ang3 angles = Ang3::GetAnglesXYZ(tm);
					Quat Rot = Quat::CreateRotationXYZ(angles);
					string Prefab = "0";
					//char idName[3] = "0";
					IStatInstGroup Group;
					if (gEnv->p3DEngine->GetStatInstGroup(pVeg->GetStatObjGroupId(), Group))
					{
						Prefab = Group.szFileName;
						//Prefab = itoa(pVeg->GetStatObjGroupId(), idName, 10);
						//Prefab = idName;
					}
					rnd_node->setAttr("Type", type);
					rnd_node->setAttr("Pos", Pos);
					rnd_node->setAttr("Rotate", Rot);
					rnd_node->setAttr("Scale", Scale);
					rnd_node->setAttr("Prefab", Prefab);
					obj_added_to_fl = true;
				}
			}
		}
	}
	//finaly save//add check for added objects, if succesfully_gen_oblects really empty do not save XML file
	if (!obj_added_to_fl)
		return;

	Objects_node->saveToFile(file_pth);
}

void CProceduralGenerator::SaveStreamingSectorsInXML(const string &path)
{
	if (!GetEntity())
		return;

	//too big xml file
	if ((generated_objects_in_stream_rd > 1000000) || use_generation_manager_thread)
	{
		SaveStreamingSectorsSlicedInXMLFiles(path);
		return;
	}

	string file_nmn = GetEntity()->GetName()+ string("_streaming_support") + string(".grp");
	string file_pth = path + file_nmn;
	XmlNodeRef Objects_node = GetISystem()->CreateXmlNode("Objects");
	if (!Objects_node)
	{
		CryLogAlways("Cannot create xml node Objects(fnc CProceduralGenerator::SaveObjectsInXML)");
		return;
	}
	bool obj_added_to_fl = false;
	string layer = "StreamingObjectsLayerXc01";
	string LayerGUID = GenerateRNDGUID();
	string type = "";
	std::vector<SGenerationSector *>::const_iterator it_x = generated_sectors.begin();
	std::vector<SGenerationSector *>::const_iterator end_x = generated_sectors.end();
	int count_x = generated_sectors.size();
	if (count_x > 0)
	{
		for (int ir = 0; ir < count_x, it_x != end_x; ir++, ++it_x)
		{
			SGenerationSector *gen_sec = (*it_x);
			if (gen_sec == nullptr)
				continue;

			std::vector<SQuedGenObject>::const_iterator it = gen_sec->sector_gen_oblects.begin();
			std::vector<SQuedGenObject>::const_iterator end = gen_sec->sector_gen_oblects.end();
			int count = gen_sec->sector_gen_oblects.size();
			if (count > 0)
			{
				for (int il = 0; il < count, it != end; il++, ++it)
				{
					const SQuedGenObject&Obj_gt = (*it);
					if (Obj_gt.object_type == 1)
					{
						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						type = "Brush";
						//string layer = "Main";
						//string LayerGUID = "{E0199113-42D5-4141-9C20-09D9C93AD7D4}";
						string objGUId = GenerateRNDGUID();
						string nmn_generated = "Brush_Generated_";
						nmn_generated = GenerateNodeXName(nmn_generated);
						int FlNumber = -1;
						int ColorRGB = 16777215;
						int MatLayersMask = 0;
						string Prefab = "";
						Prefab = Obj_gt.object_mdl;

						int OutdoorOnly = 0; int CastShadowMaps = 1; int RainOccluder = 1;
						int SupportSecondVisarea = 0; int DynamicDistanceShadows = 0; int Hideable = 0;
						int LodRatio = Obj_gt.vDistRate[0];
						int ViewDistRatio = Obj_gt.vDistRate[1];
						int NotTriangulate = 0;
						int NoDynamicWater = 0;
						int AIRadius = -1; int NoStaticDecals = 0; int NoAmnbShadowCaster = 0;
						int RecvWind = 0; int Occluder = 0; int DrawLast = 0; int ShadowLodBias = 0;
						int RndFlags = 1610612744;

						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Layer", layer);
						rnd_node->setAttr("LayerGUID", LayerGUID);
						rnd_node->setAttr("Id", objGUId);
						rnd_node->setAttr("Name", nmn_generated);
						rnd_node->setAttr("Pos", Obj_gt.object_pos);
						rnd_node->setAttr("FloorNumber", FlNumber);
						rnd_node->setAttr("Rotate", Obj_gt.object_rot);
						rnd_node->setAttr("Scale", Obj_gt.object_scale);
						rnd_node->setAttr("ColorRGB", ColorRGB);
						rnd_node->setAttr("MatLayersMask", MatLayersMask);
						rnd_node->setAttr("Prefab", Prefab);
						rnd_node->setAttr("OutdoorOnly", OutdoorOnly);
						rnd_node->setAttr("CastShadowMaps", CastShadowMaps);
						rnd_node->setAttr("RainOccluder", RainOccluder);
						rnd_node->setAttr("SupportSecondVisarea", SupportSecondVisarea);
						rnd_node->setAttr("DynamicDistanceShadows", DynamicDistanceShadows);
						rnd_node->setAttr("Hideable", Hideable);
						rnd_node->setAttr("LodRatio", LodRatio);
						rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
						rnd_node->setAttr("NotTriangulate", NotTriangulate);
						rnd_node->setAttr("NoDynamicWater", NoDynamicWater);
						rnd_node->setAttr("AIRadius", AIRadius);
						rnd_node->setAttr("NoStaticDecals", NoStaticDecals);
						rnd_node->setAttr("NoAmnbShadowCaster", NoAmnbShadowCaster);
						rnd_node->setAttr("RecvWind", RecvWind);
						rnd_node->setAttr("Occluder", Occluder);
						rnd_node->setAttr("DrawLast", DrawLast);
						rnd_node->setAttr("ShadowLodBias", ShadowLodBias);
						rnd_node->setAttr("RndFlags", RndFlags);
						rnd_node->setAttr("SectorIds", Obj_gt.stream_sector_id);
						obj_added_to_fl = true;
					}
					else if (Obj_gt.object_type == 2)
					{

						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						type = "Decal";
						string objGUId = GenerateRNDGUID();
						string nmn_generated = "Decal_Generated_";
						nmn_generated = GenerateNodeXName(nmn_generated);
						int FlNumber = -1;
						int ColorRGB = 16744319;
						string Material = "";
						Material = Obj_gt.object_mdl;

						int ProjectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
						int Deferred = 1;
						int ViewDistRatio = Obj_gt.vDistRate[1];
						int SortPriority = cry_random(1, 150);
						int rnd_flags = 0;
						float ProjectionDepth = cry_random(0.5f, 3.0f);
						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Layer", layer);
						rnd_node->setAttr("LayerGUID", LayerGUID);
						rnd_node->setAttr("Id", objGUId);
						rnd_node->setAttr("Name", nmn_generated);
						rnd_node->setAttr("Pos", Obj_gt.object_pos);
						rnd_node->setAttr("FloorNumber", FlNumber);
						rnd_node->setAttr("Rotate", Obj_gt.object_rot);
						rnd_node->setAttr("Scale", Obj_gt.object_scale);
						rnd_node->setAttr("ColorRGB", ColorRGB);
						rnd_node->setAttr("Material", Material);
						rnd_node->setAttr("ProjectionType", ProjectionType);
						rnd_node->setAttr("Deferred", Deferred);
						rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
						rnd_node->setAttr("SortPriority", SortPriority);
						rnd_node->setAttr("ProjectionDepth", ProjectionDepth);
						rnd_node->setAttr("RndFlags", rnd_flags);
						rnd_node->setAttr("SectorIds", Obj_gt.stream_sector_id);
						obj_added_to_fl = true;

					}
					else if (Obj_gt.object_type == 4)
					{

						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						type = "Vegetation";
						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Pos", Obj_gt.object_pos);
						rnd_node->setAttr("Rotate", Obj_gt.object_rot);
						rnd_node->setAttr("Scale", Obj_gt.object_scale);
						rnd_node->setAttr("Prefab", Obj_gt.object_mdl);
						rnd_node->setAttr("SectorIds", Obj_gt.stream_sector_id);
						obj_added_to_fl = true;
					}
				}
			}
		}
	}

	//finaly save//add check for added objects, if succesfully_gen_oblects really empty do not save XML file
	if (!obj_added_to_fl)
		return;

	Objects_node->saveToFile(file_pth);
}

void CProceduralGenerator::SaveStreamingSectorsSlicedInXMLFiles(const string & path)
{
	std::vector<SGenerationSector *>::const_iterator it_x = generated_sectors.begin();
	std::vector<SGenerationSector *>::const_iterator end_x = generated_sectors.end();
	int count_x = generated_sectors.size();
	if (count_x > 0)
	{
		for (int ir = 0; ir < count_x, it_x != end_x; ir++, ++it_x)
		{
			SGenerationSector *gen_sec = (*it_x);
			if (gen_sec == nullptr)
				continue;

			string file_nmn = "";
			file_nmn.Format("Stream_sector_%i_%s%s%s", gen_sec->GetSectorId(), GetEntity()->GetName(), "_streaming_support", ".grp");
			string file_pth = path + file_nmn;
			XmlNodeRef Objects_node = GetISystem()->CreateXmlNode("Objects");
			if (!Objects_node)
			{
				CryLogAlways("Cannot create xml node Objects(fnc CProceduralGenerator::SaveStreamingSectorsSlicedInXMLFiles)");
				continue;
			}
			bool obj_added_to_fl = false;
			string layer = "StreamingObjectsLayerXc01";
			string LayerGUID = GenerateRNDGUID();
			string type = "";
			std::vector<SQuedGenObject>::const_iterator it = gen_sec->sector_gen_oblects.begin();
			std::vector<SQuedGenObject>::const_iterator end = gen_sec->sector_gen_oblects.end();
			int count = gen_sec->sector_gen_oblects.size();
			if (count > 0)
			{
				for (int il = 0; il < count, it != end; il++, ++it)
				{
					const SQuedGenObject&Obj_gt = (*it);
					if (Obj_gt.object_type == 1)
					{
						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						type = "Brush";
						//string layer = "Main";
						//string LayerGUID = "{E0199113-42D5-4141-9C20-09D9C93AD7D4}";
						string objGUId = GenerateRNDGUID();
						string nmn_generated = "Brush_Generated_";
						nmn_generated = GenerateNodeXName(nmn_generated);
						int FlNumber = -1;
						int ColorRGB = 16777215;
						int MatLayersMask = 0;
						string Prefab = "";
						Prefab = Obj_gt.object_mdl;

						int OutdoorOnly = 0; int CastShadowMaps = 1; int RainOccluder = 1;
						int SupportSecondVisarea = 0; int DynamicDistanceShadows = 0; int Hideable = 0;
						int LodRatio = Obj_gt.vDistRate[0];
						int ViewDistRatio = Obj_gt.vDistRate[1];
						int NotTriangulate = 0;
						int NoDynamicWater = 0;
						int AIRadius = -1; int NoStaticDecals = 0; int NoAmnbShadowCaster = 0;
						int RecvWind = 0; int Occluder = 0; int DrawLast = 0; int ShadowLodBias = 0;
						int RndFlags = 1610612744;

						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Layer", layer);
						rnd_node->setAttr("LayerGUID", LayerGUID);
						rnd_node->setAttr("Id", objGUId);
						rnd_node->setAttr("Name", nmn_generated);
						rnd_node->setAttr("Pos", Obj_gt.object_pos);
						rnd_node->setAttr("FloorNumber", FlNumber);
						rnd_node->setAttr("Rotate", Obj_gt.object_rot);
						rnd_node->setAttr("Scale", Obj_gt.object_scale);
						rnd_node->setAttr("ColorRGB", ColorRGB);
						rnd_node->setAttr("MatLayersMask", MatLayersMask);
						rnd_node->setAttr("Prefab", Prefab);
						rnd_node->setAttr("OutdoorOnly", OutdoorOnly);
						rnd_node->setAttr("CastShadowMaps", CastShadowMaps);
						rnd_node->setAttr("RainOccluder", RainOccluder);
						rnd_node->setAttr("SupportSecondVisarea", SupportSecondVisarea);
						rnd_node->setAttr("DynamicDistanceShadows", DynamicDistanceShadows);
						rnd_node->setAttr("Hideable", Hideable);
						rnd_node->setAttr("LodRatio", LodRatio);
						rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
						rnd_node->setAttr("NotTriangulate", NotTriangulate);
						rnd_node->setAttr("NoDynamicWater", NoDynamicWater);
						rnd_node->setAttr("AIRadius", AIRadius);
						rnd_node->setAttr("NoStaticDecals", NoStaticDecals);
						rnd_node->setAttr("NoAmnbShadowCaster", NoAmnbShadowCaster);
						rnd_node->setAttr("RecvWind", RecvWind);
						rnd_node->setAttr("Occluder", Occluder);
						rnd_node->setAttr("DrawLast", DrawLast);
						rnd_node->setAttr("ShadowLodBias", ShadowLodBias);
						rnd_node->setAttr("RndFlags", RndFlags);
						rnd_node->setAttr("SectorIds", Obj_gt.stream_sector_id);
						obj_added_to_fl = true;
					}
					else if (Obj_gt.object_type == 2)
					{

						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						type = "Decal";
						string objGUId = GenerateRNDGUID();
						string nmn_generated = "Decal_Generated_";
						nmn_generated = GenerateNodeXName(nmn_generated);
						int FlNumber = -1;
						int ColorRGB = 16744319;
						string Material = "";
						Material = Obj_gt.object_mdl;

						int ProjectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
						int Deferred = 1;
						int ViewDistRatio = Obj_gt.vDistRate[1];
						int SortPriority = cry_random(1, 150);
						int rnd_flags = 0;
						float ProjectionDepth = cry_random(0.5f, 3.0f);
						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Layer", layer);
						rnd_node->setAttr("LayerGUID", LayerGUID);
						rnd_node->setAttr("Id", objGUId);
						rnd_node->setAttr("Name", nmn_generated);
						rnd_node->setAttr("Pos", Obj_gt.object_pos);
						rnd_node->setAttr("FloorNumber", FlNumber);
						rnd_node->setAttr("Rotate", Obj_gt.object_rot);
						rnd_node->setAttr("Scale", Obj_gt.object_scale);
						rnd_node->setAttr("ColorRGB", ColorRGB);
						rnd_node->setAttr("Material", Material);
						rnd_node->setAttr("ProjectionType", ProjectionType);
						rnd_node->setAttr("Deferred", Deferred);
						rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
						rnd_node->setAttr("SortPriority", SortPriority);
						rnd_node->setAttr("ProjectionDepth", ProjectionDepth);
						rnd_node->setAttr("RndFlags", rnd_flags);
						rnd_node->setAttr("SectorIds", Obj_gt.stream_sector_id);
						obj_added_to_fl = true;

					}
					else if (Obj_gt.object_type == 4)
					{

						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						type = "Vegetation";
						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Pos", Obj_gt.object_pos);
						rnd_node->setAttr("Rotate", Obj_gt.object_rot);
						rnd_node->setAttr("Scale", Obj_gt.object_scale);
						rnd_node->setAttr("Prefab", Obj_gt.object_mdl);
						rnd_node->setAttr("SectorIds", Obj_gt.stream_sector_id);
						obj_added_to_fl = true;
					}
				}
			}
			if (!obj_added_to_fl)
				continue;

			Objects_node->saveToFile(file_pth);
		}
	}
}

string CProceduralGenerator::GenerateRNDGUID()
{
	string GUID = RANDOMNAMESXRC::GenerateObjectGUID();
	return GUID;
}

AABB CProceduralGenerator::GetRandomAABBFromTriagleInShape(const std::vector<Vec3>& polygon)
{
	return AABB();
}

Vec3 CProceduralGenerator::GetRandomPointInShape(const std::vector<Vec3>& polygon, float minz, float maxz)
{
	bool bUseTriangulation = true;
	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		props->GetValue("bUseTriangulation", bUseTriangulation);
	}

	Vec3 point(ZERO);
	std::vector<Vec3> ench_polygon;
	ench_polygon.clear();
	ench_polygon = polygon;
	int count = polygon.size();
	/*std::vector<Vec3>::const_iterator pItx = polygon.begin();
	std::vector<Vec3>::const_iterator pItEndx = polygon.end();
	for (int i = 0; i < count, pItx != pItEndx; i++, ++pItx)
	{
		std::vector<Vec3>::const_iterator pItNextx = pItx;
		if (pItNextx == pItEndx)
			continue;

		++pItNextx;
		Vec3 v1 = (*pItx);
		Vec3 v2 = (*pItNextx);
		ench_polygon.push_back(v1);
		float dv = v1.GetDistance(v2);
		Vec3 line_direction = v2 - v1;
		Vec3 line_direction_n = line_direction.GetNormalized();
		int num_seq = int(dv/1.2f);
		float coof = 1.0f / num_seq;
		for (int ic = 0; ic < num_seq; ic++)
		{
			if (ic > 0 && ic != num_seq)
			{
				float mult_vv = ic * coof;
				Vec3 lpn = v1 + (line_direction_n * mult_vv);
				ench_polygon.push_back(lpn);
			}
		}
	}*/
	count = ench_polygon.size();
	if (count <= 4 || !bUseTriangulation)
	{
		Vec3 VerLst(ZERO);
		float dist = 0.0f;
		std::vector<Vec3>::const_iterator pIterBegin = ench_polygon.begin();
		std::vector<Vec3>::const_iterator pIterEnd = ench_polygon.end();
		for (int i = 0; i < count, pIterBegin != pIterEnd; i++, ++pIterBegin)
		{
			if (i > 0)
			{
				Vec3 v1 = (*pIterBegin);
				float dv = v1.GetDistance((*ench_polygon.begin()));
				if (dist < dv)
				{
					dist = dv;
					VerLst = v1;
				}
			}
		}
		AABB trgBounds;
		Vec3 minf;
		if ((*ench_polygon.begin()).GetLength() <= VerLst.GetLength())
			minf = (*ench_polygon.begin());
		else
			minf = VerLst;
		Vec3 maxf;
		if ((*ench_polygon.begin()).GetLength() >= VerLst.GetLength())
			maxf = (*ench_polygon.begin());
		else
			maxf = VerLst;

		minf.z = minz;
		maxf.z = maxz;
		trgBounds.min = minf;
		trgBounds.max = maxf;
		if (dist <= 1.2f)
			dist = 1.2f;

		dist *= 1.5f;

		Vec3 rnd_dist(cry_random(0.0f, dist), cry_random(0.0f, dist), 0.0f);
		int rndxc = cry_random(0, 1);
		if (rndxc == 0)
		{
			rnd_dist = Vec3(cry_random(-dist, 0.0f), cry_random(-dist, 0.0f), 0.0f);
		}
		Vec3 fcxe1 = trgBounds.min + rnd_dist;
		Vec3 fcxe2 = trgBounds.max + rnd_dist;
		Vec3 pnt(ZERO);
		if (fcxe1.GetLength() > fcxe2.GetLength())
			pnt = GenerateVecRandomValue(fcxe2, fcxe1);
		else
			pnt = GenerateVecRandomValue(fcxe1, fcxe2);

		point = pnt;
		return point;
	}

	//std::vector<Vec3> ench_polygon_nv;
	//ench_polygon_nv.clear();
	//ench_polygon_nv = ench_polygon_nv;
	float dist = 0.0f;
	Vec3 vertex_last(ZERO);
	PodArray<Vec2>* poly2d = new PodArray<Vec2>;
	poly2d->Clear();
	std::vector<Vec3>::const_iterator pIterBegin = ench_polygon.begin();
	std::vector<Vec3>::const_iterator pIterEnd = ench_polygon.end();
	//poly2d->resize(count);
	if (count > 0)
	{
		Vec3 fts = (*pIterBegin);
		for (int i = 0; i < count, pIterBegin != pIterEnd; i++, ++pIterBegin)
		{
			if (i > 0)
			{
				Vec3 v1 = (*pIterBegin);
				float dv = v1.GetDistance(fts);
				if (dist < dv)
				{
					dist = dv;
					vertex_last = v1;
				}
			}
			poly2d->push_back(Vec2(*pIterBegin));
		}
	}
	PodArray<Vec2>* triangles = new PodArray<Vec2>;
	triangles->Clear();
	//triangles->resize(0);
	//triangles->reserve(100);
	PodArray<uint8>* inds = new PodArray<uint8>;
	inds->Clear();
	//inds->resize(0);
	//inds->reserve(100);
	TriangulatePolygon2D(poly2d->begin(), count, triangles, inds);
	count = triangles->size();
	Vec2* it = triangles->begin();
	Vec2* end = triangles->end();
	if (count > 0)
	{
		//int num_triangs = count / 3;
		//int selected_trg_nm = cry_random(0, num_triangs);
		int selected_trg_nm = cry_random(0, count-2);
		Vec2 sel_triangle[3];
		Vec2 temp_triangle[3];
		Vec2 sel_quand_four_point=Vec2(vertex_last);
		for (int i = 0; i < 3; i++)
		{
			sel_triangle[i] = Vec2(vertex_last);
			temp_triangle[i] = Vec2(vertex_last);
		}
		float dist_trgx = 0.0f;
		Vec2 dn_tgn = Vec2(vertex_last);
		bool triangle_selected = false;
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			Vec2* itd = it;
			Vec2 thf1 = (*itd);
			itd++;
			if (itd == end)
				break;

			Vec2 thf2 = (*itd);
			itd++;
			if (itd == end)
				break;

			Vec2 thf3 = (*itd);
			if (selected_trg_nm == i)
			{
				triangle_selected = true;
				sel_triangle[0] = thf1;
				sel_triangle[1] = thf2;
				sel_triangle[2] = thf3;
				itd++;
				sel_quand_four_point = (*itd);
				break;
			}
			temp_triangle[0] = thf1;
			temp_triangle[1] = thf2;
			temp_triangle[2] = thf3;
		}

		if (!triangle_selected)
		{
			for (int i = 0; i < 3; i++)
			{
				sel_triangle[i] = temp_triangle[i];
			}
		}

		for (int i = 0; i < 3; i++)
		{
			if (i > 0)
			{
				float dv = sel_triangle[i].GetDistance(sel_triangle[0]);
				if (dist_trgx < dv)
				{
					dist_trgx = dv;
					dn_tgn = sel_triangle[i];
				}
			}
		}

		if (sel_quand_four_point != Vec2(vertex_last))
		{
			float dv = sel_triangle[0].GetDistance(sel_quand_four_point);
			if (dv > dist_trgx)
			{
				dist_trgx = dv;
				dn_tgn = sel_quand_four_point;
			}
		}

		AABB trgBounds;
		Vec3 minf;
		if (sel_triangle[0].GetLength() <= dn_tgn.GetLength())
			minf = sel_triangle[0];
		else
			minf = dn_tgn;
		Vec3 maxf;
		if (sel_triangle[0].GetLength() >= dn_tgn.GetLength())
			maxf = sel_triangle[0];
		else
			maxf = dn_tgn;

		minf.z = minz;
		maxf.z = maxz;
		trgBounds.min = minf;
		trgBounds.max = maxf;
		if (dist_trgx <= 0.2f)
			dist_trgx = 3.2f;

		dist_trgx *= 2.3f;

		Vec3 rnd_dist(cry_random(0.0f, dist_trgx), cry_random(0.0f, dist_trgx), 0.0f);
		int rndxc = cry_random(0, 1);
		if (rndxc == 0)
		{
			rnd_dist = Vec3(cry_random(-dist_trgx, 0.0f), cry_random(-dist_trgx, 0.0f), 0.0f);
		}
		Vec3 fcxe1 = trgBounds.min + rnd_dist;
		Vec3 fcxe2 = trgBounds.max + rnd_dist;
		Vec3 pnt(ZERO);
		pnt = (fcxe1 + fcxe2)*0.5000f;
		int rndxc2 = cry_random(0, 1);
		if (rndxc2 == 0)
		{
			pnt = GenerateVecRandomValue(fcxe2, pnt);
		}
		else
		{
			pnt = GenerateVecRandomValue(fcxe1, pnt);
		}
		//if (pnt.GetLength() > fcxe2.GetLength())
		//	pnt = cry_random(fcxe2, pnt);
		//else
		//	pnt = cry_random(fcxe1, fcxe2);
		/*if (fcxe1.GetLength() > fcxe2.GetLength())
			pnt = cry_random(fcxe2, fcxe1);
		else
			pnt = cry_random(fcxe1, fcxe2);*/

		point = pnt;
	}
	delete (poly2d);
	delete (inds);
	delete (triangles);
	return point;
}

Vec3 CProceduralGenerator::GetRandomPointInSphere(Vec3 point, float radius)
{
	float dist = radius * 2.0f;
	Vec3 rnd_pt = Vec3(ZERO);
	Vec3 x_dr = (point - Vec3(0.0f, 0.0f, point.z)).GetNormalizedSafe();
	Vec3 x_pt = point + (x_dr * dist);
	x_pt.z += radius / 2.0f;
	Vec3 x_pt2 = point - (x_dr * dist);
	x_pt2.z -= radius / 2.0f;
	bool ft_iter = false;
	while ((point.GetDistance(rnd_pt) > radius) || !ft_iter)
	{
		Vec3 rnd_dist(cry_random(0.0f, dist), cry_random(0.0f, dist), 0.0f);
		int rndxc = cry_random(0, 1);
		if (rndxc == 0)
		{
			rnd_dist = Vec3(cry_random(-dist, 0.0f), cry_random(-dist, 0.0f), 0.0f);
		}
		Vec3 fcxe1 = x_pt2 + rnd_dist;
		Vec3 fcxe2 = x_pt + rnd_dist;
		Vec3 pnt(ZERO);
		if (fcxe1.GetLength() > fcxe2.GetLength())
			pnt = GenerateVecRandomValue(fcxe2, fcxe1);
		else
			pnt = GenerateVecRandomValue(fcxe1, fcxe2);

		rnd_pt = pnt;
		ft_iter = true;
	}
	return rnd_pt;
}

float CProceduralGenerator::GetAdditionalObjectZCor(const float &baseZ)
{
	if (setting_data.gen_up_from_nrm_rnd == 0.0f)
		return baseZ;

	return baseZ + cry_random(-setting_data.gen_up_from_nrm_rnd, setting_data.gen_up_from_nrm_rnd);
}

void CProceduralGenerator::UpdateGenDirection()
{
	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (pScriptTable && pScriptTable->GetValue("Properties", props))
	{
		float GenDirectionRt = 0.0f;
		float GenDirectionHgt = 0.0f;
		props->GetValue("fGenDirectionRot", GenDirectionRt);
		if (GenDirectionRt > 3.5f)
		{
			GenDirectionRt = 3.5f;
		}
		else if (GenDirectionRt < -3.5f)
		{
			GenDirectionRt = -3.5f;
		}
		props->GetValue("fGenDirectionRHeight", GenDirectionHgt);
		if (GenDirectionHgt > 3.5f)
		{
			GenDirectionHgt = 3.5f;
		}
		else if (GenDirectionHgt < -3.5f)
		{
			GenDirectionHgt = -3.5f;
		}
		Vec3 ShapeWPos = GetEntity()->GetWorldPos();
		ShapeWPos.z += 3.0f;
		Quat rot_z = Quat::CreateRotationZ(GenDirectionRt);
		Vec3 defDir = Vec3(0, 0, -1);
		Vec3 newDir = defDir;
		newDir = newDir.GetRotated(Vec3(0, 1, 0), GenDirectionHgt);
		gen_direction = rot_z * newDir;
		gen_direction.normalize();
	}
}

void CProceduralGenerator::UpdateStreamingProcess()
{
	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	ITerrain* pTerrain = gEnv->p3DEngine->GetITerrain();
	if (!pTerrain)
		return;

	generatorWorldPoint = Vec2(GetEntity()->GetWorldPos().x, GetEntity()->GetWorldPos().y);
	CCamera& cam = GetISystem()->GetViewCamera();
	int new_nearest_steaming_segment = GetNearestStreamingSector(Vec2(cam.GetPosition()), sector_size);
	int sectors_number_hv = streaming_cover_size / sector_size;
	int sectors_number = sectors_number_hv*sectors_number_hv;
	if (generated_sectors.size() == 0)
	{
		int counter_hv = 0;
		generated_sectors.clear();
		loaded_sectors_ids.clear();
		generated_sectors.resize(0);
		loaded_sectors_ids.resize(0);
		//unloading_sectors_ids.clear();
		//unloading_sectors_ids.resize(0);
		Vec2i posx = Vec2i(generatorWorldPoint);
		for (int i = 0; i < sectors_number; ++i)
		{
			if (sectors_number_hv <= counter_hv)
			{
				counter_hv = 0;
				posx.x = int(generatorWorldPoint.x);
				posx.y += sector_size;
			}
			counter_hv++;
			SGenerationSector *gen_sec = new SGenerationSector();
			gen_sec->sector_id = i + 1;
			gen_sec->sector_size = sector_size;
			gen_sec->sector_start_point = Vec2i(posx.x, posx.y);
			gen_sec->sector_center_point = Vec2(Vec2((gen_sec->sector_start_point + Vec2i(sector_size, sector_size)) + gen_sec->sector_start_point) * 0.50000f);
			generated_sectors.push_back(gen_sec);
			posx.x += sector_size;
		}
		for (int i = 0; i < 9; ++i)
		{
			//SLoadedSector lds = SLoadedSector();

			int sec_id = new_nearest_steaming_segment;
			if (i > 0)
			{
				sec_id = -1;
			}

			if (i == 1)
			{
				sec_id = new_nearest_steaming_segment + sectors_number_hv;
			}
			else if (i == 2)
			{
				sec_id = new_nearest_steaming_segment - sectors_number_hv;
			}
			else if (i == 3)
			{
				sec_id = new_nearest_steaming_segment + 1;
			}
			else if (i == 4)
			{
				sec_id = new_nearest_steaming_segment - 1;
			}
			else if (i == 5)
			{
				sec_id = new_nearest_steaming_segment + sectors_number_hv + 1;
			}
			else if (i == 6)
			{
				sec_id = new_nearest_steaming_segment + sectors_number_hv - 1;
			}
			else if (i == 7)
			{
				sec_id = new_nearest_steaming_segment - sectors_number_hv + 1;
			}
			else if (i == 8)
			{
				sec_id = new_nearest_steaming_segment - sectors_number_hv - 1;
			}

			if (sec_id > sectors_number)
			{
				sec_id = sectors_number;
			}
			else if (sec_id < 1)
			{
				sec_id = 1;
			}

			for (int ic = 0; ic < loaded_sectors_ids.size(); ++ic)
			{
				if (loaded_sectors_ids[ic] == sec_id)
				{
					sec_id = -1;
				}
			}
			loaded_sectors_ids.push_back(sec_id);
		}
		return;
	}

	while (!gen_objects_queue.empty())
	{
		AddObjectToSectorQue(gen_objects_queue.front(), GetNearestStreamingSector(Vec2(gen_objects_queue.front().object_pos), sector_size));
		//GreateObjectFromQue(gen_objects_queue.front());
		gen_objects_queue.pop();
	}

	
	bool refresh_current_sectors = (old_nearest_steaming_segment < 0);
	if (old_nearest_steaming_segment != new_nearest_steaming_segment)
	{
		//loaded_sectors_ids.clear();
		//loaded_sectors_ids.resize(0);
		old_nearest_steaming_segment = new_nearest_steaming_segment;
		std::vector<int> loaded_sectors_ids2;
		loaded_sectors_ids2.clear();
		loaded_sectors_ids2.resize(0);
		for (int i = 0; i < 9; ++i)
		{
			//SLoadedSector lds = SLoadedSector();

			int sec_id = new_nearest_steaming_segment;
			if (i > 0)
			{
				sec_id = -1;
			}

			if (i == 1)
			{
				sec_id = new_nearest_steaming_segment + sectors_number_hv;
			}
			else if (i == 2)
			{
				sec_id = new_nearest_steaming_segment - sectors_number_hv;
			}
			else if (i == 3)
			{
				sec_id = new_nearest_steaming_segment + 1;
			}
			else if (i == 4)
			{
				sec_id = new_nearest_steaming_segment - 1;
			}
			else if (i == 5)
			{
				sec_id = new_nearest_steaming_segment + sectors_number_hv + 1;
			}
			else if (i == 6)
			{
				sec_id = new_nearest_steaming_segment + sectors_number_hv - 1;
			}
			else if (i == 7)
			{
				sec_id = new_nearest_steaming_segment - sectors_number_hv + 1;
			}
			else if (i == 8)
			{
				sec_id = new_nearest_steaming_segment - sectors_number_hv - 1;
			}

			if (sec_id > sectors_number)
			{
				sec_id = sectors_number;
			}
			else if (sec_id < 1)
			{
				sec_id = 1;
			}

			for (int ic = 0; ic < loaded_sectors_ids2.size(); ++ic)
			{
				if (loaded_sectors_ids2[ic] == sec_id)
				{
					sec_id = -1;
				}
			}
			loaded_sectors_ids2.push_back(sec_id);
		}

		for (int i = 0; i < loaded_sectors_ids2.size(); ++i)
		{
			if (loaded_sectors_ids2[i] > 0)
				GenerateObjectsInSector(loaded_sectors_ids2[i]);
		}

		for (int ic = 0; ic < loaded_sectors_ids.size(); ++ic)
		{
			bool clear_sector = true;
			for (int i = 0; i < loaded_sectors_ids2.size(); ++i)
			{
				if (loaded_sectors_ids[ic] == loaded_sectors_ids2[i])
				{
					clear_sector = false;
				}
			}
			if (clear_sector)
			{
				if (loaded_sectors_ids[ic] > 0 && !IsSectorInUnloadedSectors(loaded_sectors_ids[ic]))
					ClearGeneratedRenderNodesInSector(loaded_sectors_ids[ic], true);
			}
		}
		loaded_sectors_ids = loaded_sectors_ids2;
	}
	else
	{
		
	}

	if (refresh_current_sectors)
	{
		for (int i = 0; i < loaded_sectors_ids.size(); ++i)
		{
			if (loaded_sectors_ids[i] > 0)
				GenerateObjectsInSector(loaded_sectors_ids[i]);
		}
	}
}

void CProceduralGenerator::DoPaint(Vec3 point, float radius)
{
	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	if (!gEnv->pPhysicalWorld)
		return;

	UpdateGenDirection();

	if (!generation_setting_data_refilled)
		RefillGenerationSettingData();

	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = "";
		brushes[i] = "";
		arc_entites[i] = "";
		vegetations[i] = "";
		surf_ids[i] = 0;
		veg_inds[i] = 0;
		brush_params[i] = 0;
	}


	bool decal_gen = false;
	bool brushes_gen = false;
	bool arc_ents_gen = false;
	bool terrain_surf_ids_gen = false;
	bool vegetation_gen = false;
	float vegetations_gen_coof = 0;
	float vegetations_base_scale = 0;
	float vegetations_rnd_scale = 0;
	float vegetations_gen_up_ht = 0;
	float vegetations_gen_rnd_pwr = 0;
	bool gen_vegetations_floating = false;
	bool gen_vegetations_rnd_rot_full = false;
	float decals_gen_coof = 0;
	int decals_aval_num = 0;
	float decals_base_scale = 0;
	float decals_rnd_scale = 0;
	float decals_gen_up_ht = 0;
	float decals_gen_rnd_pwr = 0;
	int gen_updwn = 0;
	float gen_up_from_nrm = 0;
	bool gen_arr_dist_points_for_all = false;
	bool gen_brushes_floating = false;
	bool gen_brushes_rnd_rot_full = false;
	float gen_brushes_base_scale = 0.0f;
	float gen_brushes_rnd_scale = 0.0f;
	float gen_brushes_up_hgt = 0.0f;
	float gen_brushes_rnd_pwr = 0.0f;
	float gen_brushes_coof = 0;
	float gen_arc_ent_coof = 0;
	float gen_surf_coof = 0;

	bool gen_arc_ent_floating = false;
	bool gen_arc_ent_rnd_rot_full = false;
	float gen_arc_ent_base_scale = 0.0f;
	float gen_arc_ent_rnd_scale = 0.0f;
	float gen_arc_ent_up_hgt = 0.0f;
	float gen_arc_ent_rnd_pwr = 0.0f;
	float gen_surf_rnd_pwr = 0.0f;

	if (!props)
		return;

	bool UseCameraDirectonInPaintMode = false;
	props->GetValue("bUseCameraDirectonInPaintMode", UseCameraDirectonInPaintMode);

	//int sector_size = 128;
	props->GetValue("fStreamingSectorSize", sector_size);

	//-----------Slope&Height Setup-------------
	float fMinHeight = 0.0f;
	float fMaxHeight = 11111.0f;
	float fMinSlope = 0.0;
	float fMaxSlope = 180.0;
	props->GetValue("fMinHeight", fMinHeight);
	props->GetValue("fMaxHeight", fMaxHeight);
	props->GetValue("fMinSlope", fMinSlope);
	props->GetValue("fMaxSlope", fMaxSlope);
	//------------------------------------------
	props->GetValue("nGenUpDown", gen_updwn);
	//decals------------------------------
	props->GetValue("bGenerateDecals", decal_gen);
	props->GetValue("fDecalsGenCoof", decals_gen_coof);
	props->GetValue("fileDecalOne", decals[0]);
	props->GetValue("fileDecalTwo", decals[1]);
	props->GetValue("fileDecalThree", decals[2]);
	props->GetValue("fileDecalFour", decals[3]);
	props->GetValue("fileDecalFive", decals[4]);
	props->GetValue("fDecalsGenBaseScale", decals_base_scale);
	props->GetValue("fDecalsGenRandomScale", decals_rnd_scale);
	props->GetValue("fDecalsGenUpHeight", decals_gen_up_ht);
	props->GetValue("fDecalsGenRandPower", decals_gen_rnd_pwr);
	//---------------------------------
	props->GetValue("bUseOneOfGenPointsArrToDistrAllObj", gen_arr_dist_points_for_all);
	props->GetValue("fHeigthUpFromNormal", gen_up_from_nrm);
	//brushes--------------------------
	props->GetValue("bGenerateBrushes", brushes_gen);
	props->GetValue("objBrushOne", brushes[0]);
	props->GetValue("objBrushTwo", brushes[1]);
	props->GetValue("objBrushThree", brushes[2]);
	props->GetValue("objBrushFour", brushes[3]);
	props->GetValue("objBrushFive", brushes[4]);
	props->GetValue("nBrushOnePhyPrm", brush_params[0]);
	props->GetValue("nBrushTwoPhyPrm", brush_params[1]);
	props->GetValue("nBrushThreePhyPrm", brush_params[2]);
	props->GetValue("nBrushFourPhyPrm", brush_params[3]);
	props->GetValue("nBrushFivePhyPrm", brush_params[4]);
	props->GetValue("bBrushesGenFloating", gen_brushes_floating);
	props->GetValue("fBrushesGenCoof", gen_brushes_coof);
	props->GetValue("fBrushesGenBaseScale", gen_brushes_base_scale);
	props->GetValue("fBrushesGenRandomScale", gen_brushes_rnd_scale);
	props->GetValue("fBrushesGenUpHeight", gen_brushes_up_hgt);
	props->GetValue("fBrushesGenRandPower", gen_brushes_rnd_pwr);
	props->GetValue("bBrushesGenFullRandRotation", gen_brushes_rnd_rot_full);
	//---------------------------------
	//Archetype entites----------------
	props->GetValue("bGenerateArcEntites", arc_ents_gen);
	props->GetValue("bArcEntitesGenFloating", gen_arc_ent_floating);
	props->GetValue("sArcEntityOne", arc_entites[0]);
	props->GetValue("sArcEntityTwo", arc_entites[1]);
	props->GetValue("sArcEntityThree", arc_entites[2]);
	props->GetValue("sArcEntityFour", arc_entites[3]);
	props->GetValue("sArcEntityFive", arc_entites[4]);
	props->GetValue("fArcEntitesGenCoof", gen_arc_ent_coof);
	props->GetValue("fArcEntitesGenBaseScale", gen_arc_ent_base_scale);
	props->GetValue("fArcEntitesGenRandomScale", gen_arc_ent_rnd_scale);
	props->GetValue("fArcEntitesGenUpHeight", gen_arc_ent_up_hgt);
	props->GetValue("fArcEntitesGenRandPower", gen_arc_ent_rnd_pwr);
	props->GetValue("bArcEntitesGenFullRandRotation", gen_arc_ent_rnd_rot_full);
	//---------------------------------
	//terrain surfaces ids------------------
	props->GetValue("bGenerateTerrainSurfaceIds", terrain_surf_ids_gen);
	props->GetValue("nTerrainSurfaceIdOne", surf_ids[0]);
	props->GetValue("nTerrainSurfaceIdTwo", surf_ids[1]);
	props->GetValue("nTerrainSurfaceIdThree", surf_ids[2]);
	props->GetValue("nTerrainSurfaceIdFour", surf_ids[3]);
	props->GetValue("nTerrainSurfaceIdFive", surf_ids[4]);
	props->GetValue("nTerrainSurfaceGenCoof", gen_surf_coof);
	props->GetValue("fTerrainSurfaceGenRandPower", gen_surf_rnd_pwr);
	//---------------------------------
	//vegetations------------------
	props->GetValue("bGenerateVegetations", vegetation_gen);
	props->GetValue("objVegetationOne", vegetations[0]);
	props->GetValue("objVegetationTwo", vegetations[1]);
	props->GetValue("objVegetationThree", vegetations[2]);
	props->GetValue("objVegetationFour", vegetations[3]);
	props->GetValue("objVegetationFive", vegetations[4]);
	props->GetValue("fVegetationIndexOne", veg_inds[0]);
	props->GetValue("fVegetationIndexTwo", veg_inds[1]);
	props->GetValue("fVegetationIndexThree", veg_inds[2]);
	props->GetValue("fVegetationIndexFour", veg_inds[3]);
	props->GetValue("fVegetationIndexFive", veg_inds[4]);
	props->GetValue("bVegetationsGenFloating", gen_vegetations_floating);
	props->GetValue("fVegetationsGenCoof", vegetations_gen_coof);
	props->GetValue("fVegetationsGenBaseScale", vegetations_base_scale);
	props->GetValue("fVegetationsGenRandomScale", vegetations_rnd_scale);
	props->GetValue("fVegetationsGenUpHeight", vegetations_gen_up_ht);
	props->GetValue("fVegetationsGenRandPower", vegetations_gen_rnd_pwr);
	props->GetValue("bVegetationsGenFullRandRotation", gen_vegetations_rnd_rot_full);
	bool veg_atm = false;
	float veg_bending = 0.0f;
	props->GetValue("fVegetationsBending", veg_bending);
	props->GetValue("bVegetationsAutoMerged", veg_atm);
	//---------------------------------
	float z_gen_rot_rand_val = 0.0f;
	props->GetValue("fGenZRotationRandomVal", z_gen_rot_rand_val);

	int obj_view_params[2];
	for (int i = 0; i < 2; i++)
	{
		obj_view_params[i] = 0;
	}
	props->GetValue("fGenObjectsViewDistance", obj_view_params[0]);
	props->GetValue("fGenObjectsLodDistance", obj_view_params[1]);
	bool gen_olly_on_terrain = false;
	props->GetValue("bGenOnTerrainOnly", gen_olly_on_terrain);
	int gen_on_stat_objs = ent_static;
	if (gen_olly_on_terrain)
		gen_on_stat_objs = 0;

	bool RotateToShapeOrSplineSegmentDir = false;
	props->GetValue("bRotateToShapeOrSplineSegmentDir", RotateToShapeOrSplineSegmentDir);

	float ShapeGenWidth = 1.0f;
	props->GetValue("fShapeGenWidth", ShapeGenWidth);
	bool RotToSurfaceNormal = true;
	props->GetValue("bRotToSurfaceNormal", RotToSurfaceNormal);

	float min_dist_gen_old = 0.0f;
	props->GetValue("fMinDistToOldGenObjs", min_dist_gen_old);

	float min_dist_gen_old_rnd = 0.0f;
	props->GetValue("fMinDistToOldGenObjsRandom", min_dist_gen_old_rnd);
	float normal_rot_mult_rnd = 0.0f;
	props->GetValue("fRotToNormalRandom", normal_rot_mult_rnd);

	float rnd_min_dist_gen_old = 0.0f;
	props->GetValue("fMinDistToOldGenObjsRandom", rnd_min_dist_gen_old);

	float normal_rot_mult_x = 1.0f;
	props->GetValue("fRotToNormalMlt", normal_rot_mult_x);
	normal_rot_mult_x = min(1.0f, normal_rot_mult_x);

	for (int i = 0; i < 2; i++)
	{
		if (obj_view_params[i] > 255)
			obj_view_params[i] = 255;
	}

	for (int i = 0; i < 5; i++)
	{
		if (!decals[i].empty())
		{
			decals_aval_num += 1;
		}
	}

	if (UseCameraDirectonInPaintMode)
	{
		CCamera& cam = GetISystem()->GetViewCamera();
		gen_direction = cam.GetViewdir();
	}

	paint_pos[1] = point;
	Matrix34 mat = GetEntity()->GetWorldTM();
	mat.SetScale(Vec3(1.0f));
	mat.SetTranslation(GetEntity()->GetWorldPos());
	std::vector<Matrix34> gen_points;
	gen_points.clear();
	Vec3 segment_dirc = (paint_pos[1] - paint_pos[0]).GetNormalizedSafe();
		if (gen_arr_dist_points_for_all)
		{
			for (int ic = 0; ic < int(gen_brushes_coof*radius); ic++)
			{
				Vec3 pnt = GetRandomPointInSphere(point, radius);
				ray_hit hit;
				int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
					rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

				bool under_terrain = false;
				if (n <= 0)
				{
					n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
						rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

					under_terrain = true;
				}

				if (!gen_brushes_floating)
				{
					if (n > 0)
					{
						if (min_dist_gen_old > 0.0f)
						{
							float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
							if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
							{
								continue;
							}
						}

						if (normal_rot_mult_x < 1.0f)
						{
							float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
							hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
						}

						IRenderNode *pNode = NULL;
						pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
						pNode->SetRndFlags(ERF_PROCEDURAL, true);
						Matrix34 mat_2 = mat;
						Vec3 pss = hit.pt;
						pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
						mat_2.SetTranslation(pss);
						if (RotateToShapeOrSplineSegmentDir)
						{
							hit.n = segment_dirc;
						}
						Vec3 nrm = hit.n;
						if (under_terrain)
							nrm = -nrm;

						if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
						{
							continue;
						}

						Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
						float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
						if (gen_brushes_rnd_rot_full)
						{
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
						}

						mat_2.Set(Vec3(1.00f), projected_orientation, pss);
						gen_points.push_back(mat_2);
					}
				}
				else
				{
					if (min_dist_gen_old > 0.0f)
					{
						float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
						if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
						{
							continue;
						}
					}

					if (normal_rot_mult_x < 1.0f)
					{
						float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
						hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
					}

					IRenderNode *pNode = NULL;
					pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
					pNode->SetRndFlags(ERF_PROCEDURAL, true);
					Matrix34 mat_2 = mat;
					Vec3 pss = pnt;
					pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
					mat_2.SetTranslation(pss);
					Vec3 nrm = Vec3(0, 0, 1);
					if (RotateToShapeOrSplineSegmentDir)
					{
						nrm = segment_dirc;
					}

					if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
					{
						continue;
					}

					Quat projected_orientation = GenerateObjOrientation(mat_2, nrm);
					float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
					projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
					if (gen_brushes_rnd_rot_full)
					{
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
						projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
					}

					mat_2.Set(Vec3(1.00f), projected_orientation, pss);
					gen_points.push_back(mat_2);
				}
			}
		}

		if (decal_gen && decals_aval_num > 0)
		{
			for (int i = 0; i < 5; i++)
			{
				if (!decals[i].empty())
				{
					if (!gen_arr_dist_points_for_all)
					{
						for (int ic = 0; ic < int(decals_gen_coof*radius); ic++)
						{
							Vec3 pnt = GetRandomPointInSphere(point, radius);
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * decals_gen_up_ht, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								under_terrain = true;
							}

							if (RotateToShapeOrSplineSegmentDir)
							{
								hit.n = segment_dirc;
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								Vec3 pss = hit.pt;
								pss.z += 0.1f;
								SDecalProperties decalProperties;
								decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
								mat.CreateIdentity();
								mat.SetTranslation(pss);
								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								const Matrix34& wtm(mat);
								Vec3 x(wtm.GetColumn0().GetNormalized());
								Vec3 y(wtm.GetColumn1().GetNormalized());
								Vec3 z(nrm.GetNormalized());

								y = z.Cross(x);
								if (y.GetLengthSquared() < 1e-4f)
									y = z.GetOrthogonal();
								y.Normalize();
								x = y.Cross(z);

								Matrix33 newOrient;
								newOrient.SetColumn(0, x);
								newOrient.SetColumn(1, y);
								newOrient.SetColumn(2, z);
								Quat q(newOrient);
								float angle(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								q = (q * Quat(Matrix33::CreateRotationZ(angle)));

								mat.Set(Vec3(1.00f), q, pss);

								decalProperties.m_pos = pss;
								decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
								Matrix33 rotation(mat);
								rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
								rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
								rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

								//rotation.SetScale(Vec3(0.05f));
								float add_rads = 0.0f;
								if (decals_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
								}

								decalProperties.m_pMaterialName = decals[i];
								decalProperties.m_radius = decals_base_scale + add_rads;
								decalProperties.m_explicitRightUpFront = rotation;
								decalProperties.m_sortPrio = cry_random(1, 150);
								decalProperties.m_deferred = true;
								decalProperties.m_depth = cry_random(0.5f, 3.0f);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3(decalProperties.m_radius);
									que_obj.object_pos = pss;
									que_obj.object_rot = q;
									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
								pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

								IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
								pRenderNode->SetDecalProperties(decalProperties);
								if (pNode)
								{
									pNode->SetMatrix(mat);
									pNode->SetViewDistRatio(obj_view_params[0]);
									gEnv->p3DEngine->RegisterEntity(pNode);
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
					}
					else
					{
						std::vector<Matrix34>::const_iterator it = gen_points.begin();
						std::vector<Matrix34>::const_iterator end = gen_points.end();
						int count = gen_points.size();
						if (count > 0)
						{
							for (int i = 0; i < count, it != end; i++, ++it)
							{
								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
								pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
									Matrix34 tm = (*it);
									if (que_obj.object_scale != Vec3(1, 1, 1))
										tm.OrthonormalizeFast();

									Ang3 angles = Ang3::GetAnglesXYZ(tm);
									Quat Rot = Quat::CreateRotationXYZ(angles);

									que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
									que_obj.object_rot = Rot;

									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_scale = Vec3((*it).GetColumn(0).GetLength());
									Matrix34 tm = (*it);
									if (que_obj.object_scale != Vec3(1, 1, 1))
										tm.OrthonormalizeFast();

									Ang3 angles = Ang3::GetAnglesXYZ(tm);
									Quat Rot = Quat::CreateRotationXYZ(angles);

									que_obj.object_pos = (*it).TransformPoint(Vec3(0, 0, 0));
									que_obj.object_rot = Rot;

									que_obj.object_slot = i;
									que_obj.object_type = 2;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = false;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									continue;
								}

								SDecalProperties decalProperties;
								decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
								decalProperties.m_pos = (*it).TransformPoint(Vec3(0, 0, 0));
								decalProperties.m_normal = (*it).TransformPoint(Vec3(0, 0, 1));
								Matrix33 rotation((*it));
								rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
								rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
								rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

								float add_rads = 0.0f;
								if (decals_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-decals_rnd_scale, decals_rnd_scale);
								}

								decalProperties.m_pMaterialName = decals[i];
								decalProperties.m_radius = decals_base_scale + add_rads;
								decalProperties.m_explicitRightUpFront = rotation;
								decalProperties.m_sortPrio = cry_random(1, 150);
								decalProperties.m_deferred = true;
								decalProperties.m_depth = cry_random(0.5f, 3.0f);

								IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
								pRenderNode->SetDecalProperties(decalProperties);
								if (pNode)
								{
									pNode->SetMatrix((*it));
									pNode->SetViewDistRatio(obj_view_params[0]);
									gEnv->p3DEngine->RegisterEntity(pNode);
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
					}
				}
			}
		}

		if (brushes_gen)
		{
			for (int i = 0; i < 5; i++)
			{
				if (!brushes[i].empty())
				{
					std::vector<Matrix34>::const_iterator it = gen_points.begin();
					std::vector<Matrix34>::const_iterator end = gen_points.end();
					int count = gen_points.size();
					for (int ic = 0; ic < int(gen_brushes_coof*radius); ic++)
					{
						if (count > 0 && ic < count)
						{
							Matrix34 idtn_mtx;
							idtn_mtx.CreateIdentity();
							if (it != end)
							{
								if (gen_arr_dist_points_for_all)
								{
									idtn_mtx = (*it);
									float add_rads = 0.0f;
									if (gen_brushes_rnd_scale > 0.0f)
									{
										add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
									}

									Vec3 nrm = Vec3(0, 0, 1);
									if (RotateToShapeOrSplineSegmentDir)
									{
										nrm = segment_dirc;
									}
									Vec3 pss = idtn_mtx.GetTranslation();
									Quat projected_orientation = GenerateObjOrientation(idtn_mtx, nrm);
									float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
									if (gen_brushes_rnd_rot_full)
									{
										projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
										projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
									}
									idtn_mtx.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);
									if (is_que_enabled && !is_streaming_sectors_enabled)
									{
										SQuedGenObject que_obj = SQuedGenObject();
										que_obj.object_pos = pss;
										que_obj.object_rot = projected_orientation;
										que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
										que_obj.object_slot = i;
										que_obj.object_type = 1;
										que_obj.vDistRate[0] = obj_view_params[1];
										que_obj.vDistRate[1] = obj_view_params[0];
										que_obj.physics_enable = brush_params[i] != 0;
										gen_objects_queue.push(que_obj);
										++it;
										continue;
									}

									if (is_streaming_sectors_enabled)
									{
										SQuedGenObject que_obj = SQuedGenObject();
										que_obj.object_pos = pss;
										que_obj.object_rot = projected_orientation;
										que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
										que_obj.object_slot = i;
										que_obj.object_type = 1;
										que_obj.vDistRate[0] = obj_view_params[1];
										que_obj.vDistRate[1] = obj_view_params[0];
										que_obj.physics_enable = brush_params[i] != 0;
										AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
										++it;
										continue;
									}
									IRenderNode *pNode = NULL;
									pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
									IStatObj *pObj = NULL;
									pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);
									if (pNode)
									{
										IBrush *pBrush = (IBrush*)pNode;
										if (pBrush && pObj)
										{
											pObj->AddRef();
											AABB box2 = pObj->GetAABB();
											box2.SetTransformedAABB(idtn_mtx, box2);
											pBrush->SetBBox(box2);
											pBrush->SetMaterial(0);
											pBrush->OffsetPosition(Vec3(ZERO));
											pBrush->SetEntityStatObj(0, pObj, 0);
											pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
											pBrush->SetLodRatio(obj_view_params[1]);
											pBrush->SetViewDistRatio(obj_view_params[0]);
											if (brush_params[i] == 1)
												pBrush->Physicalize();

											pBrush->SetBBox(box2);
											gEnv->p3DEngine->RegisterEntity(pBrush);
											pBrush->SetMatrix(idtn_mtx);
											pBrush->SetBBox(box2);
											pBrush->SetDrawLast(false);
											if (brush_params[i] == 0)
												pBrush->Dephysicalize();
										}
										succesfully_gen_oblects.push_back(pNode);
									}
								}
							}
							++it;

							if (gen_arr_dist_points_for_all)
								continue;
						}

						if (!gen_brushes_floating)
						{
							Vec3 pnt = GetRandomPointInSphere(point, radius);
							ray_hit hit;
							int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							bool under_terrain = false;
							if (n <= 0)
							{
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

								if (n > 0)
								{
									under_terrain = true;
								}
							}

							if (gen_updwn == 1 && under_terrain)
							{
								pnt.z = pnt.z + cry_random(5.0f, 8.0f);
								n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * gen_brushes_up_hgt, gen_on_stat_objs | ent_terrain,
									rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
							}

							if (RotateToShapeOrSplineSegmentDir)
							{
								hit.n = segment_dirc;
							}

							if (n > 0)
							{
								if (min_dist_gen_old > 0.0f)
								{
									float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
									if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
									{
										continue;
									}
								}

								if (normal_rot_mult_x < 1.0f)
								{
									float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
									hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
								}

								float add_rads = 0.0f;
								if (gen_brushes_rnd_scale > 0.0f)
								{
									add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
								}

								Vec3 nrm = hit.n;
								if (under_terrain)
									nrm = -nrm;

								if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
								{
									continue;
								}

								Vec3 pss = hit.pt;
								pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
								mat.CreateIdentity();
								mat.SetTranslation(pss);

								Quat projected_orientation = GenerateObjOrientation(mat, nrm);
								float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
								if (gen_brushes_rnd_rot_full)
								{
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
									projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
								}
								mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

								if (is_que_enabled && !is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									gen_objects_queue.push(que_obj);
									continue;
								}

								if (is_streaming_sectors_enabled)
								{
									SQuedGenObject que_obj = SQuedGenObject();
									que_obj.object_pos = pss;
									que_obj.object_rot = projected_orientation;
									que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
									que_obj.object_slot = i;
									que_obj.object_type = 1;
									que_obj.vDistRate[0] = obj_view_params[1];
									que_obj.vDistRate[1] = obj_view_params[0];
									que_obj.physics_enable = brush_params[i] != 0;
									AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
									++it;
									continue;
								}

								IRenderNode *pNode = NULL;
								pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
								IStatObj *pObj = NULL;
								pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

								if (pNode)
								{
									IBrush *pBrush = (IBrush*)pNode;
									if (pBrush && pObj)
									{
										pObj->AddRef();
										AABB box2 = pObj->GetAABB();
										box2.SetTransformedAABB(mat, box2);
										pBrush->SetBBox(box2);
										pBrush->SetMaterial(0);

										pBrush->OffsetPosition(Vec3(ZERO));
										pBrush->SetEntityStatObj(0, pObj, 0);
										pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
										pBrush->SetLodRatio(obj_view_params[1]);
										pBrush->SetViewDistRatio(obj_view_params[0]);
										if (brush_params[i] == 1)
											pBrush->Physicalize();

										pBrush->SetBBox(box2);
										gEnv->p3DEngine->RegisterEntity(pBrush);
										pBrush->SetMatrix(mat);
										pBrush->SetBBox(box2);
										pBrush->SetDrawLast(false);
										if (brush_params[i] == 0)
											pBrush->Dephysicalize();
									}
									succesfully_gen_oblects.push_back(pNode);
								}
							}
						}
						else
						{
							Vec3 pnt = GetRandomPointInSphere(point, radius);
							float add_rads = 0.0f;
							if (gen_brushes_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_brushes_rnd_scale, gen_brushes_rnd_scale);
							}
							Vec3 nrm = Vec3(0, 0, 1);
							if (RotateToShapeOrSplineSegmentDir)
							{
								nrm = segment_dirc;
							}

							Vec3 pss = pnt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_arc_ent_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_brushes_base_scale + add_rads), projected_orientation, pss);

							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(gen_brushes_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 1;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = brush_params[i] != 0;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}

							IRenderNode *pNode = NULL;
							pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
							IStatObj *pObj = NULL;
							pObj = gEnv->p3DEngine->LoadStatObj(brushes[i], NULL, NULL, false);

							if (pNode)
							{
								IBrush *pBrush = (IBrush*)pNode;
								if (pBrush && pObj)
								{
									pObj->AddRef();
									AABB box2 = pObj->GetAABB();
									box2.SetTransformedAABB(mat, box2);
									pBrush->SetBBox(box2);
									pBrush->SetMaterial(0);
									pBrush->OffsetPosition(Vec3(ZERO));
									pBrush->SetEntityStatObj(0, pObj, 0);
									pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
									pBrush->SetLodRatio(obj_view_params[1]);
									pBrush->SetViewDistRatio(obj_view_params[0]);
									if (brush_params[i] == 1)
										pBrush->Physicalize();

									pBrush->SetBBox(box2);
									gEnv->p3DEngine->RegisterEntity(pBrush);
									pBrush->SetMatrix(mat);
									pBrush->SetBBox(box2);
									pBrush->SetDrawLast(false);
									if (brush_params[i] == 0)
										pBrush->Dephysicalize();
								}
								succesfully_gen_oblects.push_back(pNode);
							}
						}
					}
				}
			}
		}

		if (arc_ents_gen)
		{
			for (int i = 0; i < 5; i++)
			{
				if (!arc_entites[i].empty())
				{
					for (int ic = 0; ic < int(gen_arc_ent_coof*radius); ic++)
					{
						Vec3 pnt = GetRandomPointInSphere(point, radius);
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (RotateToShapeOrSplineSegmentDir)
						{
							hit.n = segment_dirc;
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							string nmn_generated = "Arc_ent_generated_c_x_";
							nmn_generated = GenerateNodeXName(nmn_generated);

							float add_rads = 0.0f;
							if (gen_arc_ent_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-gen_arc_ent_rnd_scale, gen_arc_ent_rnd_scale);
							}
							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);
							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_brushes_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(gen_arc_ent_base_scale + add_rads), projected_orientation, pss);

							IEntity* pEntity = NULL;
							SEntitySpawnParams params;
							IEntityArchetype* pArchetype = gEnv->pEntitySystem->LoadEntityArchetype(arc_entites[i]);
							if (NULL != pArchetype)
							{
								params.nFlags = ENTITY_FLAG_SPAWNED;
								params.pArchetype = pArchetype;
								params.sName = nmn_generated;
								params.vPosition = mat.GetTranslation();
								params.vScale = Vec3(gen_arc_ent_base_scale + add_rads);


								params.qRotation = projected_orientation;

								// Create
								int nCastShadowMinSpec = 0;
								if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
								{
									objectVars->getAttr("CastShadowMinSpec", nCastShadowMinSpec);

									static ICVar* pObjShadowCastSpec = gEnv->pConsole->GetCVar("e_ObjShadowCastSpec");
									if (nCastShadowMinSpec <= pObjShadowCastSpec->GetIVal())
										params.nFlags |= ENTITY_FLAG_CASTSHADOW;

									bool bRecvWind;
									objectVars->getAttr("RecvWind", bRecvWind);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_RECVWIND;

									bool bOutdoorOnly;
									objectVars->getAttr("OutdoorOnly", bOutdoorOnly);
									if (bRecvWind)
										params.nFlags |= ENTITY_FLAG_OUTDOORONLY;

									bool bNoStaticDecals;
									objectVars->getAttr("NoStaticDecals", bNoStaticDecals);
									if (bNoStaticDecals)
										params.nFlags |= ENTITY_FLAG_NO_DECALNODE_DECALS;
								}

								pEntity = gEnv->pEntitySystem->SpawnEntity(params);
								if (pEntity)
								{
									succesfully_gen_entites.push_back(pEntity->GetId());
									IEntityRender* pRenderProx = pEntity->GetRenderInterface();
									if (pRenderProx && pRenderProx->GetRenderNode())
									{
										if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
										{
											int nViewDistRatio = 100;
											objectVars->getAttr("ViewDistRatio", nViewDistRatio);
											pRenderProx->SetViewDistRatio(nViewDistRatio);

											int nLodRatio = 100;
											objectVars->getAttr("lodRatio", nLodRatio);
											pRenderProx->SetLodRatio(nLodRatio);
										}
									}

									if (XmlNodeRef objectVars = pArchetype->GetObjectVars())
									{
										bool bHiddenInGame = false;
										objectVars->getAttr("HiddenInGame", bHiddenInGame);
										if (bHiddenInGame)
											pEntity->Hide(true);
									}
								}
							}
						}
					}
				}
			}
		}

		if (vegetation_gen)
		{
			int old_iter = 0;
			ITerrain* pterrain = gEnv->p3DEngine->GetITerrain();
			for (int i = 0; i < 5; i++)
			{
				if (!pterrain)
					break;

				if (!vegetations[i].empty())
				{
					IStatInstGroup Group;
					if (veg_inds[i] <= 0)
					{
						int ctfn = GetVegetationGrpId(vegetations[i]);
						if (ctfn >= 0)
						{
							veg_inds[i] = ctfn;
						}
					}

					if (veg_inds[i] <= -1)
						continue;

					gEnv->p3DEngine->GetStatInstGroup(veg_inds[i], Group);
					for (int ic = 0; ic < int(vegetations_gen_coof*radius); ic++)
					{
						Vec3 pnt = GetRandomPointInSphere(point, radius);
						ray_hit hit;
						int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						bool under_terrain = false;
						if (n <= 0)
						{
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(-gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

							if (n > 0)
							{
								under_terrain = true;
							}
						}

						if (gen_updwn == 1 && under_terrain)
						{
							pnt.z = pnt.z + cry_random(5.0f, 8.0f);
							n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, Vec3(gen_direction) * vegetations_gen_up_ht, gen_on_stat_objs | ent_terrain,
								rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
						}

						if (RotateToShapeOrSplineSegmentDir)
						{
							hit.n = segment_dirc;
						}

						if (n > 0)
						{
							if (min_dist_gen_old > 0.0f)
							{
								float min_distance_gen_random_xc = min_dist_gen_old + cry_random(-min_dist_gen_old_rnd, min_dist_gen_old_rnd);
								if (!IsPointNotTooCloseToOldObjs(hit.pt, min_distance_gen_random_xc))
								{
									continue;
								}
							}

							if (normal_rot_mult_x < 1.0f)
							{
								float nrm_rot_mlt_rnd_val_xc = normal_rot_mult_x + cry_random(-normal_rot_mult_rnd, normal_rot_mult_rnd);
								hit.n = Vec3::CreateLerp(Vec3(-gen_direction), hit.n, nrm_rot_mlt_rnd_val_xc);
							}

							float add_rads = 0.0f;
							if (vegetations_rnd_scale > 0.0f)
							{
								add_rads = cry_random(-vegetations_rnd_scale, vegetations_rnd_scale);
							}

							Vec3 nrm = hit.n;
							if (under_terrain)
								nrm = -nrm;

							if (!IsValidSlopeAndHeight(hit.pt, nrm, fMinSlope, fMaxSlope, fMinHeight, fMaxHeight))
							{
								continue;
							}

							Vec3 pss = hit.pt;
							pss.z += GetAdditionalObjectZCor(gen_up_from_nrm);
							mat.CreateIdentity();
							mat.SetTranslation(pss);

							Quat projected_orientation = GenerateObjOrientation(mat, nrm);
							float rand_ang(-DEG2RAD(cry_random(0.0f, z_gen_rot_rand_val)));
							projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationZ(rand_ang)));
							if (gen_vegetations_rnd_rot_full)
							{
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, 359.0f)))));
								projected_orientation = (projected_orientation * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, 359.0f)))));
							}
							mat.Set(Vec3(vegetations_base_scale + add_rads), projected_orientation, pss);
							if (is_que_enabled && !is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								gen_objects_queue.push(que_obj);
								continue;
							}

							if (is_streaming_sectors_enabled)
							{
								SQuedGenObject que_obj = SQuedGenObject();
								que_obj.object_pos = pss;
								que_obj.object_rot = projected_orientation;
								que_obj.object_scale = Vec3(vegetations_base_scale + add_rads);
								que_obj.object_slot = i;
								que_obj.object_type = 4;
								que_obj.vDistRate[0] = obj_view_params[1];
								que_obj.vDistRate[1] = obj_view_params[0];
								que_obj.physics_enable = true;
								cry_strcpy(que_obj.object_mdl, Group.szFileName);
								//que_obj.object_mdl = Group.szFileName;
								AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
								continue;
							}
							IVegetation *pBrush = nullptr;
							uint8 ang_veg[3];
							Ang3 orient_xc = Ang3(projected_orientation);
							ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
							ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
							ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
							pBrush = (IVegetation*)pterrain->AddVegetationInstance(veg_inds[i], pss, vegetations_base_scale + add_rads, 1, ang_veg[0], ang_veg[1], ang_veg[2]);

							if (pBrush)
							{
								pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
								if (!Group.bAutoMerged)
									succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
							}
						}
					}
				}
			}
		}

		if (terrain_surf_ids_gen)
		{

		}

	already_gen = true;
}

void CProceduralGenerator::DoEraseObjectsAtPont(Vec3 point, float radius)
{
	if (!gEnv->p3DEngine)
		return;
	
	paint_pos[1] = point;
	std::vector<IRenderNode *> obj_to_erase_gen_oblects;
	obj_to_erase_gen_oblects.clear();
	std::vector<EntityId> obj_to_erase_gen_oblects2;
	obj_to_erase_gen_oblects2.clear();
	std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
	uint32 count = succesfully_gen_oblects.size();
	uint32 num_objs_to_del = 0;
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if ((*it) != nullptr)
			{
				IRenderNode *pLsc = (*it);
				if (!FCVPointCLCheck(pLsc->GetPos(), point, radius*radius))
				{
					if (pLsc->GetPos().GetDistance(point) < radius)
					{
						gEnv->p3DEngine->UnRegisterEntityDirect(pLsc);
						gEnv->p3DEngine->DeleteRenderNode(pLsc);
						num_objs_to_del += 1;
						obj_to_erase_gen_oblects.push_back((*it));
					}
				}
			}
		}
		uint32 fc_size = obj_to_erase_gen_oblects.size();
		for (int ic = 0; ic < fc_size; ++ic)
		{
			IRenderNode *pLsc2 = obj_to_erase_gen_oblects[ic];
			DeleteRenderNodeFromArray(pLsc2);
		}
		obj_to_erase_gen_oblects.clear();
		obj_to_erase_gen_oblects.resize(0);
	}
	num_objs_to_del = 0;
	/*count = obj_to_erase_gen_oblects.size();
	for (int i = 0; i < count; ++i)
	{
		IRenderNode *pLsc = obj_to_erase_gen_oblects[i];
		if (pLsc)
		{
			if (pLsc->GetPos().GetDistance(point) < radius)
			{
				gEnv->p3DEngine->UnRegisterEntityDirect(pLsc);
				gEnv->p3DEngine->DeleteRenderNode(pLsc);
				obj_to_erase_gen_oblects2;
			}
		}
	}*/
	//ents
	//gEnv->pEntitySystem->RemoveEntity((*it_a), true);
	std::vector<EntityId>::const_iterator it_a = succesfully_gen_entites.begin();
	std::vector<EntityId>::const_iterator end_a = succesfully_gen_entites.end();
	int count_a = succesfully_gen_entites.size();
	if (count_a > 0)
	{
		for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
		{
			IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
			if (!pEntity)
				continue;

			if (!FCVPointCLCheck(pEntity->GetWorldPos(), point, radius*radius))
			{
				if (pEntity->GetWorldPos().GetDistance(point) < radius)
				{
					gEnv->pEntitySystem->RemoveEntity((*it_a), true);
					num_objs_to_del += 1;
					obj_to_erase_gen_oblects2.push_back((*it_a));
				}
			}
		}
		uint32 fc_size = obj_to_erase_gen_oblects2.size();
		for (int ic = 0; ic < fc_size; ++ic)
		{
			EntityId pLsc2 = obj_to_erase_gen_oblects2[ic];
			DeleteEntityNodeFromArray(pLsc2);
		}
		obj_to_erase_gen_oblects2.clear();
		obj_to_erase_gen_oblects2.resize(0);
	}
}

bool CProceduralGenerator::IsValidSlopeAndHeight(const Vec3 & point, const Vec3 & normal, const float & slopeMin, const float & slopeMax, const float & heightMin, const float & heightMax)
{
	bool is_valid = true;
	if ((point.z < heightMin) || (point.z > heightMax))
	{
		is_valid = false;
	}
	else
	{
		//Vec3 Nl_normal = Vec3(0, 0, 1);
		float fGroundAngle = RAD2DEG(acos_tpl(normal.z));
		if ((fGroundAngle < slopeMin) || (fGroundAngle > slopeMax))
		{
			is_valid = false;
		}
	}

	return is_valid;
}

void CProceduralGenerator::ReconstructSplineFromShape()
{
	IGameVolumes::VolumeInfo volumeInfo;
	if (!GetVolumeInfoForEntityGen(GetEntityId(), &volumeInfo))
		return;

	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	props->GetValue("bOnSplineGen", is_SplineGen);
	props->GetValue("bOnShapeBorderGen", is_ShapeGen);

	float spline_segments_mult = 1.0f;
	props->GetValue("fMltSplineSeg", spline_segments_mult);
	spline_segments_mult = max(0.002f, 0.1f*spline_segments_mult);
	Matrix34 ShapeMTX = GetEntity()->GetWorldTM();
	uint32 vertexCount = volumeInfo.verticesCount;
	xrf_gen_shape.clear();
	xrf_gen_shape.resize(0);
	Vec3 xcv[2];
	if (is_SplineGen)
	{
		xrf_gen_shape.clear();
		xrf_gen_shape.resize(0);
		for (uint32 i = 0; i < vertexCount; ++i)
		{
			Vec3 vfg = volumeInfo.pVertices[i];
			//if ((i == 0) || (i == vertexCount) || (i == (vertexCount - 1)))
				//xrf_gen_shape.push_back(ShapeMTX.TransformPoint(vfg));

			if (i == 0)
				xcv[0] = ShapeMTX.TransformPoint(vfg);

			if (i == vertexCount - 1)
				xcv[1] = ShapeMTX.TransformPoint(vfg);

			Vec3 vx[4];
			int create_spline = 2;
			int counterx = 0;
			for (int vccx = 0; vccx < 4; ++vccx)
			{
				vx[vccx] = Vec3(ZERO);
				int vetxcn = i + counterx;
				if (vetxcn < vertexCount)
				{
					Vec3 vfg4 = volumeInfo.pVertices[vetxcn];
					vx[vccx] = ShapeMTX.TransformPoint(vfg4);
					counterx += 1;
				}

				if (vx[vccx] == Vec3(ZERO))
				{
					create_spline = 0;
				}
			}

			if (create_spline > 0)
			{
				//start spline
				if (i == 0)
				{
					for (float fxc = 0.0f; fxc < (1.0f + spline_segments_mult); fxc += spline_segments_mult)
					{
						Vec3 ip0 = vx[0];
						if (fxc >= 0.5f)
						{
							ip0.SetQuadraticSpline(vx[0], vx[1], vx[2], fxc - 0.5f);
							xrf_gen_shape.push_back(ip0);
						}
					}
				}
				//main spline
				for (float fxc = 0.0f; fxc < (1.0f + spline_segments_mult); fxc += spline_segments_mult)
				{
					Vec3 ip0 = vx[0];
					ip0.SetQuadraticSpline(vx[0], vx[1], vx[2], fxc * 0.5f + 0.5f);
					Vec3 ip1;
					ip1.SetQuadraticSpline(vx[1], vx[2], vx[3], fxc * 0.5f);
					ip0.SetLerp(ip0, ip1, fxc);
					xrf_gen_shape.push_back(ip0);
				}
				//end spline
				if (i == (vertexCount - 4))
				{
					for (float fxc = 0.0f; fxc < (1.0f + spline_segments_mult); fxc += spline_segments_mult)
					{
						Vec3 ip0 = vx[3];
						if (fxc <= 0.5f)
						{
							ip0.SetQuadraticSpline(vx[1], vx[2], vx[3], fxc + 0.5f);
							xrf_gen_shape.push_back(ip0);
						}
					}
				}
			}
		}
	}
	else if (is_ShapeGen)
	{
		xrf_gen_shape.clear();
		xrf_gen_shape.resize(0);
		for (uint32 i = 0; i < vertexCount; ++i)
		{
			Vec3 vfg = volumeInfo.pVertices[i];
			xrf_gen_shape.push_back(ShapeMTX.TransformPoint(vfg));
		}
	}
}

void CProceduralGenerator::ClearGeneratedRenderNodes()
{
	std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
	int count = succesfully_gen_oblects.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			gEnv->p3DEngine->UnRegisterEntityDirect((*it));
			gEnv->p3DEngine->DeleteRenderNode((*it));
		}
	}
	succesfully_gen_oblects.clear();
	succesfully_gen_oblects.resize(0);
	for (int i = 0; i < 5; i++)
	{
		veg_grps[i];
	}
	//ents
	std::vector<EntityId>::const_iterator it_a = succesfully_gen_entites.begin();
	std::vector<EntityId>::const_iterator end_a = succesfully_gen_entites.end();
	int count_a = succesfully_gen_entites.size();
	if (count_a > 0)
	{
		for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
		{
			IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
			if (!pEntity)
				continue;

			gEnv->pEntitySystem->RemoveEntity((*it_a), true);
		}
	}
	succesfully_gen_entites.clear();
	succesfully_gen_entites.resize(0);

	//gen_objects_queue._Get_container.clear();
	//gen_objects_queue._Get_container.resize(0);
	while (!gen_objects_queue.empty())
	{
		gen_objects_queue.pop();
	}
}

void CProceduralGenerator::ClearGeneratedRenderNodesInSector(const int & sectorIds, const bool &delayed)
{
	if (delayed)
	{
		int sec_idc = sectorIds;
		unloading_sectors_ids.push_back(sec_idc);
		return;
	}

	SGenerationSector *pSgen_sect = GetStreamingSectorPointer(sectorIds);
	if (pSgen_sect == nullptr)
		return;

	std::vector<IRenderNode *>::const_iterator it = pSgen_sect->sector_succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = pSgen_sect->sector_succesfully_gen_oblects.end();
	int count = pSgen_sect->sector_succesfully_gen_oblects.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			gEnv->p3DEngine->UnRegisterEntityDirect((*it));
			gEnv->p3DEngine->DeleteRenderNode((*it));
		}
	}
	pSgen_sect->sector_succesfully_gen_oblects.clear();
	pSgen_sect->sector_succesfully_gen_oblects.resize(0);
	//ents
	std::vector<EntityId>::const_iterator it_a = pSgen_sect->sector_succesfully_gen_entites.begin();
	std::vector<EntityId>::const_iterator end_a = pSgen_sect->sector_succesfully_gen_entites.end();
	int count_a = pSgen_sect->sector_succesfully_gen_entites.size();
	if (count_a > 0)
	{
		for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
		{
			IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
			if (!pEntity)
				continue;

			gEnv->pEntitySystem->RemoveEntity((*it_a), true);
		}
	}
	pSgen_sect->sector_succesfully_gen_entites.clear();
	pSgen_sect->sector_succesfully_gen_entites.resize(0);
	pSgen_sect->SetGenerationComplete(false);
	pSgen_sect->SetNumberOfLoadedObjects(0);
	if (load_objects_in_streamFromStreamingGenFiles)
	{
		UnloadXMLStreamFileFromSector(pSgen_sect);
	}
}

void CProceduralGenerator::ClearGeneratedRenderNodesInAllSectors()
{
	for (int i = 0; i < generated_sectors.size(); ++i)
	{
		if (generated_sectors[i])
			ClearGeneratedRenderNodesInSector(generated_sectors[i]->GetSectorId());

		SAFE_DELETE(generated_sectors[i]);
	}
	generated_sectors.clear();
	generated_sectors.resize(0);
}

bool CProceduralGenerator::IsGeneratedRenderNodeAreadyInArray(IRenderNode *pNode)
{
	std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
	int count = succesfully_gen_oblects.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if (pNode == (*it))
			{
				CryLogAlways("CProceduralGenerator::IsGeneratedRenderNodeAreadyInArray return true");
				return true;
			}
		}
	}
	return false;
}

Quat CProceduralGenerator::GenerateObjOrientation(const Matrix34 & orig_mtx, const Vec3 & normal)
{
	Vec3 x(orig_mtx.GetColumn0().GetNormalized());
	Vec3 y(orig_mtx.GetColumn1().GetNormalized());
	Vec3 z(normal.GetNormalized());

	y = z.Cross(x);
	if (y.GetLengthSquared() < 1e-4f)
		y = z.GetOrthogonal();
	y.Normalize();
	x = y.Cross(z);

	Matrix33 newOrient;
	newOrient.SetColumn(0, x);
	newOrient.SetColumn(1, y);
	newOrient.SetColumn(2, z);
	Quat q(newOrient);
	return q;
}

bool CProceduralGenerator::IsPointNotTooCloseToOldObjs(const Vec3 &point, const float &dist)
{
	bool is_close = false;
	std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
	float x_cnr_val = point.x + dist;
	float y_cnr_val = point.y + dist;
	float x_dnr_val = point.x - dist;
	float y_dnr_val = point.y - dist;
	int count = succesfully_gen_oblects.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			//if((*it) == nullptr)
			//	continue;

			Vec3 Pos = (*it)->GetPos();
			if(Pos.x > x_cnr_val || Pos.y > y_cnr_val || Pos.x < x_dnr_val || Pos.y < y_dnr_val)
				continue;

			bool iscn = FCVPointCLCheck(Pos, point, dist);
			if (iscn)
			{
				continue;
			}
			else
			{
				is_close = true;
				break;
			}
		}
	}
	//ents
	std::vector<EntityId>::const_iterator it_a = succesfully_gen_entites.begin();
	std::vector<EntityId>::const_iterator end_a = succesfully_gen_entites.end();
	int count_a = succesfully_gen_entites.size();
	if ((count_a > 0) && !is_close)
	{
		for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
		{
			IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
			if (!pEntity)
				continue;

			Vec3 Pos = pEntity->GetWorldPos();
			bool iscn = FCVPointCLCheck(Pos, point, dist);
			if (iscn)
			{
				continue;
			}
			else
			{
				is_close = true;
				break;
			}
		}
	}
	return !is_close;
}

bool CProceduralGenerator::FCVPointCLCheck(const Vec3 & point, const Vec3 & point2, const float & dist)
{
	Vec3 cs = point - point2;
	bool is_cl = false;
	if (cs.GetLengthSquared() > dist)
		is_cl = true;

	//CryLogAlways("CProceduralGenerator::FCVPointCLCheck called");
	return is_cl;
}

bool CProceduralGenerator::DeleteRenderNodeFromArray(IRenderNode * pNode)
{
	std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
	std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
	uint32 count = succesfully_gen_oblects.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			IRenderNode *pLsc1 = (*it);
			if (pLsc1 == pNode)
			{
				succesfully_gen_oblects.erase(it);
				succesfully_gen_oblects.resize(count - 1);
				return true;
			}
		}
	}
	return false;
}

bool CProceduralGenerator::DeleteEntityNodeFromArray(EntityId ids)
{
	std::vector<EntityId>::const_iterator it = succesfully_gen_entites.begin();
	std::vector<EntityId>::const_iterator end = succesfully_gen_entites.end();
	uint32 count = succesfully_gen_entites.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			EntityId pLsc1 = (*it);
			if (pLsc1 == ids)
			{
				succesfully_gen_entites.erase(it);
				succesfully_gen_entites.resize(count - 1);
				return true;
			}
		}
	}
	return false;
}

void CProceduralGenerator::GenerateObjectsFromXMLFile(string path)
{
	XmlNodeRef Objects_node = GetISystem()->LoadXmlFromFile(path);
	if (!Objects_node)
	{
		if (!load_objects_in_streamFromStreamingGenFiles)
		{
			CryLogAlways("CProceduralGenerator::GenerateObjectsFromXMLFile called, loading objects XMLFile failed");
		}
		return;
	}

	if (!gEnv->p3DEngine)
		return;

	ITerrain* pTerrain = gEnv->p3DEngine->GetITerrain();
	if (!pTerrain)
		return;

	string obJtype = "";
	Vec3 pos = Vec3(ZERO);
	Quat rotation = Quat::CreateIdentity();
	Vec3 scale = Vec3(1);
	string model_pth = "";
	int lod_dst = 0, view_dst = 0;
	uint32 sector_idsc = 0;
	string Material = "";
	for (int i = 0; i < Objects_node->getChildCount(); ++i)
	{
		XmlNodeRef this_object = Objects_node->getChild(i);
		if (!this_object)
			continue;

		obJtype = this_object->getAttr("Type");
		Material = this_object->getAttr("Material");
		this_object->getAttr("Pos", pos);
		this_object->getAttr("Rotate", rotation);
		this_object->getAttr("Scale", scale);
		model_pth = this_object->getAttr("Prefab");
		this_object->getAttr("LodRatio", lod_dst);
		this_object->getAttr("ViewDistRatio", view_dst);
		this_object->getAttr("SectorIds", sector_idsc);
		if (obJtype == string("Brush"))
		{
			if (is_que_enabled && !is_streaming_sectors_enabled)
			{
				SQuedGenObject que_obj = SQuedGenObject();
				que_obj.object_pos = pos;
				que_obj.object_rot = rotation;
				que_obj.object_scale = scale;
				que_obj.object_slot = 0;
				que_obj.object_type = 1;
				que_obj.physics_enable = true;
				cry_strcpy(que_obj.object_mdl, model_pth);
				//que_obj.object_mdl = model_pth;
				que_obj.vDistRate[0] = lod_dst;
				que_obj.vDistRate[1] = view_dst;
				que_obj.stream_sector_id = sector_idsc;
				gen_objects_queue.push(que_obj);
				continue;
			}

			if (is_streaming_sectors_enabled)
			{
				SQuedGenObject que_obj = SQuedGenObject();
				que_obj.object_pos = pos;
				que_obj.object_rot = rotation;
				que_obj.object_scale = scale;
				que_obj.object_slot = 0;
				que_obj.object_type = 1;
				que_obj.vDistRate[0] = lod_dst;
				que_obj.vDistRate[1] = view_dst;
				que_obj.physics_enable = true;
				cry_strcpy(que_obj.object_mdl, model_pth);
				//que_obj.object_mdl = model_pth;
				que_obj.stream_sector_id = sector_idsc;
				if(sector_idsc > 0)
					AddObjectToSectorQue(que_obj, sector_idsc);
				else
					AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));

				continue;
			}
			IRenderNode *pNode = NULL;
			pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
			IStatObj *pObj = NULL;
			pObj = gEnv->p3DEngine->LoadStatObj(model_pth, NULL, NULL, false);
			if (pNode)
			{
				IBrush *pBrush = (IBrush*)pNode;
				if (pBrush && pObj)
				{
					Matrix34 mat = Matrix34::CreateIdentity();
					mat.SetTranslation(pos);
					mat.Set(scale, rotation, pos);
					pObj->AddRef();
					AABB box2 = pObj->GetAABB();
					box2.SetTransformedAABB(mat, box2);
					pBrush->SetBBox(box2);
					pBrush->SetMaterial(0);
					pBrush->OffsetPosition(Vec3(ZERO));
					pBrush->SetEntityStatObj(0, pObj, 0);
					pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
					pBrush->SetLodRatio(lod_dst);
					pBrush->SetViewDistRatio(view_dst);
					pBrush->Physicalize();

					pBrush->SetBBox(box2);
					gEnv->p3DEngine->RegisterEntity(pBrush);
					pBrush->SetMatrix(mat);
					pBrush->SetBBox(box2);
					pBrush->SetDrawLast(false);
				}
				succesfully_gen_oblects.push_back(pNode);
			}
		}
		else if (obJtype == string("Decal"))
		{
			if (is_que_enabled && !is_streaming_sectors_enabled)
			{
				SQuedGenObject que_obj = SQuedGenObject();
				que_obj.object_pos = pos;
				que_obj.object_rot = rotation;
				que_obj.object_scale = scale;
				que_obj.object_slot = 0;
				que_obj.object_type = 2;
				que_obj.vDistRate[0] = lod_dst;
				que_obj.vDistRate[1] = view_dst;
				que_obj.physics_enable = false;
				cry_strcpy(que_obj.object_mdl, Material);
				//que_obj.object_mdl = Material;
				que_obj.stream_sector_id = sector_idsc;
				gen_objects_queue.push(que_obj);
				continue;
			}

			if (is_streaming_sectors_enabled)
			{
				SQuedGenObject que_obj = SQuedGenObject();
				que_obj.object_pos = pos;
				que_obj.object_rot = rotation;
				que_obj.object_scale = scale;
				que_obj.object_slot = 0;
				que_obj.object_type = 2;
				que_obj.vDistRate[0] = lod_dst;
				que_obj.vDistRate[1] = view_dst;
				que_obj.physics_enable = false;
				cry_strcpy(que_obj.object_mdl, Material);
				//que_obj.object_mdl = Material;
				que_obj.stream_sector_id = sector_idsc;
				if (sector_idsc > 0)
					AddObjectToSectorQue(que_obj, sector_idsc);
				else
					AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));

				continue;
			}
			IRenderNode *pNode = NULL;
			pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
			pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);
			Matrix34 mat = Matrix34::CreateIdentity();
			mat.SetTranslation(pos);
			mat.Set(scale, rotation, pos);
			SDecalProperties decalProperties;
			decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
			decalProperties.m_pos = pos;
			decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
			Matrix33 rotation(mat);
			rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
			rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
			rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

			decalProperties.m_pMaterialName = Material;
			decalProperties.m_radius = scale.x;
			decalProperties.m_explicitRightUpFront = rotation;
			decalProperties.m_sortPrio = cry_random(1, 150);
			decalProperties.m_deferred = true;
			decalProperties.m_depth = cry_random(0.5f, 3.0f);

			IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
			if(pRenderNode)
				pRenderNode->SetDecalProperties(decalProperties);

			if (pNode)
			{
				pNode->SetMatrix(mat);
				pNode->SetViewDistRatio(view_dst);
				gEnv->p3DEngine->RegisterEntity(pNode);
				succesfully_gen_oblects.push_back(pNode);
			}
		}
		else if (obJtype == string("EntityArc"))
		{

		}
		else if (obJtype == string("Vegetation"))
		{
			if (is_que_enabled && !is_streaming_sectors_enabled)
			{
				SQuedGenObject que_obj = SQuedGenObject();
				que_obj.object_pos = pos;
				que_obj.object_rot = rotation;
				que_obj.object_scale = scale;
				que_obj.object_slot = 0;
				que_obj.object_type = 4;
				que_obj.vDistRate[0] = lod_dst;
				que_obj.vDistRate[1] = view_dst;
				que_obj.physics_enable = true;
				cry_strcpy(que_obj.object_mdl, model_pth);
				//que_obj.object_mdl = model_pth;
				que_obj.stream_sector_id = sector_idsc;
				gen_objects_queue.push(que_obj);
				continue;
			}

			if (is_streaming_sectors_enabled)
			{
				SQuedGenObject que_obj = SQuedGenObject();
				que_obj.object_pos = pos;
				que_obj.object_rot = rotation;
				que_obj.object_scale = scale;
				que_obj.object_slot = 0;
				que_obj.object_type = 4;
				que_obj.vDistRate[0] = lod_dst;
				que_obj.vDistRate[1] = view_dst;
				que_obj.physics_enable = true;
				cry_strcpy(que_obj.object_mdl, model_pth);
				//que_obj.object_mdl = model_pth;
				que_obj.stream_sector_id = sector_idsc;
				if (sector_idsc > 0)
					AddObjectToSectorQue(que_obj, sector_idsc);
				else
					AddObjectToSectorQue(que_obj, GetNearestStreamingSector(Vec2(que_obj.object_pos), sector_size));
				continue;
			}
			int veg_ids = GetVegetationGrpId(model_pth);
			if (veg_ids < 0)
				continue;

			IVegetation *pBrush = nullptr;
			uint8 ang_veg[3];
			Ang3 orient_xc = Ang3(rotation);
			ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
			ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
			ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
			pBrush = (IVegetation*)pTerrain->AddVegetationInstance(veg_ids, pos, scale.x, 1, ang_veg[0], ang_veg[1], ang_veg[2]);
			if (pBrush)
			{
				pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
				IStatInstGroup Group;
				if (gEnv->p3DEngine->GetStatInstGroup(veg_ids, Group))
				{
					if (!Group.bAutoMerged)
						succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
				}
			}
		}
		else if (obJtype == string("TerrainTxt"))
		{

		}
	}
}

void CProceduralGenerator::LoadXMLStreamFileInSector(SGenerationSector * pSgen_sect)
{
	if (pSgen_sect == nullptr)
		return;

	if(pSgen_sect->loading_xml_complete)
		return;

	UnloadXMLStreamFileFromSector(pSgen_sect);
	GenerateObjectsFromXMLFile(GetStreamSectorFilepath(pSgen_sect->GetSectorId()));
	pSgen_sect->loading_xml_complete = true;
}

void CProceduralGenerator::UnloadXMLStreamFileFromSector(SGenerationSector * pSgen_sect)
{
	if (pSgen_sect == nullptr)
		return;

	pSgen_sect->sector_gen_oblects.clear();
	pSgen_sect->sector_gen_oblects.resize(0);
	pSgen_sect->loading_xml_complete = false;
}

void CProceduralGenerator::GreateObjectFromQue(const SQuedGenObject & object_q, int sectorIds)
{
	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	if (!gEnv->pPhysicalWorld)
		return;

	ITerrain* pTerrain = gEnv->p3DEngine->GetITerrain();
	if (!pTerrain)
		return;

	if(object_q.object_slot > 4 || object_q.object_slot < 0)
		return;

	if (!generation_setting_data_refilled)
		RefillGenerationSettingData();

	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = setting_data.decals[i];
		brushes[i] = setting_data.brushes[i];
		arc_entites[i] = setting_data.arc_entites[i];
		vegetations[i] = setting_data.vegetations[i];
		surf_ids[i] = setting_data.surf_ids[i];
		veg_inds[i] = setting_data.veg_inds[i];
		brush_params[i] = setting_data.brush_params[i];
	}

	if (!props)
		return;

	bool EnableStreaming = false;
	props->GetValue("bEnableObjectsStreaming", EnableStreaming);
	if (EnableStreaming)
	{
		//int sector_size = 128;
		props->GetValue("fStreamingSectorSize", sector_size);
		AddObjectToSectorQue(object_q, GetNearestStreamingSector(Vec2(object_q.object_pos), sector_size));
		return;
	}

	//if (!object_q.object_mdl.empty())
	if(!(!strcmp(object_q.object_mdl, "")))
	{
		for (int i = 0; i < 5; i++)
		{
			if (i != object_q.object_slot)
				continue;

			if (object_q.object_type == 1)
			{
				brushes[i] = object_q.object_mdl;
			}
			else if (object_q.object_type == 2)
			{
				decals[i] = object_q.object_mdl;
			}
			else if (object_q.object_type == 3)
			{
				arc_entites[i] = object_q.object_mdl;
			}
			else if (object_q.object_type == 4)
			{
				vegetations[i] = object_q.object_mdl;
				veg_inds[i] = atoi(object_q.object_mdl);
			}
		}
	}

	if (object_q.object_type == 1)
	{
		IRenderNode *pNode = NULL;
		pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
		IStatObj *pObj = NULL;
		pObj = gEnv->p3DEngine->LoadStatObj(brushes[object_q.object_slot], NULL, NULL, false);
		if (pNode)
		{
			IBrush *pBrush = (IBrush*)pNode;
			if (pBrush && pObj)
			{
				Matrix34 mat = Matrix34::CreateIdentity();
				mat.SetTranslation(object_q.object_pos);
				mat.Set(object_q.object_scale, object_q.object_rot, object_q.object_pos);
				pObj->AddRef();
				AABB box2 = pObj->GetAABB();
				box2.SetTransformedAABB(mat, box2);
				pBrush->SetBBox(box2);
				pBrush->SetMaterial(0);
				pBrush->OffsetPosition(Vec3(ZERO));
				pBrush->SetEntityStatObj(0, pObj, 0);
				pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
				pBrush->SetLodRatio(setting_data.obj_view_params[1]);
				pBrush->SetViewDistRatio(setting_data.obj_view_params[0]);
				if(object_q.object_mdl || object_q.physics_enable)
					pBrush->Physicalize();

				pBrush->SetBBox(box2);
				gEnv->p3DEngine->RegisterEntity(pBrush);
				pBrush->SetMatrix(mat);
				pBrush->SetBBox(box2);
				pBrush->SetDrawLast(false);
				if (!object_q.object_mdl || !object_q.physics_enable)
					pBrush->Dephysicalize();

				//CryLogAlways("brushes[object_q.object_slot] == %s", brushes[object_q.object_slot]);
				//CryLogAlways("mat.pos == x- %f, y- %f,z- %f", mat.GetTranslation().x, mat.GetTranslation().y, mat.GetTranslation().z);
			}
			succesfully_gen_oblects.push_back(pNode);
		}
	}
	else if (object_q.object_type == 2)
	{
		IRenderNode *pNode = NULL;
		pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
		pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);
		Matrix34 mat = Matrix34::CreateIdentity();
		mat.SetTranslation(object_q.object_pos);
		mat.Set(object_q.object_scale, object_q.object_rot, object_q.object_pos);
		SDecalProperties decalProperties;
		decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
		decalProperties.m_pos = object_q.object_pos;
		decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
		Matrix33 rotation(mat);
		rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
		rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
		rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

		decalProperties.m_pMaterialName = decals[object_q.object_slot];
		decalProperties.m_radius = object_q.object_scale.x;
		decalProperties.m_explicitRightUpFront = rotation;
		decalProperties.m_sortPrio = cry_random(1, 150);
		decalProperties.m_deferred = true;
		decalProperties.m_depth = cry_random(0.5f, 3.0f);

		IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
		if (pRenderNode)
			pRenderNode->SetDecalProperties(decalProperties);

		if (pNode)
		{
			pNode->SetMatrix(mat);
			pNode->SetViewDistRatio(setting_data.obj_view_params[0]);
			gEnv->p3DEngine->RegisterEntity(pNode);
			succesfully_gen_oblects.push_back(pNode);
		}
	}
	else if (object_q.object_type == 3)
	{

	}
	else if (object_q.object_type == 4)
	{
		int veg_ids = veg_inds[object_q.object_slot];
		if (veg_inds[object_q.object_slot] <= 0)
		{
			if (vegetations[object_q.object_slot] != string("auto"))
			{
				int ctfn = GetVegetationGrpId(vegetations[object_q.object_slot]);
				if (ctfn >= 0)
				{
					veg_ids = ctfn;
				}
			}
		}

		if (veg_ids <= -1)
			return;

		IVegetation *pBrush = nullptr;
		uint8 ang_veg[3];
		Ang3 orient_xc = Ang3(object_q.object_rot);
		ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
		ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
		ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
		pBrush = (IVegetation*)pTerrain->AddVegetationInstance(veg_ids, object_q.object_pos, object_q.object_scale.x, 1, ang_veg[0], ang_veg[1], ang_veg[2]);
		if (pBrush)
		{
			pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
			IStatInstGroup Group;
			if (gEnv->p3DEngine->GetStatInstGroup(veg_ids, Group))
			{
				if (!Group.bAutoMerged)
					succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
			}
		}
	}
}

void CProceduralGenerator::AddObjectToSectorQue(const SQuedGenObject & object_q, const int & sectorIds)
{
	SGenerationSector *pSgen_sect = GetStreamingSectorPointer(sectorIds);
	if (pSgen_sect == nullptr)
		return;

	SQuedGenObject nt_gen_obj = SQuedGenObject();
	cry_strcpy(nt_gen_obj.object_mdl, object_q.object_mdl);
	//nt_gen_obj.object_mdl = object_q.object_mdl;
	nt_gen_obj.object_pos = object_q.object_pos;
	nt_gen_obj.object_rot = object_q.object_rot;
	nt_gen_obj.object_scale = object_q.object_scale;
	nt_gen_obj.object_slot = object_q.object_slot;
	nt_gen_obj.object_type = object_q.object_type;
	nt_gen_obj.physics_enable = object_q.physics_enable;
	nt_gen_obj.stream_sector_id = sectorIds;
	//SQuedGenObject nt_gen_obj = object_q;
	pSgen_sect->sector_gen_oblects.push_back(nt_gen_obj);
}

void CProceduralGenerator::GenerateObjectsInSector(const int & sectorIds)
{
	SGenerationSector *pSgen_sect = GetStreamingSectorPointer(sectorIds);
	if (pSgen_sect == nullptr)
		return;

	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	if (!gEnv->pPhysicalWorld)
		return;

	ITerrain* pTerrain = gEnv->p3DEngine->GetITerrain();
	if (!pTerrain)
		return;

	/*SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	if (!props)
		return;*/

	if (load_objects_in_streamFromStreamingGenFiles)
	{
		LoadXMLStreamFileInSector(pSgen_sect);
	}

	if (pSgen_sect->IsGenerationComplete())
		return;

	if (is_que_enabled)
	{
		return;
	}

	if (!generation_setting_data_refilled)
		RefillGenerationSettingData();

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = setting_data.decals[i];
		brushes[i] = setting_data.brushes[i];
		arc_entites[i] = setting_data.arc_entites[i];
		vegetations[i] = setting_data.vegetations[i];
		surf_ids[i] = setting_data.surf_ids[i];
		veg_inds[i] = setting_data.veg_inds[i];
		brush_params[i] = setting_data.brush_params[i];
	}

	//if (!props)
	//	return;

	std::vector<SQuedGenObject>::const_iterator it_x = pSgen_sect->sector_gen_oblects.begin();
	std::vector<SQuedGenObject>::const_iterator end_x = pSgen_sect->sector_gen_oblects.end();
	int count_x = pSgen_sect->sector_gen_oblects.size();
	if (count_x > 0)
	{
		for (int ir = 0; ir < count_x, it_x != end_x; ir++, ++it_x)
		{
			const SQuedGenObject & object_q = (*it_x);
			//CryLogAlways("CProceduralGenerator::GenerateObjectsInSector called, object_to_generation_type == %i, slot == %i", object_q.object_type, object_q.object_slot);
			if (object_q.object_slot > 4 || object_q.object_slot < 0)
				continue;

			//if (!object_q.object_mdl.empty())
			if (!(!strcmp(object_q.object_mdl, "")))
			{
				for (int i = 0; i < 5; i++)
				{
					if (i != object_q.object_slot)
						continue;

					if (object_q.object_type == 1)
					{
						brushes[i] = object_q.object_mdl;
					}
					else if (object_q.object_type == 2)
					{
						decals[i] = object_q.object_mdl;
					}
					else if (object_q.object_type == 3)
					{
						arc_entites[i] = object_q.object_mdl;
					}
					else if (object_q.object_type == 4)
					{
						vegetations[i] = object_q.object_mdl;
						veg_inds[i] = atoi(object_q.object_mdl);
					}
				}
			}

			if (object_q.object_type == 1)
			{
				IRenderNode *pNode = NULL;
				pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
				IStatObj *pObj = NULL;
				pObj = gEnv->p3DEngine->LoadStatObj(brushes[object_q.object_slot], NULL, NULL, false);
				if (pNode)
				{
					IBrush *pBrush = (IBrush*)pNode;
					if (pBrush && pObj)
					{
						Matrix34 mat = Matrix34::CreateIdentity();
						mat.SetTranslation(object_q.object_pos);
						mat.Set(object_q.object_scale, object_q.object_rot, object_q.object_pos);
						pObj->AddRef();
						AABB box2 = pObj->GetAABB();
						box2.SetTransformedAABB(mat, box2);
						pBrush->SetBBox(box2);
						pBrush->SetMaterial(0);
						pBrush->OffsetPosition(Vec3(ZERO));
						pBrush->SetEntityStatObj(0, pObj, 0);
						pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
						pBrush->SetLodRatio(setting_data.obj_view_params[1]);
						pBrush->SetViewDistRatio(setting_data.obj_view_params[0]);
						if (object_q.object_mdl || object_q.physics_enable)
							pBrush->Physicalize();

						pBrush->SetBBox(box2);
						gEnv->p3DEngine->RegisterEntity(pBrush);
						pBrush->SetMatrix(mat);
						pBrush->SetBBox(box2);
						pBrush->SetDrawLast(false);
						if (!object_q.object_mdl || !object_q.physics_enable)
							pBrush->Dephysicalize();

					}
					pSgen_sect->sector_succesfully_gen_oblects.push_back(pNode);
					//CryLogAlways("CProceduralGenerator::GenerateObjectsInSector called, Brush created, pos = x - %f, y - %f, z - %f,",
					//	object_q.object_pos.x, object_q.object_pos.y, object_q.object_pos.z);
				}
			}
			else if (object_q.object_type == 2)
			{
				IRenderNode *pNode = NULL;
				pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
				pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);
				Matrix34 mat = Matrix34::CreateIdentity();
				mat.SetTranslation(object_q.object_pos);
				mat.Set(object_q.object_scale, object_q.object_rot, object_q.object_pos);
				SDecalProperties decalProperties;
				decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
				decalProperties.m_pos = object_q.object_pos;
				decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
				Matrix33 rotation(mat);
				rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
				rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
				rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

				decalProperties.m_pMaterialName = decals[object_q.object_slot];
				decalProperties.m_radius = object_q.object_scale.x;
				decalProperties.m_explicitRightUpFront = rotation;
				decalProperties.m_sortPrio = cry_random(1, 150);
				decalProperties.m_deferred = true;
				decalProperties.m_depth = cry_random(0.5f, 3.0f);

				IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
				if (pRenderNode)
					pRenderNode->SetDecalProperties(decalProperties);

				if (pNode)
				{
					pNode->SetMatrix(mat);
					pNode->SetViewDistRatio(setting_data.obj_view_params[0]);
					gEnv->p3DEngine->RegisterEntity(pNode);
					pSgen_sect->sector_succesfully_gen_oblects.push_back(pNode);
				}
			}
			else if (object_q.object_type == 3)
			{

			}
			else if (object_q.object_type == 4)
			{
				int veg_ids = veg_inds[object_q.object_slot];
				if (veg_inds[object_q.object_slot] <= 0)
				{
					if (vegetations[object_q.object_slot] != string("auto"))
					{
						int ctfn = GetVegetationGrpId(vegetations[object_q.object_slot]);
						if (ctfn >= 0)
						{
							veg_ids = ctfn;
						}
					}
				}

				if (veg_ids <= -1)
					continue;

				IVegetation *pBrush = nullptr;
				uint8 ang_veg[3];
				Ang3 orient_xc = Ang3(object_q.object_rot);
				ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
				ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
				ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
				pBrush = (IVegetation*)pTerrain->AddVegetationInstance(veg_ids, object_q.object_pos, object_q.object_scale.x, 1, ang_veg[0], ang_veg[1], ang_veg[2]);
				if (pBrush)
				{
					pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
					IStatInstGroup Group;
					if (gEnv->p3DEngine->GetStatInstGroup(veg_ids, Group))
					{
						if (!Group.bAutoMerged)
							pSgen_sect->sector_succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
					}
				}
			}
		}
		pSgen_sect->SetNumberOfLoadedObjects(pSgen_sect->sector_succesfully_gen_oblects.size() + pSgen_sect->sector_succesfully_gen_entites.size());
		if (count_x == pSgen_sect->GetNumberOfLoadedObjects())
		{

		}
		pSgen_sect->SetGenerationComplete(true);
	}
	//CryLogAlways("CProceduralGenerator::GenerateObjectsInSector called, sector ids == %i", sectorIds);
}

void CProceduralGenerator::GenerateObjectInSector(SGenerationSector *pSgen_sect, const uint32 & objectIds)
{
	if (pSgen_sect == nullptr)
		return;

	if (!GetEntity())
		return;

	if (!gEnv->p3DEngine)
		return;

	if (!gEnv->pPhysicalWorld)
		return;

	ITerrain* pTerrain = gEnv->p3DEngine->GetITerrain();
	if (!pTerrain)
		return;

	if (pSgen_sect->IsGenerationComplete())
		return;

	if (!generation_setting_data_refilled)
		RefillGenerationSettingData();

	string decals[5];
	string brushes[5];
	string arc_entites[5];
	string vegetations[5];
	int surf_ids[5];
	int veg_inds[5];
	int brush_params[5];
	for (int i = 0; i < 5; i++)
	{
		decals[i] = setting_data.decals[i];
		brushes[i] = setting_data.brushes[i];
		arc_entites[i] = setting_data.arc_entites[i];
		vegetations[i] = setting_data.vegetations[i];
		surf_ids[i] = setting_data.surf_ids[i];
		veg_inds[i] = setting_data.veg_inds[i];
		brush_params[i] = setting_data.brush_params[i];
	}

	uint32 count_x = pSgen_sect->sector_gen_oblects.size();
	if (count_x > 0)
	{
		for (int ir = 0; ir < count_x; ++ir)
		{
		}
	}

	if(count_x <= objectIds)
		return;

	const SQuedGenObject & object_q = pSgen_sect->sector_gen_oblects[objectIds];
	//CryLogAlways("CProceduralGenerator::GenerateObjectsInSector called, object_to_generation_type == %i, slot == %i", object_q.object_type, object_q.object_slot);
	if (object_q.object_slot > 4 || object_q.object_slot < 0)
		return;

	//if (!object_q.object_mdl.empty())
	if (!(!strcmp(object_q.object_mdl, "")))
	{
		for (int i = 0; i < 5; i++)
		{
			if (i != object_q.object_slot)
				continue;

			if (object_q.object_type == 1)
			{
				brushes[i] = object_q.object_mdl;
			}
			else if (object_q.object_type == 2)
			{
				decals[i] = object_q.object_mdl;
			}
			else if (object_q.object_type == 3)
			{
				arc_entites[i] = object_q.object_mdl;
			}
			else if (object_q.object_type == 4)
			{
				vegetations[i] = object_q.object_mdl;
				veg_inds[i] = atoi(object_q.object_mdl);
			}
		}
	}

	if (object_q.object_type == 1)
	{
		IRenderNode *pNode = NULL;
		pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
		IStatObj *pObj = NULL;
		pObj = gEnv->p3DEngine->LoadStatObj(brushes[object_q.object_slot], NULL, NULL, false);
		if (pNode)
		{
			IBrush *pBrush = (IBrush*)pNode;
			if (pBrush && pObj)
			{
				Matrix34 mat = Matrix34::CreateIdentity();
				mat.SetTranslation(object_q.object_pos);
				mat.Set(object_q.object_scale, object_q.object_rot, object_q.object_pos);
				pObj->AddRef();
				AABB box2 = pObj->GetAABB();
				box2.SetTransformedAABB(mat, box2);
				pBrush->SetBBox(box2);
				pBrush->SetMaterial(0);
				pBrush->OffsetPosition(Vec3(ZERO));
				pBrush->SetEntityStatObj(0, pObj, 0);
				pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
				pBrush->SetLodRatio(setting_data.obj_view_params[1]);
				pBrush->SetViewDistRatio(setting_data.obj_view_params[0]);
				if (object_q.object_mdl || object_q.physics_enable)
					pBrush->Physicalize();

				pBrush->SetBBox(box2);
				gEnv->p3DEngine->RegisterEntity(pBrush);
				pBrush->SetMatrix(mat);
				pBrush->SetBBox(box2);
				pBrush->SetDrawLast(false);
				if (!object_q.object_mdl || !object_q.physics_enable)
					pBrush->Dephysicalize();

			}
			pSgen_sect->sector_succesfully_gen_oblects.push_back(pNode);
			//CryLogAlways("CProceduralGenerator::GenerateObjectsInSector called, Brush created, pos = x - %f, y - %f, z - %f,",
			//	object_q.object_pos.x, object_q.object_pos.y, object_q.object_pos.z);
		}
	}
	else if (object_q.object_type == 2)
	{
		IRenderNode *pNode = NULL;
		pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
		pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);
		Matrix34 mat = Matrix34::CreateIdentity();
		mat.SetTranslation(object_q.object_pos);
		mat.Set(object_q.object_scale, object_q.object_rot, object_q.object_pos);
		SDecalProperties decalProperties;
		decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
		decalProperties.m_pos = object_q.object_pos;
		decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
		Matrix33 rotation(mat);
		rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
		rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
		rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

		decalProperties.m_pMaterialName = decals[object_q.object_slot];
		decalProperties.m_radius = object_q.object_scale.x;
		decalProperties.m_explicitRightUpFront = rotation;
		decalProperties.m_sortPrio = cry_random(1, 150);
		decalProperties.m_deferred = true;
		decalProperties.m_depth = cry_random(0.5f, 3.0f);

		IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
		if (pRenderNode)
			pRenderNode->SetDecalProperties(decalProperties);

		if (pNode)
		{
			pNode->SetMatrix(mat);
			pNode->SetViewDistRatio(setting_data.obj_view_params[0]);
			gEnv->p3DEngine->RegisterEntity(pNode);
			pSgen_sect->sector_succesfully_gen_oblects.push_back(pNode);
		}
	}
	else if (object_q.object_type == 3)
	{

	}
	else if (object_q.object_type == 4)
	{
		int veg_ids = veg_inds[object_q.object_slot];
		if (veg_inds[object_q.object_slot] <= 0)
		{
			if (vegetations[object_q.object_slot] != string("auto"))
			{
				int ctfn = GetVegetationGrpId(vegetations[object_q.object_slot]);
				if (ctfn >= 0)
				{
					veg_ids = ctfn;
				}
			}
		}

		if (veg_ids <= -1)
			return;

		IVegetation *pBrush = nullptr;
		uint8 ang_veg[3];
		Ang3 orient_xc = Ang3(object_q.object_rot);
		ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
		ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
		ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
		pBrush = (IVegetation*)pTerrain->AddVegetationInstance(veg_ids, object_q.object_pos, object_q.object_scale.x, 1, ang_veg[0], ang_veg[1], ang_veg[2]);
		if (pBrush)
		{
			pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
			IStatInstGroup Group;
			if (gEnv->p3DEngine->GetStatInstGroup(veg_ids, Group))
			{
				if (!Group.bAutoMerged)
					pSgen_sect->sector_succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
			}
		}
	}
	pSgen_sect->SetNumberOfLoadedObjects(pSgen_sect->sector_succesfully_gen_oblects.size() + pSgen_sect->sector_succesfully_gen_entites.size());
	if (count_x == pSgen_sect->GetNumberOfLoadedObjects())
	{
		pSgen_sect->SetGenerationComplete(true);
	}
}

void CProceduralGenerator::DeleteObjectFromSector(SGenerationSector * pSgen_sect, const uint32 & objectIds)
{
	if (pSgen_sect == nullptr)
		return;

	if(!gEnv->p3DEngine)
		return;

	//std::vector<IRenderNode *>::const_iterator it = pSgen_sect->sector_succesfully_gen_oblects.begin();
	//std::vector<IRenderNode *>::const_iterator end = pSgen_sect->sector_succesfully_gen_oblects.end();
	int count = pSgen_sect->sector_succesfully_gen_oblects.size();
	if (count > 0)
	{
		//gEnv->p3DEngine->UnRegisterEntityDirect(pSgen_sect->sector_succesfully_gen_oblects.back());
		gEnv->p3DEngine->DeleteRenderNode(pSgen_sect->sector_succesfully_gen_oblects.back());
		pSgen_sect->sector_succesfully_gen_oblects.pop_back();
		pSgen_sect->SetNumberOfLoadedObjects(pSgen_sect->GetNumberOfLoadedObjects() - 1);
		/*for (int i = 0; i < count, it != end; i++, ++it)
		{
			if (objectIds == i)
			{
				gEnv->p3DEngine->UnRegisterEntityDirect((*it));
				gEnv->p3DEngine->DeleteRenderNode((*it));
				pSgen_sect->SetNumberOfLoadedObjects(pSgen_sect->GetNumberOfLoadedObjects() - 1);
				pSgen_sect->sector_succesfully_gen_oblects.erase(it);
				pSgen_sect->sector_succesfully_gen_oblects.resize(count - 1);
				break;
			}
		}*/
	}

	if (pSgen_sect->sector_succesfully_gen_oblects.size() <= 0)
	{
		ClearGeneratedRenderNodesInSector(pSgen_sect->GetSectorId());
	}
	pSgen_sect->SetGenerationComplete(false);
	/*std::vector<EntityId>::const_iterator it_a = pSgen_sect->sector_succesfully_gen_entites.begin();
	std::vector<EntityId>::const_iterator end_a = pSgen_sect->sector_succesfully_gen_entites.end();
	int count_a = pSgen_sect->sector_succesfully_gen_entites.size();
	if (count_a > 0)
	{
		for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
		{
			if (objectIds == i)
			{
				IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
				if (!pEntity)
					continue;

				gEnv->pEntitySystem->RemoveEntity((*it_a), true);
				pSgen_sect->sector_succesfully_gen_entites.erase(it_a);
				pSgen_sect->sector_succesfully_gen_entites.resize(count_a - 1);
				break;
			}
		}
	}*/
}

CProceduralGenerator::SGenerationSector* CProceduralGenerator::GetStreamingSectorPointer(const int & sectorIds)
{
	if(sectorIds < 0)
		return nullptr;

	std::vector<SGenerationSector *>::const_iterator it_x = generated_sectors.begin();
	std::vector<SGenerationSector *>::const_iterator end_x = generated_sectors.end();
	int count_x = generated_sectors.size();
	if (count_x > 0)
	{
		for (int ir = 0; ir < count_x, it_x != end_x; ir++, ++it_x)
		{
			SGenerationSector *pSgen_sect = (*it_x);
			if (!pSgen_sect || (pSgen_sect->GetSectorId() != sectorIds))
				continue;

			return pSgen_sect;
		}
	}
	return nullptr;
}

sint16 CProceduralGenerator::GetVegetationGrpId(const string &vgName)
{
	if(!gEnv->p3DEngine)
		return -1;

	if(vgName.empty())
		return -1;

	if (vgName.size() == 4)
	{
		if(vgName == string("auto"))
			return -1;
	}
	const bool is_in_editor = gEnv->IsEditor();
	sint16 vg_grp = -1;
	for (int ic = 0; ic < 256; ic++)
	{
		IStatInstGroup Group;
		if (!gEnv->p3DEngine->GetStatInstGroup(ic, Group))
		{
			vg_grp = -1;
			break;
		}

		string lcf = string(Group.szFileName);
		lcf.MakeLower();
		if (lcf == string(vgName).MakeLower())
		{
			vg_grp = ic;
			break;
		}
		/*if (!is_in_editor)
		{
			if (lcf == string(vgName).MakeLower())
			{
				vg_grp = ic;
				break;
			}
		}
		else
		{
			if (lcf == vgName)
			{
				vg_grp = ic;
				break;
			}
		}*/
		//CryLogAlways("GetVegetationGrpId lcf == %s", lcf);
	}
	return vg_grp;
}

void CProceduralGenerator::SetGeneratedState(bool state)
{
	already_gen = state;
}

Vec3 CProceduralGenerator::GenerateVecRandomValue(const Vec3 & min, const Vec3 & max)
{
	return Vec3(cry_random(min.x, max.x), cry_random(min.y, max.y),cry_random(min.z, max.z));
}

void CProceduralGenerator::AddObjToGenQue(SQuedGenObject object)
{
	gen_objects_queue.push(object);
}

void CProceduralGenerator::GetGenShapeSpline(std::vector<Vec3>& spline)
{
	spline = xrf_gen_shape;
}

int CProceduralGenerator::GetNearestStreamingSector(const Vec2 & point, const int & sectorSize)
{
	int sectors_number_hv = streaming_cover_size / sectorSize;
	int sectors_number = sectors_number_hv * sectors_number_hv;
	int row_pos = int(point.y + generatorWorldPoint.y/* - 64.0f*/) / sectorSize;
	if (row_pos <= 0)
	{
		row_pos = 0;
	}
	//float dist = 10000.0f;
	//int last_id = 1;
	int col_pos = int(point.x + generatorWorldPoint.x/* - 64.0f*/) / sectorSize;
	if (col_pos <= 0)
	{
		col_pos = 0;
	}
	//int requested_sector = (((row_pos * sectors_number_hv) - sectors_number_hv) + col_pos) + 1;
	int requested_sector = (row_pos * sectors_number_hv) + col_pos + 1;
	if (requested_sector > sectors_number)
		requested_sector = sectors_number;
	else if (requested_sector < 1)
		requested_sector = 1;

	return requested_sector;
	/*std::vector<int> loaded_sectors_ids2;
	for (int i = 0; i < 18; ++i)
	{
		//SLoadedSector lds = SLoadedSector();

		int sec_id = requested_sector;
		if (i > 0)
		{
			sec_id = -1;
		}

		if (i == 1)
		{
			sec_id = requested_sector + sectors_number_hv;
		}
		else if (i == 2)
		{
			sec_id = requested_sector - sectors_number_hv;
		}
		else if (i == 3)
		{
			sec_id = requested_sector + 1;
		}
		else if (i == 4)
		{
			sec_id = requested_sector - 1;
		}
		else if (i == 5)
		{
			sec_id = requested_sector + sectors_number_hv + 1;
		}
		else if (i == 6)
		{
			sec_id = requested_sector + sectors_number_hv - 1;
		}
		else if (i == 7)
		{
			sec_id = requested_sector - sectors_number_hv + 1;
		}
		else if (i == 8)
		{
			sec_id = requested_sector - sectors_number_hv - 1;
		}
		else if (i == 9)
		{
			sec_id = requested_sector + 2;
		}
		else if (i == 10)
		{
			sec_id = requested_sector - 2;
		}
		else if (i == 11)
		{
			sec_id = requested_sector + sectors_number_hv + 2;
		}
		else if (i == 12)
		{
			sec_id = requested_sector + sectors_number_hv - 2;
		}
		else if (i == 13)
		{
			sec_id = requested_sector - sectors_number_hv + 2;
		}
		else if (i == 14)
		{
			sec_id = requested_sector - sectors_number_hv - 2;
		}
		else if (i == 15)
		{
			sec_id = requested_sector + (sectors_number_hv * 2);
		}
		else if (i == 16)
		{
			sec_id = requested_sector - (sectors_number_hv * 2);
		}
		else if (i == 17)
		{
			sec_id = requested_sector + (sectors_number_hv * 2) - 1;
		}
		else if (i == 18)
		{
			sec_id = requested_sector - (sectors_number_hv * 2) + 1;
		}

		if (sec_id > sectors_number)
		{
			sec_id = sectors_number;
		}
		else if (sec_id < 1)
		{
			sec_id = 1;
		}
		loaded_sectors_ids2.push_back(sec_id);
	}
	dist = 10000.0f;
	last_id = 1;
	for (int ic = 0; ic < loaded_sectors_ids2.size(); ++ic)
	{
		if (loaded_sectors_ids2[ic] > 0)
		{
			SGenerationSector *pSgen_sect = GetStreamingSectorPointer(loaded_sectors_ids2[ic]);
			if (pSgen_sect == nullptr)
				continue;

			float dist_cx = pSgen_sect->sector_center_point.GetDistance(point);
			if (dist_cx <= dist)
			{
				dist = dist_cx;
				last_id = loaded_sectors_ids2[ic];
			}
		}
	}
	loaded_sectors_ids2.clear();
	requested_sector = last_id;
	return requested_sector;*/
}

void CProceduralGenerator::ProcessCurrentSectors()
{
	/*for (int ic = 0; ic < loaded_sectors_ids.size(); ++ic)
	{
		uint32 cq_objects_loaded = 0;
		uint32 old_numb_ojs = 9999999;
		if (loaded_sectors_ids[ic] > 0)
		{
			SGenerationSector *pSgen_sect = GetStreamingSectorPointer(loaded_sectors_ids[ic]);
			if (pSgen_sect == nullptr)
				continue;

				if (load_objects_in_streamFromStreamingGenFiles)
				{
				if (!pSgen_sect->loading_xml_complete)
				{
				continue;
				}
				}

			while ((pSgen_sect->GetNumberOfGeneratedObjects() != pSgen_sect->GetNumberOfLoadedObjects())
				|| !pSgen_sect->IsGenerationComplete())
			{
				if (old_numb_ojs == pSgen_sect->GetNumberOfLoadedObjects())
				{
					break;
				}
				old_numb_ojs = pSgen_sect->GetNumberOfLoadedObjects();
				GenerateObjectInSector(pSgen_sect, pSgen_sect->GetNumberOfLoadedObjects());
				cq_objects_loaded += 1;
				if (cq_objects_loaded >= que_perframe_limit)
					break;
			}
		}
	}*/
	std::vector<int> complete_unloading_sectors_ids;
	complete_unloading_sectors_ids.clear();
	complete_unloading_sectors_ids.resize(0);
	for (int ic = 0; ic < unloading_sectors_ids.size(); ++ic)
	{
		uint32 cq_objects_unloaded = 0;
		uint32 old_numb_ojs = 0;
		const int sect_ids = unloading_sectors_ids[ic];
		if (sect_ids > 0)
		{
			SGenerationSector *pSgen_sect = GetStreamingSectorPointer(sect_ids);
			if (pSgen_sect == nullptr)
				continue;

			/*if (load_objects_in_streamFromStreamingGenFiles)
			{
				if (!pSgen_sect->loading_xml_complete)
				{
					continue;
				}
			}*/

			if (pSgen_sect->GetNumberOfLoadedObjects() <= 0)
			{
				complete_unloading_sectors_ids.push_back(sect_ids);
				continue;
			}

			bool simply_clear_ct = false;
			for (int i = 0; i < loaded_sectors_ids.size(); ++i)
			{
				if (loaded_sectors_ids[i] == sect_ids)
				{
					simply_clear_ct = true;
					break;
				}
			}
			int failures_num = 0;
			while ((pSgen_sect->GetNumberOfLoadedObjects() > 0) && !simply_clear_ct)
			{
				if (old_numb_ojs == pSgen_sect->GetNumberOfLoadedObjects())
				{
					++failures_num;
					old_numb_ojs -= 1;
					if (failures_num > 10)
					{
						simply_clear_ct = true;
						break;
					}
					continue;
				}
				old_numb_ojs = pSgen_sect->GetNumberOfLoadedObjects();
				DeleteObjectFromSector(pSgen_sect, old_numb_ojs - failures_num);
				cq_objects_unloaded += 1;
				if (pSgen_sect->GetNumberOfLoadedObjects() <= 0)
				{
					complete_unloading_sectors_ids.push_back(sect_ids);
				}

				if (cq_objects_unloaded >= que_perframe_limit)
					break;
			}

			if (simply_clear_ct)
			{
				complete_unloading_sectors_ids.push_back(sect_ids);
				ClearGeneratedRenderNodesInSector(sect_ids);
				//CryLogAlways("CProceduralGenerator::ProcessCurrentSectors fail detected");
			}
		}
	}
	for (int ic = 0; ic < complete_unloading_sectors_ids.size(); ++ic)
	{
		RemoveUnloadedSectorFromProcessing(complete_unloading_sectors_ids[ic]);
	}
}

void CProceduralGenerator::RemoveUnloadedSectorFromProcessing(const int & idSx)
{
	std::vector<int>::const_iterator it = unloading_sectors_ids.begin();
	std::vector<int>::const_iterator end = unloading_sectors_ids.end();
	int count = unloading_sectors_ids.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if ((*it) == idSx)
			{
				ClearGeneratedRenderNodesInSector(idSx);
				unloading_sectors_ids.erase(it);
				unloading_sectors_ids.resize(count - 1);
				break;
			}
		}
	}
}

bool CProceduralGenerator::IsSectorInUnloadedSectors(const int & idSx)
{
	std::vector<int>::const_iterator it = unloading_sectors_ids.begin();
	std::vector<int>::const_iterator end = unloading_sectors_ids.end();
	int count = unloading_sectors_ids.size();
	if (count > 0)
	{
		for (int i = 0; i < count, it != end; i++, ++it)
		{
			if ((*it) == idSx)
			{
				return true;
			}
		}
	}
	return false;
}

void CProceduralGenerator::RefillGenerationSettingData()
{
	if (!GetEntity())
		return;

	SmartScriptTable props;
	IScriptTable* pScriptTable = GetEntity()->GetScriptTable();
	if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
		return;

	if (!props)
		return;

	props->GetValue("fMinHeight", setting_data.fMinHeight);
	props->GetValue("fMaxHeight", setting_data.fMaxHeight);
	props->GetValue("fMinSlope", setting_data.fMinSlope);
	props->GetValue("fMaxSlope", setting_data.fMaxSlope);
	//------------------------------------------
	props->GetValue("nGenUpDown", setting_data.gen_updwn);
	//decals------------------------------
	props->GetValue("bGenerateDecals", setting_data.decal_gen);
	props->GetValue("fDecalsGenCoof", setting_data.decals_gen_coof);
	props->GetValue("fileDecalOne", setting_data.decals[0]);
	props->GetValue("fileDecalTwo", setting_data.decals[1]);
	props->GetValue("fileDecalThree", setting_data.decals[2]);
	props->GetValue("fileDecalFour", setting_data.decals[3]);
	props->GetValue("fileDecalFive", setting_data.decals[4]);
	props->GetValue("fDecalsGenBaseScale", setting_data.decals_base_scale);
	props->GetValue("fDecalsGenRandomScale", setting_data.decals_rnd_scale);
	props->GetValue("fDecalsGenUpHeight", setting_data.decals_gen_up_ht);
	props->GetValue("fDecalsGenRandPower", setting_data.decals_gen_rnd_pwr);
	//---------------------------------
	props->GetValue("bUseOneOfGenPointsArrToDistrAllObj", setting_data.gen_arr_dist_points_for_all);
	props->GetValue("fHeigthUpFromNormal", setting_data.gen_up_from_nrm);
	props->GetValue("fHeigthUpFromNormalRand", setting_data.gen_up_from_nrm_rnd);
	//brushes--------------------------
	props->GetValue("bGenerateBrushes", setting_data.brushes_gen);
	props->GetValue("objBrushOne", setting_data.brushes[0]);
	props->GetValue("objBrushTwo", setting_data.brushes[1]);
	props->GetValue("objBrushThree", setting_data.brushes[2]);
	props->GetValue("objBrushFour", setting_data.brushes[3]);
	props->GetValue("objBrushFive", setting_data.brushes[4]);
	props->GetValue("nBrushOnePhyPrm", setting_data.brush_params[0]);
	props->GetValue("nBrushTwoPhyPrm", setting_data.brush_params[1]);
	props->GetValue("nBrushThreePhyPrm", setting_data.brush_params[2]);
	props->GetValue("nBrushFourPhyPrm", setting_data.brush_params[3]);
	props->GetValue("nBrushFivePhyPrm", setting_data.brush_params[4]);
	props->GetValue("bBrushesGenFloating", setting_data.gen_brushes_floating);
	props->GetValue("fBrushesGenCoof", setting_data.gen_brushes_coof);
	props->GetValue("fBrushesGenBaseScale", setting_data.gen_brushes_base_scale);
	props->GetValue("fBrushesGenRandomScale", setting_data.gen_brushes_rnd_scale);
	props->GetValue("fBrushesGenUpHeight", setting_data.gen_brushes_up_hgt);
	props->GetValue("fBrushesGenRandPower", setting_data.gen_brushes_rnd_pwr);
	props->GetValue("bBrushesGenFullRandRotation", setting_data.gen_brushes_rnd_rot_full);
	//---------------------------------
	//Archetype entites----------------
	props->GetValue("bGenerateArcEntites", setting_data.arc_ents_gen);
	props->GetValue("bArcEntitesGenFloating", setting_data.gen_arc_ent_floating);
	props->GetValue("sArcEntityOne", setting_data.arc_entites[0]);
	props->GetValue("sArcEntityTwo", setting_data.arc_entites[1]);
	props->GetValue("sArcEntityThree", setting_data.arc_entites[2]);
	props->GetValue("sArcEntityFour", setting_data.arc_entites[3]);
	props->GetValue("sArcEntityFive", setting_data.arc_entites[4]);
	props->GetValue("fArcEntitesGenCoof", setting_data.gen_arc_ent_coof);
	props->GetValue("fArcEntitesGenBaseScale", setting_data.gen_arc_ent_base_scale);
	props->GetValue("fArcEntitesGenRandomScale", setting_data.gen_arc_ent_rnd_scale);
	props->GetValue("fArcEntitesGenUpHeight", setting_data.gen_arc_ent_up_hgt);
	props->GetValue("fArcEntitesGenRandPower", setting_data.gen_arc_ent_rnd_pwr);
	props->GetValue("bArcEntitesGenFullRandRotation", setting_data.gen_arc_ent_rnd_rot_full);
	//---------------------------------
	//terrain surfaces ids------------------
	props->GetValue("bGenerateTerrainSurfaceIds", setting_data.terrain_surf_ids_gen);
	props->GetValue("nTerrainSurfaceIdOne", setting_data.surf_ids[0]);
	props->GetValue("nTerrainSurfaceIdTwo", setting_data.surf_ids[1]);
	props->GetValue("nTerrainSurfaceIdThree", setting_data.surf_ids[2]);
	props->GetValue("nTerrainSurfaceIdFour", setting_data.surf_ids[3]);
	props->GetValue("nTerrainSurfaceIdFive", setting_data.surf_ids[4]);
	props->GetValue("nTerrainSurfaceGenCoof", setting_data.gen_surf_coof);
	props->GetValue("fTerrainSurfaceGenRandPower", setting_data.gen_surf_rnd_pwr);
	//---------------------------------
	//vegetations------------------
	props->GetValue("bGenerateVegetations", setting_data.vegetation_gen);
	props->GetValue("objVegetationOne", setting_data.vegetations[0]);
	props->GetValue("objVegetationTwo", setting_data.vegetations[1]);
	props->GetValue("objVegetationThree", setting_data.vegetations[2]);
	props->GetValue("objVegetationFour", setting_data.vegetations[3]);
	props->GetValue("objVegetationFive", setting_data.vegetations[4]);
	props->GetValue("fVegetationIndexOne", setting_data.veg_inds[0]);
	props->GetValue("fVegetationIndexTwo", setting_data.veg_inds[1]);
	props->GetValue("fVegetationIndexThree", setting_data.veg_inds[2]);
	props->GetValue("fVegetationIndexFour", setting_data.veg_inds[3]);
	props->GetValue("fVegetationIndexFive", setting_data.veg_inds[4]);
	props->GetValue("bVegetationsGenFloating", setting_data.gen_vegetations_floating);
	props->GetValue("fVegetationsGenCoof", setting_data.vegetations_gen_coof);
	props->GetValue("fVegetationsGenBaseScale", setting_data.vegetations_base_scale);
	props->GetValue("fVegetationsGenRandomScale", setting_data.vegetations_rnd_scale);
	props->GetValue("fVegetationsGenUpHeight", setting_data.vegetations_gen_up_ht);
	props->GetValue("fVegetationsGenRandPower", setting_data.vegetations_gen_rnd_pwr);
	props->GetValue("bVegetationsGenFullRandRotation", setting_data.gen_vegetations_rnd_rot_full);
	props->GetValue("fVegetationsBending", setting_data.veg_bending);
	props->GetValue("bVegetationsAutoMerged", setting_data.veg_atm);
	props->GetValue("fGenZRotationRandomVal", setting_data.z_gen_rot_rand_val);
	props->GetValue("fGenObjectsViewDistance", setting_data.obj_view_params[0]);
	props->GetValue("fGenObjectsLodDistance", setting_data.obj_view_params[1]);
	props->GetValue("bGenOnTerrainOnly", setting_data.gen_olly_on_terrain);
	if (setting_data.gen_olly_on_terrain)
		setting_data.gen_on_stat_objs = 0;

	props->GetValue("fMinDistToOldGenObjs", setting_data.min_dist_gen_old);
	props->GetValue("fMinDistToOldGenObjsRandom", setting_data.min_dist_gen_old_rnd);
	props->GetValue("fRotToNormalRandom", setting_data.normal_rot_mult_rnd);
	props->GetValue("fRotToNormalMlt", setting_data.normal_rot_mult_x);
	setting_data.normal_rot_mult_x = min(1.0f, setting_data.normal_rot_mult_x);
	for (int i = 0; i < 2; i++)
	{
		if (setting_data.obj_view_params[i] > 255)
			setting_data.obj_view_params[i] = 255;
	}
	generation_setting_data_refilled = true;
}

string CProceduralGenerator::GetStreamSectorFilepath(const int & sectorIds)
{
	bool is_mod = false;
	if (const ICmdLineArg* pModArg = gEnv->pSystem->GetICmdLine()->FindArg(eCLAT_Pre, "MOD"))
	{
		if (gEnv->pSystem->IsMODValid(pModArg->GetValue()))
		{
			is_mod = true;
		}
	}
	string file_nmn = "";
	file_nmn.Format("Stream_sector_%i_%s%s%s", sectorIds, GetEntity()->GetName(), "_streaming_support", ".grp");
	string levelPath = PathUtil::GetGameFolder();
	ILevelInfo* pLevelInfo = gEnv->pGameFramework->GetILevelSystem()->GetCurrentLevel();
	if (pLevelInfo)
	{
		levelPath = PathUtil::AddSlash(levelPath);
		if (is_mod)
			levelPath = "";

		levelPath = levelPath + pLevelInfo->GetPath();
	}
	levelPath = PathUtil::AddSlash(levelPath);
	levelPath = levelPath + string("GeneratedObjs");
	levelPath = PathUtil::AddSlash(levelPath);
	string file_pth = levelPath + file_nmn;
	return file_pth;
}

uint32 CProceduralGenerator::GetSectorsNTNumber()
{
	uint32 sectors_number_hv = streaming_cover_size / sector_size;
	uint32 sectors_number = sectors_number_hv*sectors_number_hv;
	return sectors_number;
}

bool CProceduralGenerator::ReloadExtension(IGameObject * pGameObject, const SEntitySpawnParams &params)
{
	ResetGameObject();
	return true;
}

bool CProceduralGenerator::GetVolumeInfoForEntityGen(EntityId entityId, IGameVolumes::VolumeInfo* volumeInfo)
{
	IGameVolumes* pGameVolumesMgr = gEnv->pGameFramework->GetIGameVolumesManager();
	if (pGameVolumesMgr != NULL)
	{
		return pGameVolumesMgr->GetVolumeInfoForEntity(entityId, volumeInfo);
	}

	return false;
}

void CProceduralGenerator::TriangulatePolygon2D(const Vec2* pPolygon, const int numPolyPts, PodArray<Vec2>* pVertices, PodArray<uint8>* pIndices)
{

	if (pPolygon && numPolyPts >= 3 && numPolyPts <= 720 && (pVertices || pIndices))
	{
		int numPts = numPolyPts;
		int numTris = numPts - 2;

		// Offset for output vertex data
		int vertOffset = 0;

		if (pVertices)
		{
			vertOffset = pVertices->Count();
			pVertices->resize(vertOffset + numTris * 3);
		}

		// Offset for output index data
		int indOffset = 0;

		if (pIndices)
		{
			indOffset = pIndices->Count();
			pIndices->resize(indOffset + numTris * 3);
		}

		// Simple case, push single triangle
		if (numTris == 1)
		{
			if (pVertices)
			{
				(*pVertices)[vertOffset] = pPolygon[0];
				(*pVertices)[vertOffset + 1] = pPolygon[1];
				(*pVertices)[vertOffset + 2] = pPolygon[2];
			}

			if (pIndices)
			{
				(*pIndices)[indOffset] = 0;
				(*pIndices)[indOffset + 1] = 1;
				(*pIndices)[indOffset + 2] = 2;
			}
		}
		// Complex case, need to triangulate full point list
		else if (numTris > 1)
		{
			// Initialise outline list as pIndices
			std::vector<int> outline;
			outline.resize(numPts);

			for (int i = 0; i < numPts; ++i)
			{
				outline[i] = i;
			}

			// Already know the expected number of triangles
			for (int i = 0; i < numTris; ++i)
			{
				std::vector<int>::iterator pIterBegin = outline.begin();
				std::vector<int>::iterator pIterEnd = outline.end();

				std::vector<int>::iterator pIterA = pIterBegin;
				std::vector<int>::iterator pIterB = pIterBegin;
				std::vector<int>::iterator pIterC = pIterBegin;
				++pIterB;
				++pIterC;
				++pIterC;

				// Loop around entire list and check for ears
				for (int j = 0; j < numPts; ++j)
				{
					const Vec2& a = pPolygon[*pIterA];
					const Vec2& b = pPolygon[*pIterB];
					const Vec2& c = pPolygon[*pIterC];

					// Valid ear triangles will be convex (internal angle < 180)
					if (TriangleIsConvex2D(a, b, c))
					{
						// Check valid triangles against all other outline points
						std::vector<int>::const_iterator pIterPt = pIterBegin;
						bool isEar = true;

						do
						{
							if (pIterPt != pIterA && pIterPt != pIterB && pIterPt != pIterC)
							{
								const Vec2& pt = pPolygon[*pIterPt];

								// When we have a point in the triangle, it means this is *not* an ear
								if (PointInTriangle2D(pt, a, b, c))
								{
									isEar = false;
									break;
								}
							}

							++pIterPt;
						} while (pIterPt != pIterEnd);

						// Create triangle and clip ear when found
						if (isEar)
						{
							if (pVertices)
							{
								(*pVertices)[vertOffset] = pPolygon[*pIterA];
								(*pVertices)[vertOffset + 1] = pPolygon[*pIterB];
								(*pVertices)[vertOffset + 2] = pPolygon[*pIterC];
							}

							if (pIndices)
							{
								(*pIndices)[indOffset] = *pIterA;
								(*pIndices)[indOffset + 1] = *pIterB;
								(*pIndices)[indOffset + 2] = *pIterC;
							}

							// If ear is last point, erase immediately
							if (*pIterB == *pIterEnd)
							{
								outline.pop_back();
							}
							// May need to shuffle points along (rather than erase)
							else
							{
								while (pIterC != pIterEnd)
								{
									*pIterB = *pIterC;
									++pIterB;
									++pIterC;

									// Need to handle looping case
									if (pIterB == pIterEnd)
									{
										pIterB = pIterBegin;
									}
								}

								outline.pop_back();
							}

							// Done with this ear
							break;
						}
					}

					// Try next triangle
					++pIterA;
					++pIterB;
					++pIterC;

					// Treat outline as a ring buffer
					pIterBegin = outline.begin();
					pIterEnd = outline.end();

					if (pIterC == pIterEnd)
					{
						pIterC = pIterBegin;
					}
					else if (pIterB == pIterEnd)
					{
						pIterB = pIterBegin;
					}
				}

				// Clipped one ear point
				--numPts;
				vertOffset += 3;
				indOffset += 3;
			}
		}
	}
}

bool CProceduralGenerator::PointInTriangle2D(const Vec2& pt, const Vec2& a, const Vec2& b, const Vec2& c)
{
	float ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
	float cCROSSap, bCROSScp, aCROSSbp;

	ax = c.x - b.x;
	ay = c.y - b.y;
	bx = a.x - c.x;
	by = a.y - c.y;
	cx = b.x - a.x;
	cy = b.y - a.y;

	apx = pt.x - a.x;
	apy = pt.y - a.y;
	bpx = pt.x - b.x;
	bpy = pt.y - b.y;
	cpx = pt.x - c.x;
	cpy = pt.y - c.y;

	aCROSSbp = ax * bpy - ay * bpx;
	cCROSSap = cx * apy - cy * apx;
	bCROSScp = bx * cpy - by * cpx;

	float ra = (float)__fsel(aCROSSbp, 1.0f, 0.0f);
	float rb = (float)__fsel(cCROSSap, 1.0f, 0.0f);
	float rc = (float)__fsel(bCROSScp, 1.0f, 0.0f);

	return (ra + rb + rc) > 2.5f;
}

bool CProceduralGenerator::TriangleIsConvex2D(const Vec2& a, const Vec2& b, const Vec2& c)
{
	const Vec2 ba = a - b;
	const Vec2 bc = c - b;

	const float cross = ba.x * bc.y - bc.x * ba.y;
	return (cross < 0.0f);
}

string CProceduralGenerator::GenerateNodeXName(string fts_nm)
{
	fts_nm = RANDOMNAMESXRC::GenerateObjectName(fts_nm);
	return fts_nm;
}

class CFlowGenerateObjectsInGenerator : public CFlowBaseNode<eNCT_Instanced>
{
private:
	enum EInputPorts
	{
		EIP_Generate = 0,
		EIP_Clear,
		EIP_Save,
		EIP_UseGenMask,
		EIP_GenerationMask,
		EIP_GenerationMaskXYStart,
		EIP_MaskScale,
		EIP_MaskPixelScale,
		EIP_GenerationInAir,
		EIP_GenerationOnWater,
		EIP_GenerationOnTerrainOnly,
		EIP_GenerationOnObjectsOnly,
		EIP_GenerationAvoidObjects,
		EIP_GenerationHeight,
		EIP_GenerationPower,
		EIP_GenerationIterations,
		EIP_GenerationCalcPointRands,
		EIP_ObjectPhysics,
		EIP_ObjectType,
		EIP_ObjectMdl,
		EIP_ObjectBaseScale,
		EIP_ObjectRandomScale,
		EIP_XRandomRotation,
		EIP_YRandomRotation,
		EIP_ZRandomRotation,
		EIP_MinHeight,
		EIP_MaxHeight,
		EIP_MinSlope,
		EIP_MaxSlope,
		EIP_RotationToNormal,
		EIP_RotationToNormalRandomness,
		EIP_GenDirectionRandomness,
		EIP_GenDirectionBase,
		EIP_MinDistanceToOtherObjects,
		EIP_MDistanceTOORandom,
		EIP_OffsetFromNormal,
		EIP_OffsetFromNormalRandom,
		EIP_Offset,
		EIP_OffsetRandom,
		EIP_VisDist,
	};

	enum EOutputPorts
	{
		EOP_Done
	};

public:
	CFlowGenerateObjectsInGenerator(SActivationInfo * pActInfo)
	{
		succesfully_gen_oblects.clear();
		succesfully_gen_oblects.resize(0);
		succesfully_gen_entites.clear();
		succesfully_gen_entites.resize(0);
	}

	virtual ~CFlowGenerateObjectsInGenerator()
	{
		ClearGeneratedObjs(NULL);
	}

	IFlowNodePtr Clone(SActivationInfo * pActInfo)
	{
		return new CFlowGenerateObjectsInGenerator(pActInfo);
	}

	void Serialize(SActivationInfo* pActInfo, TSerialize ser)
	{
	}

	virtual void GetConfiguration(SFlowNodeConfig& config)
	{
		static const SInputPortConfig inputs[] =
		{
			InputPortConfig_Void("Generate", _HELP("")),
			InputPortConfig_Void("Clear", _HELP("")),
			InputPortConfig_Void("SaveToXML", _HELP("")),
			InputPortConfig<bool>("UseGenMask", false, _HELP("")),
			InputPortConfig<string>("GenerationMask", _HELP("")),
			InputPortConfig<Vec3>("GenerationMaskStartPoint", Vec3(ZERO), _HELP("")),
			InputPortConfig<float>("MaskScale", 1.0f, _HELP("")),
			InputPortConfig<float>("MaskPixelScale", 1.0f, _HELP("")),
			InputPortConfig<bool>("GenerationInAir", false, _HELP("")),
			InputPortConfig<bool>("GenerationOnWater", false, _HELP("")),
			InputPortConfig<bool>("GenerationOnTerrainOnly", false, _HELP("")),
			InputPortConfig<bool>("GenerationOnObjectsOnly", false, _HELP("")),
			InputPortConfig<bool>("GenerationAvoidObjects", false, _HELP("")),
			InputPortConfig<float>("GenerationHeight", 10.0f, _HELP("")),
			InputPortConfig<float>("GenerationRandomPower", 35.0f, _HELP("0-100")),
			InputPortConfig<float>("GenerationIters", 1.0f, _HELP("")),
			InputPortConfig<float>("CalculationPointRnds", 1.0f, _HELP("")),
			InputPortConfig<int>("ObjectPhysics", 1, _HELP("")),
			InputPortConfig<int>("ObjectType", 0, _HELP("Object type to generation"), _HELP("ObjectType"),_UICONFIG("enum_int:brush=0,decal=1,arc=2,vegetation=3")),
			InputPortConfig<string>("ObjectModel", _HELP("")),
			InputPortConfig<float>("ObjectBaseScale", 1.0f, _HELP("")),
			InputPortConfig<float>("ObjectRandomScale", 0.0f, _HELP("")),
			InputPortConfig<float>("XRandomRotation", 360.0f, _HELP("")),
			InputPortConfig<float>("YRandomRotation", 360.0f, _HELP("")),
			InputPortConfig<float>("ZRandomRotation", 360.0f, _HELP("")),
			InputPortConfig<float>("MinHeight", 0.0f, _HELP("")),
			InputPortConfig<float>("MaxHeight", 8192.0f, _HELP("")),
			InputPortConfig<float>("MinSlope", -180.0f, _HELP("")),
			InputPortConfig<float>("MaxSlope", 180.0f, _HELP("")),
			InputPortConfig<float>("RotationToNormal", 1.0f, _HELP("")),
			InputPortConfig<float>("RotationToNormalRandomness", 0.0f, _HELP("")),
			InputPortConfig<Vec3>("GenDirectionRandomness", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("GenDirectionBase", Vec3(0.0f,0.0f,-1.0f), _HELP("")),
			InputPortConfig<float>("MinDistanceToOtherObjects", false, _HELP("")),
			InputPortConfig<float>("MDistanceTOORandom", false, _HELP("")),
			InputPortConfig<Vec3>("OffsetFromNormal", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("OffsetFromNormalRandom", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("Offset", Vec3(ZERO), _HELP("")),
			InputPortConfig<Vec3>("OffsetRandom", Vec3(ZERO), _HELP("")),
			InputPortConfig<int>("VisibleDistance", 50, _HELP("")),
			{ 0 }
		};

		static const SOutputPortConfig outputs[] =
		{
			OutputPortConfig_Void("Done", _HELP("")),
			{ 0 }
		};

		config.nFlags |= EFLN_TARGET_ENTITY;
		config.pInputPorts = inputs;
		config.pOutputPorts = outputs;
		config.sDescription = _HELP("G");
		config.SetCategory(EFLN_ADVANCED);
	}

	virtual void ProcessEvent(EFlowEvent event, SActivationInfo *pActInfo)
	{
		switch (event)
		{
		case eFE_Activate:
		{
			CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
			if (IsPortActive(pActInfo, EIP_Generate))
			{

				if (GetPortBool(pActInfo, EIP_UseGenMask))
				{
					if (GetPortString(pActInfo, EIP_GenerationMask).empty())
						return;

					GenerateByMask(pActInfo);
				}

				if (pProcGen != NULL && !GetPortBool(pActInfo, EIP_UseGenMask))
				{
					SmartScriptTable props;
					IScriptTable* pScriptTable = pProcGen->GetEntity()->GetScriptTable();
					if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
						return;

					if (!props)
						return;

					bool is_spln_gen = false;
					bool is_sphn_gen = false;
					props->GetValue("bOnSplineGen", is_spln_gen);
					props->GetValue("bOnShapeBorderGen", is_sphn_gen);
					if (is_spln_gen || is_sphn_gen)
					{
						pProcGen->ReconstructSplineFromShape();
					}
					GenerateByGeneratorShape(pActInfo);
					pProcGen->SetGeneratedState(true);
				}
			}

			if (IsPortActive(pActInfo, EIP_Clear))
			{
				ClearGeneratedObjs(pActInfo);
			}

			if (IsPortActive(pActInfo, EIP_Save))
			{
				SaveObjects(pActInfo);
			}
			ActivateOutput(pActInfo, EOP_Done, true);
			break;
		}
		case eFE_Update:
		{
			if (!pActInfo->pEntity)
				return;

			break;
		}
		break;
		}
	}

	void GenerateByMask(SActivationInfo *pActInfo)
	{
		ICryPak* pCryPak = gEnv->pCryPak;
		if (!pCryPak)
			return;

		FILE* pFile = pCryPak->FOpen(GetPortString(pActInfo, EIP_GenerationMask), "rbx");
		if (pFile)
		{
			int width = 0;
			int height = 0;
			int depth = 0;
			bool bSuccess = BMPXX::LoadBMP("", pFile, 0, width, height, depth, false);
			if (bSuccess)
			{
				DynArray<uint8> data;
				data.resize(width * height * depth);
				bSuccess = BMPXX::LoadBMP("", pFile, data.begin(), width, height, depth, false);
				if (bSuccess)
				{
					float BitmapScale = GetPortFloat(pActInfo, EIP_MaskScale);
					float BitmapGenPixelScale = GetPortFloat(pActInfo, EIP_MaskPixelScale);
					int origBytesPerLine = width * depth;
					for (int y = 0; y < height; y++)
					{
						int src_y = y;
						src_y = (height - 1) - y;
						uint8* pSrcLine = data.begin() + src_y * origBytesPerLine;
						if (depth == 3)
						{
							uint8 r, g, b;
							for (int x = 0; x < width; x++)
							{
								b = *pSrcLine++;
								g = *pSrcLine++;
								r = *pSrcLine++;
								float gen_xp = float(b + g + r);
								if (gen_xp > 0.0f)
								{
									gen_xp = (gen_xp / 768.0f) * 100.0f;
									Vec2 xy_map(float(x*BitmapScale), float(y*BitmapScale));
									int gen_Iters = int(GetPortFloat(pActInfo, EIP_GenerationIterations)*BitmapGenPixelScale);
									if (gen_Iters <= 1)
										gen_Iters = 1;

									CProceduralGenerator::SQuedGenObject objPrm = CProceduralGenerator::SQuedGenObject();
									for (int i = 0; i < gen_Iters; ++i)
									{
										GenerateObjectParamsAtTerrainPoint(pActInfo, objPrm, xy_map, gen_xp, BitmapGenPixelScale);
										if (objPrm.object_slot == 66)
											continue;

										GenerateObject(pActInfo, objPrm);
									}
								}
							}
						}
					}
					if (pActInfo->pEntity)
					{
						CProceduralGenerator* pProcGen = static_cast<CProceduralGenerator*>(gEnv->pGameFramework->QueryGameObjectExtension(pActInfo->pEntity->GetId(), "ProceduralGenerator"));
						if(pProcGen)
						{
							pProcGen->SetGeneratedState(true);
						}
					}
					data.clear();
					return;
				}
				data.clear();
			}
		}
	}

	void GenerateByGeneratorShape(SActivationInfo *pActInfo)
	{
		CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
		if(!pProcGen)
			return;

		IGameVolumes::VolumeInfo volumeInfo;
		if (!pProcGen->GetVolumeInfoForEntityGen(pProcGen->GetEntityId(), &volumeInfo))
			return;

		AABB genBounds;
		pProcGen->GetEntity()->GetWorldBounds(genBounds);
		Matrix34 mat = pProcGen->GetEntity()->GetWorldTM();
		mat.SetScale(Vec3(1.0f));
		mat.SetTranslation(pProcGen->GetEntity()->GetWorldPos());
		uint32 vertexCount = volumeInfo.verticesCount;
		float volumeHeight = volumeInfo.volumeHeight;
		float dist = 0.0f;
		int vertex_last = 1;
		genBounds.min = mat.TransformPoint(volumeInfo.pVertices[0]);
		std::vector<Vec3> poli_shape;
		poli_shape.clear();
		poli_shape.push_back(genBounds.min);
		for (uint32 i = 1; i < vertexCount; ++i)
		{
			Vec3 v1 = volumeInfo.pVertices[i];
			poli_shape.push_back(mat.TransformPoint(v1));
			float dv = v1.GetDistance(volumeInfo.pVertices[0]);
			if (dist < dv)
			{
				dist = dv;
				vertex_last = i;
			}
		}
		genBounds.max = mat.TransformPoint(volumeInfo.pVertices[vertex_last]) + Vec3(0, 0, volumeHeight);
		dist = genBounds.min.GetDistance(genBounds.max)*2.0f;
		dist *= 1.3f;

		float z_bouds = genBounds.min.z;
		if (z_bouds < genBounds.max.z)
		{
			z_bouds = genBounds.max.z;
		}

		if (pProcGen->IsSplineBased())
		{
			std::vector<Vec3> spline_pnts;
			spline_pnts.clear();
			spline_pnts.resize(0);
			pProcGen->GetGenShapeSpline(spline_pnts);
			std::vector<Vec3>::const_iterator it_gtl = spline_pnts.begin();
			std::vector<Vec3>::const_iterator end_gtl = spline_pnts.end();
			int countxx = spline_pnts.size();
			if (countxx <= 0)
				return;

			SmartScriptTable props;
			IScriptTable* pScriptTable = pProcGen->GetEntity()->GetScriptTable();
			if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
				return;

			float ShapeGenWidth = 1.0f;
			props->GetValue("fShapeGenWidth", ShapeGenWidth);

			dist = ShapeGenWidth;
			Vec3 old_pnt(ZERO);
			float gen_coof_x = GetPortFloat(pActInfo, EIP_GenerationIterations);
			for (int ifc = 0; ifc < countxx, it_gtl != end_gtl; ifc++, ++it_gtl)
			{
				if (ifc == 0)
				{
					old_pnt = (*it_gtl);
					continue;
				}
				Vec3 cvdx = (*it_gtl);
				Vec3 segment_dirc = (cvdx - old_pnt).GetNormalizedSafe();
				dist = cvdx.GetDistance(old_pnt);
				for (int ic = 0; ic < int(gen_coof_x*dist); ic++)
				{
					Vec3 pnt = old_pnt + (segment_dirc * cry_random(0.0f, dist));
					if (ShapeGenWidth > 0.0f)
					{
						pnt = pnt + (segment_dirc.GetRotated(Vec3(0, 0, 1), cry_random(-3.5f, 3.5f)) * cry_random(0.0f, ShapeGenWidth));
					}
					pnt.z = cry_random(old_pnt.z, z_bouds);
					CProceduralGenerator::SQuedGenObject objPrm = CProceduralGenerator::SQuedGenObject();
					GenerateObjectParams(pActInfo, objPrm, pnt);
					if (objPrm.object_slot == 66)
						continue;

					GenerateObject(pActInfo, objPrm);
				}
			}
			return;
		}

		int gen_Iters = int(GetPortFloat(pActInfo, EIP_GenerationIterations)*dist);
		if (gen_Iters <= 1)
			gen_Iters = 1;

		for (int i = 0; i < gen_Iters; ++i)
		{
			CProceduralGenerator::SQuedGenObject objPrm = CProceduralGenerator::SQuedGenObject();
			Vec3 pnt = pProcGen->GetRandomPointInShape(poli_shape, genBounds.min.z, z_bouds);
			if (Overlap::Point_Polygon2D(pnt, poli_shape) && (z_bouds >= pnt.z))
			{

				GenerateObjectParams(pActInfo, objPrm, pnt);
				if (objPrm.object_slot == 66)
				{
					//objPrm.object_slot = 0;
					continue;
				}

				GenerateObject(pActInfo, objPrm);
			}
		}
	}

	void GenerateObjectParamsAtTerrainPoint(SActivationInfo *pActInfo, CProceduralGenerator::SQuedGenObject &objPar, const Vec2 &point, const float &genPower, const float &cellScale)
	{
		AABB genBounds;
		float dist = 0.0f;
		float terr_elev = 0.0f;
		Vec3 wrld_pos = GetPortVec3(pActInfo, EIP_GenerationMaskXYStart);
		CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
		if (pProcGen && GetPortVec3(pActInfo, EIP_GenerationMaskXYStart).IsZero())
		{
			wrld_pos = pProcGen->GetEntity()->GetWorldPos();
		}
		Vec3 conv_point = Vec3(wrld_pos.x + point.x, wrld_pos.y + point.y, 0.0f);
		terr_elev = gEnv->p3DEngine->GetTerrainElevation(conv_point.x, conv_point.y);
		conv_point.z = terr_elev + GetPortFloat(pActInfo, EIP_GenerationHeight) / 2.0f;
		Vec3 scalx = Vec3(0.5f * cellScale, 0.5f * cellScale, 0);
		genBounds.min = conv_point - scalx;
		conv_point.z = conv_point.z + cry_random(-(GetPortFloat(pActInfo, EIP_GenerationHeight) / 2.5f), GetPortFloat(pActInfo, EIP_GenerationHeight) / 2.5f);
		genBounds.max = conv_point + scalx;
		dist = Vec2(genBounds.min).GetDistance(Vec2(genBounds.max));
		float z_bouds = genBounds.min.z;
		if (z_bouds < genBounds.max.z)
		{
			z_bouds = genBounds.max.z;
		}
		float gen_chance = cry_random(0.0f, genPower);
		if (cry_random(0.0f, 99.0f) >= GetPortFloat(pActInfo, EIP_GenerationPower))
		{
			objPar.object_slot = 66;
			return;
		}
		bool generatedc = false;
		int gen_coof_c = int(GetPortFloat(pActInfo, EIP_GenerationCalcPointRands)*dist);
		for (int ic = 0; ic < gen_coof_c; ic++)
		{
			float ntcg = cry_random(0.0f, 100.0f);
			if (ntcg >= gen_chance)
			{
				continue;
			}
			Vec3 pnt = VecRandomVal(genBounds.min, genBounds.max);
			if (pnt.IsValid())
			{
				if (GetPortBool(pActInfo, EIP_GenerationInAir))
				{
					Vec3 nrm = Vec3(0, 0, 1);
					pnt += AddOffsetToPos(pActInfo, pnt, nrm);
					Matrix34 mat;
					mat.CreateIdentity();
					mat.SetTranslation(pnt);
					objPar.object_pos = pnt;
					objPar.object_rot = GenOrientation(pActInfo, mat, nrm);
					objPar.object_scale = GenScale(pActInfo);
					generatedc = true;
				}
				else
				{
					int gen_on_stat_objs = ent_static;
					int gen_on_terrain = ent_terrain;
					int gen_on_water = 0;
					if (GetPortBool(pActInfo, EIP_GenerationOnTerrainOnly))
					{
						gen_on_stat_objs = 0;
						gen_on_terrain = ent_terrain;
					}

					if (GetPortBool(pActInfo, EIP_GenerationOnObjectsOnly))
					{
						gen_on_stat_objs = ent_static;
						gen_on_terrain = 0;
					}

					if (GetPortBool(pActInfo, EIP_GenerationOnWater))
					{
						gen_on_water = ent_water;
					}

					int flags = gen_on_stat_objs | gen_on_terrain | gen_on_water;
					Vec3 gen_dir_x = GenDirection(pActInfo);
					ray_hit hit;
					int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, gen_dir_x * GetPortFloat(pActInfo, EIP_GenerationHeight), flags,
						rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

					bool under_terrain = false;
					if (n <= 0)
					{
						n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, -gen_dir_x * GetPortFloat(pActInfo, EIP_GenerationHeight), flags,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						if (n > 0)
						{
							under_terrain = true;
						}
					}

					if (under_terrain)
					{
						pnt.z = pnt.z + cry_random(5.0f, 8.0f);
						n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, gen_dir_x * GetPortFloat(pActInfo, EIP_GenerationHeight), flags,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
					}

					if (n > 0)
					{
						if (!CanGenerate(pActInfo, hit.pt, hit.n, under_terrain))
						{
							continue;
						}
						hit.n = GenNormal(pActInfo, hit.n, gen_dir_x);
						Vec3 nrm = hit.n;
						if (under_terrain)
							nrm = -nrm;

						pnt = hit.pt;
						pnt += AddOffsetToPos(pActInfo, pnt, nrm);
						Matrix34 mat;
						mat.CreateIdentity();
						mat.SetTranslation(pnt);
						objPar.object_pos = pnt;
						objPar.object_rot = GenOrientation(pActInfo, mat, nrm);
						objPar.object_scale = GenScale(pActInfo);
						generatedc = true;
					}
				}
			}
		}
		if(!generatedc)
		{
			objPar.object_slot = 66;
			return;
		}
		else
		{
			objPar.object_slot = 1;
		}
	}

	void GenerateObjectParams(SActivationInfo *pActInfo, CProceduralGenerator::SQuedGenObject &objParams, const Vec3 &point)
	{
		CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
		if(!pProcGen)
		{
			objParams.object_slot = 66;
			return;
		}

		if (cry_random(0.0f, 99.0f) >= GetPortFloat(pActInfo, EIP_GenerationPower))
		{
			objParams.object_slot = 66;
			return;
		}
		bool generatedc = false;
		int gen_coof_c = int(GetPortFloat(pActInfo, EIP_GenerationCalcPointRands));
		if (gen_coof_c < 1)
			gen_coof_c = 1;

		for (int ic = 0; ic < gen_coof_c; ic++)
		{
			Vec3 pnt = point;
			if (pnt.IsValid())
			{
				if (GetPortBool(pActInfo, EIP_GenerationInAir))
				{
					Vec3 nrm = Vec3(0, 0, 1);
					pnt += AddOffsetToPos(pActInfo, pnt, nrm);
					Matrix34 mat;
					mat.CreateIdentity();
					mat.SetTranslation(pnt);
					objParams.object_pos = pnt;
					objParams.object_rot = GenOrientation(pActInfo, mat, nrm);
					objParams.object_scale = GenScale(pActInfo);
					generatedc = true;
				}
				else
				{
					int gen_on_stat_objs = ent_static;
					int gen_on_terrain = ent_terrain;
					int gen_on_water = 0;
					if (GetPortBool(pActInfo, EIP_GenerationOnTerrainOnly))
					{
						gen_on_stat_objs = 0;
						gen_on_terrain = ent_terrain;
					}

					if (GetPortBool(pActInfo, EIP_GenerationOnObjectsOnly))
					{
						gen_on_stat_objs = ent_static;
						gen_on_terrain = 0;
					}

					if (GetPortBool(pActInfo, EIP_GenerationOnWater))
					{
						gen_on_water = ent_water;
					}

					int flags = gen_on_stat_objs | gen_on_terrain | gen_on_water;
					Vec3 gen_dir_x = GenDirection(pActInfo);
					ray_hit hit;
					int n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, gen_dir_x * GetPortFloat(pActInfo, EIP_GenerationHeight), flags,
						rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

					bool under_terrain = false;
					if (n <= 0)
					{
						n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, -gen_dir_x * GetPortFloat(pActInfo, EIP_GenerationHeight), flags,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);

						if (n > 0)
						{
							under_terrain = true;
						}
					}

					if (under_terrain)
					{
						pnt.z = pnt.z + cry_random(5.0f, 8.0f);
						n = gEnv->pPhysicalWorld->RayWorldIntersection(pnt, gen_dir_x * GetPortFloat(pActInfo, EIP_GenerationHeight), flags,
							rwi_colltype_any | rwi_stop_at_pierceable | rwi_ignore_back_faces, &hit, 1, 0, 0);
					}

					if (n > 0)
					{
						if (!CanGenerate(pActInfo, hit.pt, hit.n, under_terrain))
						{
							continue;
						}
						hit.n = GenNormal(pActInfo, hit.n, gen_dir_x);
						Vec3 nrm = hit.n;
						if (under_terrain)
							nrm = -nrm;

						pnt = hit.pt;
						pnt += AddOffsetToPos(pActInfo, pnt, nrm);
						Matrix34 mat;
						mat.CreateIdentity();
						mat.SetTranslation(pnt);
						objParams.object_pos = pnt;
						objParams.object_rot = GenOrientation(pActInfo, mat, nrm);
						objParams.object_scale = GenScale(pActInfo);
						generatedc = true;
					}
				}
			}
		}
		if (!generatedc)
		{
			objParams.object_slot = 66;
			return;
		}
		else
		{
			objParams.object_slot = 1;
		}
	}

	void GenerateObject(SActivationInfo *pActInfo, const CProceduralGenerator::SQuedGenObject &objParams)
	{
		CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
		CProceduralGenerator::SQuedGenObject objPrmx = CProceduralGenerator::SQuedGenObject();
		objPrmx.object_pos = objParams.object_pos;
		objPrmx.object_rot = objParams.object_rot;
		objPrmx.object_scale = objParams.object_scale;
		objPrmx.physics_enable = (GetPortInt(pActInfo, EIP_ObjectPhysics) != 0);
		objPrmx.object_slot = 1;
		objPrmx.object_type = GetPortInt(pActInfo, EIP_ObjectType) + 1;
		cry_strcpy(objPrmx.object_mdl, GetPortString(pActInfo, EIP_ObjectMdl));
		//objPrmx.object_mdl = GetPortString(pActInfo, EIP_ObjectMdl);
		if (pProcGen)
		{
			SmartScriptTable props;
			IScriptTable* pScriptTable = pProcGen->GetEntity()->GetScriptTable();
			if (!pScriptTable || !pScriptTable->GetValue("Properties", props))
				return;

			bool use_que = false;
			props->GetValue("bEnableQLoading", use_que);
			if (use_que)
			{
				pProcGen->AddObjToGenQue(objPrmx);
			}
			else
			{
				pProcGen->GreateObjectFromQue(objPrmx);
			}
		}
		else
		{
			int obj_view_params[2];
			for (int i = 0; i < 2; i++)
			{
				obj_view_params[i] = GetPortInt(pActInfo, EIP_VisDist);
			}

			for (int i = 0; i < 2; i++)
			{
				if (obj_view_params[i] > 255)
					obj_view_params[i] = 255;

				if (obj_view_params[i] <= 0)
					obj_view_params[i] = 1;
			}

			if (objPrmx.object_type == 1)
			{
				IRenderNode *pNode = NULL;
				pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Brush);
				IStatObj *pObj = NULL;
				pObj = gEnv->p3DEngine->LoadStatObj(objPrmx.object_mdl, NULL, NULL, false);
				if (pNode)
				{
					IBrush *pBrush = (IBrush*)pNode;
					if (pBrush && pObj)
					{
						Matrix34 mat = Matrix34::CreateIdentity();
						mat.SetTranslation(objPrmx.object_pos);
						mat.Set(objPrmx.object_scale, objPrmx.object_rot, objPrmx.object_pos);
						pObj->AddRef();
						AABB box2 = pObj->GetAABB();
						box2.SetTransformedAABB(mat, box2);
						pBrush->SetBBox(box2);
						pBrush->SetMaterial(0);
						pBrush->OffsetPosition(Vec3(ZERO));
						pBrush->SetEntityStatObj(0, pObj, 0);
						pBrush->SetRndFlags(1610612744 | ERF_PROCEDURAL, true);
						pBrush->SetLodRatio(obj_view_params[1]);
						pBrush->SetViewDistRatio(obj_view_params[0]);
						if (objPrmx.physics_enable)
							pBrush->Physicalize();

						pBrush->SetBBox(box2);
						gEnv->p3DEngine->RegisterEntity(pBrush);
						pBrush->SetMatrix(mat);
						pBrush->SetBBox(box2);
						pBrush->SetDrawLast(false);
						if (!objPrmx.physics_enable)
							pBrush->Dephysicalize();

					}
					succesfully_gen_oblects.push_back(pNode);
				}
			}
			else if (objPrmx.object_type == 2)
			{
				IRenderNode *pNode = NULL;
				pNode = gEnv->p3DEngine->CreateRenderNode(eERType_Decal);
				pNode->SetRndFlags(pNode->GetRndFlags() | ERF_PROCEDURAL, true);
				Matrix34 mat = Matrix34::CreateIdentity();
				mat.SetTranslation(objPrmx.object_pos);
				mat.Set(objPrmx.object_scale, objPrmx.object_rot, objPrmx.object_pos);
				SDecalProperties decalProperties;
				decalProperties.m_projectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
				decalProperties.m_pos = objPrmx.object_pos;
				decalProperties.m_normal = mat.TransformPoint(Vec3(0, 0, 1));
				Matrix33 rotation(mat);
				rotation.SetRow(0, rotation.GetRow(0).GetNormalized());
				rotation.SetRow(1, rotation.GetRow(1).GetNormalized());
				rotation.SetRow(2, rotation.GetRow(2).GetNormalized());

				decalProperties.m_pMaterialName = objPrmx.object_mdl;
				decalProperties.m_radius = objPrmx.object_scale.x;
				decalProperties.m_explicitRightUpFront = rotation;
				decalProperties.m_sortPrio = cry_random(1, 150);
				decalProperties.m_deferred = true;
				decalProperties.m_depth = cry_random(0.5f, 3.0f);

				IDecalRenderNode *pRenderNode = static_cast<IDecalRenderNode*>(pNode);
				if (pRenderNode)
					pRenderNode->SetDecalProperties(decalProperties);

				if (pNode)
				{
					pNode->SetMatrix(mat);
					pNode->SetViewDistRatio(obj_view_params[0]);
					gEnv->p3DEngine->RegisterEntity(pNode);
					succesfully_gen_oblects.push_back(pNode);
				}
			}
			else if (objPrmx.object_type == 3)
			{

			}
			else if (objPrmx.object_type == 4)
			{
				ITerrain* pTerrain = gEnv->p3DEngine->GetITerrain();
				if (!pTerrain)
					return;

				int veg_ids = atoi(objPrmx.object_mdl);
				IVegetation *pBrush = nullptr;
				uint8 ang_veg[3];
				Ang3 orient_xc = Ang3(objPrmx.object_rot);
				ang_veg[0] = uint8(RAD2DEG(orient_xc.z / 360.0f) * 255.0f);
				ang_veg[1] = uint8(RAD2DEG(orient_xc.x / 360.0f) * 255.0f);
				ang_veg[2] = uint8(RAD2DEG(orient_xc.y / 360.0f) * 255.0f);
				pBrush = (IVegetation*)pTerrain->AddVegetationInstance(veg_ids, objPrmx.object_pos, objPrmx.object_scale.x, 1, ang_veg[0], ang_veg[1], ang_veg[2]);
				if (pBrush)
				{
					pBrush->SetRndFlags(pBrush->GetRndFlags() | ERF_PROCEDURAL, true);
					IStatInstGroup Group;
					if (gEnv->p3DEngine->GetStatInstGroup(veg_ids, Group))
					{
						if (!Group.bAutoMerged)
							succesfully_gen_oblects.push_back((IRenderNode *)pBrush);
					}
				}
			}
		}
	}

	CProceduralGenerator* GetProcGenSafe(SActivationInfo *pActInfo)
	{
		CProceduralGenerator* pProcGen = NULL;
		if(pActInfo == NULL)
			return pProcGen;

		if (pActInfo->pEntity)
		{
			pProcGen = static_cast<CProceduralGenerator*>(gEnv->pGameFramework->QueryGameObjectExtension(pActInfo->pEntity->GetId(), "ProceduralGenerator"));
		}
		return pProcGen;
	}

	Vec3 VecRandomVal(const Vec3 & min, const Vec3 & max)
	{
		return Vec3(cry_random(min.x, max.x), cry_random(min.y, max.y), cry_random(min.z, max.z));
	}

	Quat GenOrientation(SActivationInfo *pActInfo, const Matrix34 & orig_mtx, const Vec3 & normal)
	{
		Vec3 x(orig_mtx.GetColumn0().GetNormalized());
		Vec3 y(orig_mtx.GetColumn1().GetNormalized());
		Vec3 z(normal.GetNormalized());

		y = z.Cross(x);
		if (y.GetLengthSquared() < 1e-4f)
			y = z.GetOrthogonal();
		y.Normalize();
		x = y.Cross(z);

		Matrix33 newOrient;
		newOrient.SetColumn(0, x);
		newOrient.SetColumn(1, y);
		newOrient.SetColumn(2, z);
		Quat q(newOrient);
		if (GetPortFloat(pActInfo, EIP_ZRandomRotation) != 0.0f)
		{
			q = (q * Quat(Matrix33::CreateRotationZ(DEG2RAD(cry_random(0.0f, GetPortFloat(pActInfo, EIP_ZRandomRotation))))));
		}
		if (GetPortFloat(pActInfo, EIP_XRandomRotation) != 0.0f)
		{
			q = (q * Quat(Matrix33::CreateRotationX(DEG2RAD(cry_random(0.0f, GetPortFloat(pActInfo, EIP_XRandomRotation))))));
		}
		if (GetPortFloat(pActInfo, EIP_YRandomRotation) != 0.0f)
		{
			q = (q * Quat(Matrix33::CreateRotationY(DEG2RAD(cry_random(0.0f, GetPortFloat(pActInfo, EIP_XRandomRotation))))));
		}
		return q;
	}

	Vec3 GenScale(SActivationInfo *pActInfo)
	{
		float add_rads = 0.0f;
		if (GetPortFloat(pActInfo, EIP_ObjectRandomScale) > 0.0f)
		{
			add_rads = cry_random(-GetPortFloat(pActInfo, EIP_ObjectRandomScale), GetPortFloat(pActInfo, EIP_ObjectRandomScale));
		}
		float scl = GetPortFloat(pActInfo, EIP_ObjectBaseScale) + add_rads;
		if (scl < 0.001f)
		{
			scl = cry_random(0.001f, GetPortFloat(pActInfo, EIP_ObjectBaseScale));
		}
		Vec3 scale = Vec3(scl);
		return scale;
	}

	Vec3 AddOffsetToPos(SActivationInfo *pActInfo, Vec3 &pos, const Vec3 &norm)
	{
		Vec3 offset = Vec3(ZERO);
		if (!GetPortVec3(pActInfo, EIP_Offset).IsZero())
		{
			offset = GetPortVec3(pActInfo, EIP_Offset);
			if (!GetPortVec3(pActInfo, EIP_OffsetRandom).IsZero())
			{
				offset += VecRandomVal(-GetPortVec3(pActInfo, EIP_OffsetRandom), GetPortVec3(pActInfo, EIP_OffsetRandom));
			}
		}
		return offset;
	}

	Vec3 GenDirection(SActivationInfo *pActInfo)
	{
		Vec3 dir = GetPortVec3(pActInfo, EIP_GenDirectionBase);
		if (!GetPortVec3(pActInfo, EIP_GenDirectionRandomness).IsZero())
		{
			dir += VecRandomVal(-GetPortVec3(pActInfo, EIP_GenDirectionRandomness), GetPortVec3(pActInfo, EIP_GenDirectionRandomness));
		}
		return dir;
	}

	Vec3 GenNormal(SActivationInfo *pActInfo,const Vec3 &norm, const Vec3 &dir)
	{
		Vec3 nrm = norm;
		if (GetPortFloat(pActInfo, EIP_RotationToNormal) < 1.0f)
		{
			float nrm_rot_mlt_rnd_val_xc = GetPortFloat(pActInfo, EIP_RotationToNormal) + cry_random(
				-GetPortFloat(pActInfo, EIP_RotationToNormalRandomness), GetPortFloat(pActInfo, EIP_RotationToNormalRandomness));
			nrm = Vec3::CreateLerp(Vec3(-dir), norm, nrm_rot_mlt_rnd_val_xc);
		}
		return nrm;
	}

	bool IsPointNotTooCloseToOldObjs(const Vec3 &point, const float &dist)
	{
		bool is_close = false;
		std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
		std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
		float x_cnr_val = point.x + dist;
		float y_cnr_val = point.y + dist;
		float x_dnr_val = point.x - dist;
		float y_dnr_val = point.y - dist;
		int count = succesfully_gen_oblects.size();
		if (count > 0)
		{
			for (int i = 0; i < count, it != end; i++, ++it)
			{
				//if((*it) == nullptr)
				//	continue;

				Vec3 Pos = (*it)->GetPos();
				if (Pos.x > x_cnr_val || Pos.y > y_cnr_val || Pos.x < x_dnr_val || Pos.y < y_dnr_val)
					continue;

				bool iscn = FCVPointCLCheck(Pos, point, dist);
				if (iscn)
				{
					continue;
				}
				else
				{
					is_close = true;
					break;
				}
			}
		}
		//ents
		std::vector<EntityId>::const_iterator it_a = succesfully_gen_entites.begin();
		std::vector<EntityId>::const_iterator end_a = succesfully_gen_entites.end();
		int count_a = succesfully_gen_entites.size();
		if ((count_a > 0) && !is_close)
		{
			for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
			{
				IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
				if (!pEntity)
					continue;

				Vec3 Pos = pEntity->GetWorldPos();
				bool iscn = FCVPointCLCheck(Pos, point, dist);
				if (iscn)
				{
					continue;
				}
				else
				{
					is_close = true;
					break;
				}
			}
		}
		return !is_close;
	}

	bool FCVPointCLCheck(const Vec3 & point, const Vec3 & point2, const float & dist)
	{
		Vec3 cs = point - point2;
		bool is_cl = false;
		if (cs.GetLengthSquared() > dist)
			is_cl = true;

		return is_cl;
	}

	bool ValidateSlopeAndHeight(const Vec3 & point, const Vec3 & normal, const float & slopeMin, const float & slopeMax, const float & heightMin, const float & heightMax)
	{
		bool is_valid = true;
		if ((point.z < heightMin) || (point.z > heightMax))
		{
			is_valid = false;
		}
		else
		{
			//Vec3 Nl_normal = Vec3(0, 0, 1);
			float fGroundAngle = RAD2DEG(acos_tpl(normal.z));
			if ((fGroundAngle < slopeMin) || (fGroundAngle > slopeMax))
			{
				is_valid = false;
			}
		}

		return is_valid;
	}

	bool CanGenerate(SActivationInfo *pActInfo, const Vec3 &pos, const Vec3 & normal, const bool &trg)
	{
		if (GetPortFloat(pActInfo, EIP_MinDistanceToOtherObjects) > 0.0f)
		{
			float min_distance_gen_random_xc = GetPortFloat(pActInfo, EIP_MinDistanceToOtherObjects) + cry_random(
				-GetPortFloat(pActInfo, EIP_MDistanceTOORandom), GetPortFloat(pActInfo, EIP_MDistanceTOORandom));
			if (!IsPointNotTooCloseToOldObjs(pos, min_distance_gen_random_xc))
			{
				return false;
			}
		}
		Vec3 nrm = normal;
		if (trg)
			nrm = -nrm;

		if (!ValidateSlopeAndHeight(pos, nrm, GetPortFloat(pActInfo, EIP_MinSlope), GetPortFloat(pActInfo, EIP_MaxSlope),
			GetPortFloat(pActInfo, EIP_MinHeight), GetPortFloat(pActInfo, EIP_MaxHeight)))
		{
			return false;
		}
		return true;
	}

	void ClearGeneratedObjs(SActivationInfo *pActInfo)
	{
		CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
		if (pProcGen)
		{
			pProcGen->ClearGeneratedRenderNodes();
			pProcGen->SetGeneratedState(false);
			return;
		}
		std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
		std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
		int count = succesfully_gen_oblects.size();
		if (count > 0)
		{
			for (int i = 0; i < count, it != end; i++, ++it)
			{
				gEnv->p3DEngine->UnRegisterEntityDirect((*it));
				gEnv->p3DEngine->DeleteRenderNode((*it));
			}
		}
		succesfully_gen_oblects.clear();
		succesfully_gen_oblects.resize(0);
		//ents
		std::vector<EntityId>::const_iterator it_a = succesfully_gen_entites.begin();
		std::vector<EntityId>::const_iterator end_a = succesfully_gen_entites.end();
		int count_a = succesfully_gen_entites.size();
		if (count_a > 0)
		{
			for (int i = 0; i < count_a, it_a != end_a; i++, ++it_a)
			{
				IEntity* pEntity = gEnv->pEntitySystem->GetEntity((*it_a));
				if (!pEntity)
					continue;

				gEnv->pEntitySystem->RemoveEntity((*it_a), true);
			}
		}
		succesfully_gen_entites.clear();
		succesfully_gen_entites.resize(0);
	}

	void SaveObjects(SActivationInfo *pActInfo)
	{
		CProceduralGenerator* pProcGen = GetProcGenSafe(pActInfo);
		if (pProcGen)
		{
			pProcGen->SaveObjectsInXML();
			return;
		}

		bool is_mod = false;
		if (const ICmdLineArg* pModArg = gEnv->pSystem->GetICmdLine()->FindArg(eCLAT_Pre, "MOD"))
		{
			if (gEnv->pSystem->IsMODValid(pModArg->GetValue()))
			{
				is_mod = true;
			}
		}
		string file_nmn = RANDOMNAMESXRC::GenerateObjectName("Generated_Objects_Group_") + string(".grp");
		string levelPath = PathUtil::GetGameFolder();
		ILevelInfo* pLevelInfo = gEnv->pGameFramework->GetILevelSystem()->GetCurrentLevel();
		if (pLevelInfo)
		{
			levelPath = PathUtil::AddSlash(levelPath);
			if (is_mod)
				levelPath = "";

			levelPath = levelPath + pLevelInfo->GetPath();
		}
		levelPath = PathUtil::AddSlash(levelPath);
		levelPath = levelPath + string("GeneratedObjs");
		levelPath = PathUtil::AddSlash(levelPath);
		string file_pth = levelPath;
		file_pth.append(file_nmn);

		XmlNodeRef Objects_node = GetISystem()->CreateXmlNode("Objects");
		if (!Objects_node)
		{
			CryLogAlways("Cannot create xml node Objects(fnc CProceduralGenerator::SaveObjectsInXML)");
			return;
		}
		bool obj_added_to_fl = false;
		string layer = "DefaultGenerated";
		string LayerGUID = RANDOMNAMESXRC::GenerateObjectGUID();
		std::vector<IRenderNode *>::const_iterator it = succesfully_gen_oblects.begin();
		std::vector<IRenderNode *>::const_iterator end = succesfully_gen_oblects.end();
		int count = succesfully_gen_oblects.size();
		if (count > 0)
		{
			for (int il = 0; il < count, it != end; il++, ++it)
			{
				if ((*it)->GetRenderNodeType() == eERType_Brush)
				{
					IBrush *pBrush = (IBrush*)(*it);
					if (pBrush)
					{
						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						string type = "Brush";
						//string layer = "Main";
						//string LayerGUID = "{E0199113-42D5-4141-9C20-09D9C93AD7D4}";
						string objGUId = RANDOMNAMESXRC::GenerateObjectGUID();
						string nmn_generated = "Brush_Generated_";
						nmn_generated = RANDOMNAMESXRC::GenerateObjectName(nmn_generated);
						int FlNumber = -1;
						int ColorRGB = 16777215;
						Vec3 Pos = pBrush->GetPos();
						Vec3 Scale = Vec3(pBrush->GetMatrix().GetColumn(0).GetLength());
						Matrix34 tm = pBrush->GetMatrix();
						if (Scale != Vec3(1, 1, 1))
							tm.OrthonormalizeFast();

						Ang3 angles = Ang3::GetAnglesXYZ(tm);
						Quat Rot = Quat::CreateRotationXYZ(angles);

						int MatLayersMask = 0;
						string Prefab = "";
						if (pBrush->GetEntityStatObj())
							Prefab = pBrush->GetEntityStatObj()->GetFilePath();

						int OutdoorOnly = 0; int CastShadowMaps = 1; int RainOccluder = 1;
						int SupportSecondVisarea = 0; int DynamicDistanceShadows = 0; int Hideable = 0;
						int LodRatio = pBrush->GetLodRatio();
						int ViewDistRatio = pBrush->GetViewDistRatio();
						int NotTriangulate = 0;
						int NoDynamicWater = 0;
						int AIRadius = -1; int NoStaticDecals = 0; int NoAmnbShadowCaster = 0;
						int RecvWind = 0; int Occluder = 0; int DrawLast = 0; int ShadowLodBias = 0;
						int RndFlags = 1610612744;

						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Layer", layer);
						rnd_node->setAttr("LayerGUID", LayerGUID);
						rnd_node->setAttr("Id", objGUId);
						rnd_node->setAttr("Name", nmn_generated);
						rnd_node->setAttr("Pos", Pos);
						rnd_node->setAttr("FloorNumber", FlNumber);
						rnd_node->setAttr("Rotate", Rot);
						rnd_node->setAttr("Scale", Scale);
						rnd_node->setAttr("ColorRGB", ColorRGB);
						rnd_node->setAttr("MatLayersMask", MatLayersMask);
						rnd_node->setAttr("Prefab", Prefab);
						rnd_node->setAttr("OutdoorOnly", OutdoorOnly);
						rnd_node->setAttr("CastShadowMaps", CastShadowMaps);
						rnd_node->setAttr("RainOccluder", RainOccluder);
						rnd_node->setAttr("SupportSecondVisarea", SupportSecondVisarea);
						rnd_node->setAttr("DynamicDistanceShadows", DynamicDistanceShadows);
						rnd_node->setAttr("Hideable", Hideable);
						rnd_node->setAttr("LodRatio", LodRatio);
						rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
						rnd_node->setAttr("NotTriangulate", NotTriangulate);
						rnd_node->setAttr("NoDynamicWater", NoDynamicWater);
						rnd_node->setAttr("AIRadius", AIRadius);
						rnd_node->setAttr("NoStaticDecals", NoStaticDecals);
						rnd_node->setAttr("NoAmnbShadowCaster", NoAmnbShadowCaster);
						rnd_node->setAttr("RecvWind", RecvWind);
						rnd_node->setAttr("Occluder", Occluder);
						rnd_node->setAttr("DrawLast", DrawLast);
						rnd_node->setAttr("ShadowLodBias", ShadowLodBias);
						rnd_node->setAttr("RndFlags", RndFlags);
						obj_added_to_fl = true;
					}
				}
				else if ((*it)->GetRenderNodeType() == eERType_Decal)
				{
					IDecalRenderNode *pDecal = static_cast<IDecalRenderNode*>((*it));
					if (pDecal)
					{
						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						string type = "Decal";
						//string layer = "Main";
						//string LayerGUID = "{E0199113-42D5-4141-9C20-09D9C93AD7D4}";
						string objGUId = RANDOMNAMESXRC::GenerateObjectGUID();
						string nmn_generated = "Decal_Generated_";
						Vec3 Pos = pDecal->GetPos();
						Vec3 Scale = Vec3(pDecal->GetMatrix().GetColumn(0).GetLength());
						Matrix34 tm = pDecal->GetMatrix();
						if (Scale != Vec3(1, 1, 1))
							tm.OrthonormalizeFast();

						Ang3 angles = Ang3::GetAnglesXYZ(tm);
						Quat Rot = Quat::CreateRotationXYZ(angles);

						nmn_generated = RANDOMNAMESXRC::GenerateObjectName(nmn_generated);
						int FlNumber = -1;
						int ColorRGB = 16744319;
						string Material = "";
						if (pDecal->GetMaterial())
							Material = pDecal->GetMaterial()->GetName();

						int ProjectionType = SDecalProperties::eProjectOnTerrainAndStaticObjects;
						int Deferred = 1;
						int ViewDistRatio = pDecal->GetViewDistRatio();
						int SortPriority = pDecal->GetSortPriority();
						int rnd_flags = 0;
						float ProjectionDepth = pDecal->GetDecalProperties()->m_depth;
						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Layer", layer);
						rnd_node->setAttr("LayerGUID", LayerGUID);
						rnd_node->setAttr("Id", objGUId);
						rnd_node->setAttr("Name", nmn_generated);
						rnd_node->setAttr("Pos", Pos);
						rnd_node->setAttr("FloorNumber", FlNumber);
						rnd_node->setAttr("Rotate", Rot);
						rnd_node->setAttr("Scale", Scale);
						rnd_node->setAttr("ColorRGB", ColorRGB);
						rnd_node->setAttr("Material", Material);
						rnd_node->setAttr("ProjectionType", ProjectionType);
						rnd_node->setAttr("Deferred", Deferred);
						rnd_node->setAttr("ViewDistRatio", ViewDistRatio);
						rnd_node->setAttr("SortPriority", SortPriority);
						rnd_node->setAttr("ProjectionDepth", ProjectionDepth);
						rnd_node->setAttr("RndFlags", rnd_flags);
						obj_added_to_fl = true;
					}
				}
				else if ((*it)->GetRenderNodeType() == eERType_Vegetation)
				{
					IVegetation *pVeg = static_cast<IVegetation*>((*it));
					if (pVeg)
					{
						XmlNodeRef rnd_node = Objects_node->newChild("Object");
						if (!rnd_node)
							continue;

						string type = "Vegetation";
						Vec3 Pos = pVeg->GetPos();
						Vec3 Scale = Vec3(pVeg->GetScale());
						Matrix34 tm = Matrix34::CreateIdentity();
						pVeg->GetEntityStatObj(0, 0, &tm);
						if (Scale != Vec3(1, 1, 1))
							tm.OrthonormalizeFast();

						Ang3 angles = Ang3::GetAnglesXYZ(tm);
						Quat Rot = Quat::CreateRotationXYZ(angles);
						string Prefab = "0";
						//char idName[3] = "0";
						IStatInstGroup Group;
						if (gEnv->p3DEngine->GetStatInstGroup(pVeg->GetStatObjGroupId(), Group))
						{
							Prefab = Group.szFileName;
							//Prefab = itoa(pVeg->GetStatObjGroupId(), idName, 10);
							//Prefab = idName;
						}
						rnd_node->setAttr("Type", type);
						rnd_node->setAttr("Pos", Pos);
						rnd_node->setAttr("Rotate", Rot);
						rnd_node->setAttr("Scale", Scale);
						rnd_node->setAttr("Prefab", Prefab);
						obj_added_to_fl = true;
					}
				}
			}
		}
		//finaly save//add check for added objects, if succesfully_gen_oblects really empty do not save XML file
		if (!obj_added_to_fl)
			return;

		Objects_node->saveToFile(file_pth);
	}

	virtual void GetMemoryUsage(ICrySizer * s) const
	{
		s->Add(*this);
		s->AddContainer(succesfully_gen_oblects);
		s->AddContainer(succesfully_gen_entites);
	}
private:
	std::vector<IRenderNode *> succesfully_gen_oblects;
	std::vector<EntityId> succesfully_gen_entites;
};

REGISTER_FLOW_NODE("ProceduralGeneration:GenerateObjectsInGenerator", CFlowGenerateObjectsInGenerator);